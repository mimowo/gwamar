import os, sys
import time

from src.drsoft.utils import gwamar_utils

def prepare(parameters):
  python_cmd = sys.executable
  
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "prepare")
  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", 
   "MT", "EP", "TREE_ID"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/p0_save_strains_ordered.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p1_prepare_experiments.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p2_convert_profiles_genes.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p3_convert_profiles_muts.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p4_gen_binary_trees.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p5_cmp_resistance_profiles.py" + passed_params): exit()
  t1_time = time.time()
  gwamar_utils.logExecutionTime(t1_time - t0_time, "prepare")
