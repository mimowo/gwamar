import sys
import os
import time
from src.drsoft.scoring import Scoring

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.prepare import Prepare
from src.drsoft.utils import gwamar_params_utils
from src.drsoft.utils import gwamar_utils
  
def perm_test(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "permtest")
  
  params = ["D", "REVORDER", "SCORES",  "OW", "W", "GEN_BIN_PROFILES", "MT", "EP"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)
  
  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/perm1_generate_res_profiles.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/perm2_compute_perm_rep_scores.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/perm3_save_perm_rep_rankings.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/perm4_compute_perm_comb_scores.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/perm5_combine_perm_rep_scores.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/perm6_calc_pvalues.py" + passed_params): exit()
  t1_time = time.time()
 
  gwamar_utils.logExecutionTime(t1_time - t0_time, "perm_test")

def analysis1(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "analysis")
  
  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", "MT", "EP", "SCORE_SORT"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)
  
  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/a1_save_details_scores_all.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/a2_save_details_scores_sel.py" + passed_params): exit()
  t1_time = time.time()

  gwamar_utils.logExecutionTime(t1_time - t0_time, "analysis")

def analysis2(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "analysis")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", 
   "MT", "EP", "SCORE_SORT"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/a3_save_associations_all.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/a4_save_associations_sel.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "analysis2")

def other(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "other")
  
  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", "MT", "EP"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/show0_gold_associations.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "other")

def comparisonF(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "comparison")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", 
  "MT",  "EP", "RAND", "SUBSETS", "STATS"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/cmp3_draw_figures.py" + passed_params): exit()
  t1_time = time.time()
    
  gwamar_utils.logExecutionTime(t1_time - t0_time, "comparisonF")

def comparison3(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "comparison")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", 
   "MT",  "EP", "RAND", "SUBSETS", "STATS"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/cmp2_auc_stats.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "comparison3")

def comparison2(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "comparison")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", 
   "MT",  "EP", "RAND", "SUBSETS", "STATS"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/cmp1_prec_recall_stats.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "comparison2")

def comparison1(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "comparison")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", 
   "MT", "EP", "SUBSETS"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/cmp0_save_associations.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "comparison1")

def comp_mutations(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "compensatory")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GWAMAR_DATASETS"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/show0_save_putative_muts.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/show1_latex_rpoB_figure.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/show2_latex_rpoB_table.py" + passed_params): exit()
  t1_time = time.time()

  gwamar_utils.logExecutionTime(t1_time - t0_time, "comp_mutations")

def preparebroad(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "prebroad", "prepare")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GWAMAR_DATASETS", "RAXML_TYPE", 
            "RAXML_REP", "DEFAULT_TREE_SOFT"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/p1_download_files.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p2_save_strains.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p3_save_resdata.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p4_combine_resistance_profiles.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/p5_save_point_mutations_aa.py" + passed_params): exit()
  t1_time = time.time()

  gwamar_utils.logExecutionTime(t1_time - t0_time, "preparebroad")

def preparebroadtree(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "prebroad", "tree")

  params = ["D", "REVORDER", "SCORES", "OW", "W", "GWAMAR_DATASETS", 
   "RAXML_TYPE", "RAXML_REP", "DEFAULT_TREE_SOFT"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/t0_save_point_mutations_nt.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/t1_prepare_alignments.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/t2_compute_subset_tree.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/t3_resolve_clusters.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/t4_save_strains_ordered.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "preparebroadtree")

def print_help():
  print("You have not specified the action '-a' parameter.")
  print("Example gwamar invocation: python gwamar.py -a p -w 4 -d mtu173")
  print("For more help, please read the manual: <gwamar>/manual/manual.pdf")

if __name__ == '__main__':
  parameters = gwamar_params_utils.overwriteParameters(sys.argv)
  action = parameters.get("A", None)
  if action == None:
    print_help()
    sys.exit(-1)
  
  parameters = gwamar_params_utils.readParameters()
  
  WORKERS = int(parameters["W"])

  python_cmd = sys.executable
  action = action.upper()

  if action == "P":
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      Prepare.prepare(parameters)
  elif action == "O":
    other(parameters)
  elif action == "S":
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      Scoring.scores_std(parameters)
  elif action == "SR":
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      Scoring.scores_ranks(parameters)
  elif action == "SN":
    Scoring.scores_norm(parameters)
  elif action == "SP":
    Scoring.scores_pvalues(parameters)
  elif action == "PV":
    perm_test(parameters)
  elif action in ["A1", "AS1"]:
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      analysis1(parameters)
  elif action in ["A2", "AS2"]:
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      analysis2(parameters)
  elif action in ["A", "AS"]:
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      analysis1(parameters)
      analysis2(parameters)
  elif action in ["C", "CMP"]:
    datasets = parameters.get("D").split(",")
    for dataset in datasets:
      parameters["D"] = dataset
      comparison1(parameters)
      comparison2(parameters)
      comparison3(parameters)
      parameters["D"] = ",".join(datasets)
      comparisonF(parameters)
  elif action in ["C1", "CMP1"]:
    datasets = list(parameters.get("D").split(","))
    for dataset in datasets:
      parameters["D"] = dataset
      comparison1(parameters)
  elif action in ["C2", "CMP2"]:
    datasets = list(parameters.get("D").split(","))
    for dataset in datasets:
      parameters["D"] = dataset
      comparison2(parameters)
  elif action in ["C2", "CMP3"]:
    datasets = list(parameters.get("D").split(","))
    for dataset in datasets:
      parameters["D"] = dataset
      comparison3(parameters)
  elif action in ["CF", "CMPF"]:
    comparisonF(parameters)
  elif action == "CM":
    comp_mutations(parameters)
  elif action == "PB":
    preparebroad(parameters)
    preparebroadtree(parameters)
  elif action == "PBT":
    preparebroadtree(parameters)
  else:
    print("Unknown action: " + action);
