import sys
import os
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils



def writeMutationsHeader(output_fh, strains, strains_subset = None, only_strains = False, sep="\t"):
    if not only_strains: header_line = "cc_id" + "\t" + "aln_fn_id" + "\t" + "ann_id" + "\t" + "mut_type" + "\t" + "positions" + "\t" + "ref_positions"
    else: header_line = ""
    if strains_subset == None: strains_subset = strains.allStrains()
    for strain_id in strains_subset:      
        header_line += strain_id + sep
    header_line = header_line[:-1] + "\n"   
    output_fh.write(header_line);
    
def readRefAnns(input_fh):
    ref_anns = {}
    
    for line in input_fh.readlines():
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split("\t")
        mg_tokens = tokens[1].split()
        gene_id = tokens[2]
        
        if len(gene_id) <= 1:
            continue
        
        len_tokens = tokens[3].split()
        end = int(mg_tokens[0])
        strand = mg_tokens[1]
        
        length = 0
        
        for len_token in len_tokens:
            if not len_token.startswith("#"):
                continue
            len_token = len_token[1:]
            if len_token.startswith("*"):
                len_token = len_token[1:]
            len_token = len_token.split(":")[0]
            length = int(len_token)
        ref_anns[gene_id] = (end, length, strand)
    
    return ref_anns

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = "broad_mtb"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"
    snps_dir = dataset_dir + "snps/"
    
    input_fh = open(dataset_dir + "ref_annotation.txt")
    gene_anns = readRefAnns(input_fh)
    input_fh.close()
    
    input_fh = open(dataset_dir + "strains-all.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    strains_list = strains.strain_ids

    mutations_map = {}
    ref_states = {}
    ref_states_strain = {}
    gene_ids = {}
    
    for strain_id in strains_list:
        input_fh = open(snps_dir + strain_id + "/" + strain_id + ".snps.annotated")
        for line in input_fh.readlines():
            line = line.strip()
            tokens = line.split("\t")
            ref_strain = tokens[0]
            gene_id = tokens[1]
            if gene_id == "Rv2427Ac":
                gene_id = "Rv2427c"
            gene_name = tokens[2]
            

            md_tokens = tokens[3].split("_")
            mut_type = md_tokens[1]
            ref_position = int(md_tokens[2])
            
            if gene_name.startswith("promoter_"):
                gene_name = gene_name[len("promoter_"):]
            
                if gene_name == "rpsL":
                    gene_id = "Rv0682"
                elif gene_name == "embA-embB":
                    gene_id = "Rv3794"
                elif gene_name == "fabG1-inhA":
                    gene_id = "Rv1483"
                elif gene_name == "gyrB-gyrA":
                    gene_id = "Rv0005"
                elif gene_name == "iniB-iniA-iniC":
                    gene_id = "Rv0341"
            
                nuc1 = md_tokens[3][0]
                nuc2 = md_tokens[3][len(md_tokens[3])-1]
                if len(md_tokens[3]) > 2: position = int(md_tokens[3][1:len(md_tokens[3])-1])
                else: position = 0
                ref_state = nuc1
                new_state = nuc2
                
            elif len(md_tokens) == 6:
                if gene_id in gene_anns:
                    (end, length, strand) = gene_anns[gene_id]
                else:
                    print("NO", strain_id, gene_id)
                    continue
                nuc1 = md_tokens[3][0]
                nuc2 = md_tokens[3][1]
            
                aa1 = md_tokens[5][0]
                aa2 = md_tokens[5][len(md_tokens[5])-1]
                
                position = length - abs(end - ref_position)
                position_aa = int(md_tokens[5][1:len(md_tokens[5])-1])
                
                print(position/3, position_aa)
                
                ref_state = aa1 + ":" + nuc1
                new_state = aa2 + ":" + nuc2
                
            elif len(md_tokens) == 5:
                nuc1 = md_tokens[3][0]
                nuc2 = md_tokens[3][len(md_tokens[3])-1]
                position = int(md_tokens[3][1:len(md_tokens[3])-1])
                
                ref_state = nuc1
                new_state = nuc2
                                
            if not gene_name in gene_ids: gene_ids[gene_name] = gene_id
            if not gene_name in mutations_map: mutations_map[gene_name] = {}
            if not (gene_name, position) in ref_states:
                ref_states[(gene_name, position)] = ref_state
                ref_states_strain[(gene_name, position)] = (ref_state, strain_id)
            else:
                ref_state_old = ref_states[(gene_name, position)]
                if ref_state_old !=  ref_state:
                    print("JUZ JEST INNY!", ref_state, ref_state_old, gene_name, position, strain_id, ref_states_strain[(gene_name, position)])
                
            if not position in mutations_map[gene_name]: mutations_map[gene_name][position] = {}
            mutations_map[gene_name][position][strain_id] = (new_state, mut_type)

        input_fh.close()
        
        
    output_fh = open(dataset_dir + "man-all-mutations_nt.txt", "w")
    writeMutationsHeader(output_fh, strains, strains.strain_ids, only_strains = True, sep=" ")

    for gene_name in sorted(mutations_map):
        gene_id = gene_ids[gene_name]
        text_line = gene_id + '\t' + gene_name + "\n"
        output_fh.write(text_line)

        for position in sorted(mutations_map[gene_name]):
            ref_state = ref_states[(gene_name, position)]
            if len(gene_id) <= 1: gene_id = "x" 
            
            if position <= 0: mut_t = "snp-n"
            else:
                mut_t = "snp-s"
                for strain_id in mutations_map[gene_name][position]:
                    new_state, mut_type = mutations_map[gene_name][position][strain_id]
                    if mut_type in ["N", "CN"]:
                        mut_t = "snp_n"
                        break        
                if mut_t == "snp-n":
                    continue
                
            state_strains = {}
            for strain_index in range(len(strains_list)):
                strain_id = strains_list[strain_index]
                if strain_id in mutations_map[gene_name][position]:
                    new_state, mut_type = mutations_map[gene_name][position][strain_id]
                    if not new_state in state_strains: state_strains[new_state] = []
                    state_strains[new_state].append(strain_index)
                else:
                    if not ref_state in state_strains: state_strains[ref_state] = []
                    state_strains[ref_state].append(strain_index)
                
            sel_state = sorted(state_strains, key=lambda state:-len(state_strains[state]))[0]
            text_line = str(position) + "\t"
            text_line += str(position) + "\t"
            text_line += str(mut_t) + "\t"
            desc_text_line = sel_state + " "
            
            for state in state_strains:
                if state == "?": continue
                if state == sel_state: continue
                desc_text_line += state + ":"
                for strain_index in state_strains[state]: desc_text_line += str(strain_index) + ","    
                if desc_text_line.endswith(","): desc_text_line = desc_text_line[:-1] + " "
                
            text_line += desc_text_line.strip()
            output_fh.write(text_line +"\n") 
    output_fh.close()        
