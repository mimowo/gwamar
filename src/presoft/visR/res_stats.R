require(graphics); require(grDevices)

#data_fn = paste(species_id, "_out/r_subset/", drug_name, ".txt", sep = "")

dataset = "broad_mtb"
data_fn = paste("../../../datasets/",dataset,"/stats/res_stats.txt", sep="")

data = read.table(data_fn, header=F)

output_fn = paste("../../../datasets/",dataset,"/figures/panels_f1.pdf", sep="")

rows1 = (data[,2] >= 1 & data[,3] >= 1)
rows2 = (data[,2] < 10 | data[,3] < 10)
data_mat1 = t(as.matrix(data[rows1,c(2,3,4)]))
data_mat2 = t(as.matrix(data[rows2,c(2,3,4)]))
colnames(data_mat1) <- as.vector(data[rows1,1])
colnames(data_mat2) <- as.vector(data[rows2,1])

y1_lim = max(data_mat1[1,]+data_mat1[2,] + data_mat1[3,]) + 100
y2_lim = max(data_mat2[1,]+data_mat2[2,] + data_mat2[3,]) + 100

pdf(output_fn, width=18, height=10)
par(mar=c(17.5,7,1,0), oma=c(0,0,0,0), mgp=c(5,1,0))

colors = c("green", "red", "yellow")

b <- barplot(data_mat1[c(1,2,3),], col=colors, ylim=c(0, y1_lim), cex.lab=2.5, cex.axis=2.2, cex.sub=2.5, las=2, xaxt="n", ylab="# isolates")
axis(1, at=1.2*1:length(colnames(data_mat1))-0.5, labels=colnames(data_mat1), las=2, cex.axis=2.2, cex.lab=2.5, cex.sub=2.5, cex=2.5)

rect(12, 0.99*y1_lim, 24, 0.72*y1_lim, col="white")
legend(12, 0.99*y1_lim, pt.cex=0.1, bg="white", border="black",   
		c("susceptible strains",
				"resistant strains",
				"intermediate-resistant strains"), 
		text.width=c(5,5,5),
		bty="n", box.lwd=c(0,0,0,0), cex=2.7,  fill=colors, y.intersp=0.7, x.intersp=0.1)

text(x=b,y=(data_mat1[1,])-data_mat1[1,]/2-35, labels=data_mat1[1,], pos=3, col="black", cex=1)
text(x=b,y=(data_mat1[1,]+data_mat1[2,])-data_mat1[2,]/2-35, labels=data_mat1[2,], pos=3, col="black", cex=1) 
text(x=b,y=(data_mat1[1,]+data_mat1[2,]+data_mat1[3,])-data_mat1[3,]/2-10, labels=data_mat1[3,], pos=3, col="black", cex=1) 

dev.off()

