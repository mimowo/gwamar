import os
import sys
import platform
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils

def ensure_rem(filename):
    if os.path.exists(filename):
        os.remove(filename)

def computeTreeDNAML(input_fn, outfile_fn, outtree_fn, sufix = ""):
    if platform.system().upper().count("WIN") > 0:
        phylip_path = "C:/Users/misias/Dropbox/camber/phylip/exe/"
        if platform.system().upper().count("CYGWIN") > 0:
            input_fn = input_fn.replace("/cygdrive/c/", "C:/")
            outfile_fn = outfile_fn.replace("/cygdrive/c/", "C:/")
            outtree_fn = outtree_fn.replace("/cygdrive/c/", "C:/")
            abs_phylip_path = phylip_path
        else:
            abs_phylip_path = phylip_path
    else:    
        phylip_path = "/home/misias/phylip/exe/"
        abs_phylip_path = os.path.abspath(phylip_path)
    print(phylip_path)
    print(abs_phylip_path)
    print("input_fn", input_fn)
    print("outfile", outfile_fn)
    print("outtree", outtree_fn)
    os.chdir(phylip_path)   
    ensure_rem(outfile_fn)
    ensure_rem(outtree_fn)
    tmp_fn = "tmpfile" + sufix
    tmp_fh = open(tmp_fn, "w")
    tmp_fh.write(input_fn + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outfile_fn + "\n")
    tmp_fh.write("Y" + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outtree_fn + "\n")
    tmp_fh.close()
    if platform.system().count("WIN") > 0:
        os.system(abs_phylip_path + "dnamlk < " + tmp_fn)
    else:
        os.system(abs_phylip_path + "/dnamlk < " + tmp_fn)

def computeTreePROML(input_fn, outfile_fn, outtree_fn, sufix = ""):
    if platform.system().count("WIN") > 0:
        phylip_path = "C:/Users/workshop/Dropbox/camber/phylip/exe"
        if platform.system().upper().count("CYGWIN") > 0:
            input_fn.replace("/cygdrive/c/", "C:/")
            outfile_fn.replace("/cygdrive/c/", "C:/")
            outtree_fn.replace("/cygdrive/c/", "C:/")
            abs_phylip_path = phylip_path        
    else:    
        phylip_path = "/home/misias/phylip/exe/"
        abs_phylip_path = os.path.abspath(phylip_path)
    os.chdir(phylip_path)
    ensure_rem(outfile_fn)
    ensure_rem(outtree_fn) 
    print(os.path.curdir, abs_phylip_path)
    print(input_fn)
    print(outfile_fn)
    print(outtree_fn)
    tmp_fn = "tmpfile" + sufix
    tmp_fh = open(tmp_fn, "w")
    tmp_fh.write(input_fn + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outfile_fn + "\n")
    tmp_fh.write("Y" + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outtree_fn + "\n")
    tmp_fh.close()
    if platform.system().count("dows") > 0:
        os.system("promlk < " + tmp_fn)
    else:
        os.system(abs_phylip_path + "/promlk < " + tmp_fn)

    return None

if __name__ == '__main__':
    m_type = "all"
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset_dir = parameters["DATASET_DIR"]
    phylo_dir = parameters["PHYLO_DIR"]
    snps_dir = dataset_dir + "snps/"
    phylo_dir = dataset_dir + "phylo/"
    input_dir = dataset_dir + "input/"
    
    input_fh = open(input_dir + "/strain_profiles_ord.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    strains_ord = sorted(strains.allStrains())
    
    genes = [] 
    for gene_fn in os.listdir(phylo_dir + '/genes'):
        gene_id = gene_fn.split(".")[0].split("_")[-1]
        genes.append(gene_id)
    
    for gene_id in genes:    
        input_fn = phylo_dir + 'genes/alignment_cluster_'+gene_id+'.txt'
        outfile_fn = phylo_dir + 'genes/phylofile_'+gene_id+'.txt'
        outtree_fn = phylo_dir + 'genes/phylotree_'+gene_id+'.txt'
        
        print(input_fn, outtree_fn)
        
        computeTreeDNAML(input_fn, outfile_fn, outtree_fn, sufix = "_")
    