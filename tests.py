import os, sys

python_cmd = sys.executable

if (len(sys.argv) == 1):
  pattern = "*Test.py"
elif sys.argv[1] == "clean":
  sys.exit()
else:
  pattern = sys.argv[1]
  
print("Running unit tests for pattern: " + pattern)
cmd = python_cmd + ' -m unittest discover -p ' + pattern
os.system(cmd)
