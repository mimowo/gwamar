class MutationProfile:
    def __init__(self, position, m_type="?", full_profile=None):
        self.position = position
        if m_type == "?":
            if self.position <= 0: m_type = "p"
            else: m_type = "n"
        self.m_type = m_type
        self.full_profile = full_profile
