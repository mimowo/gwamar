input_fh = open("resdata.txt")
lines = input_fh.readlines()
input_fh.close()

tls = []

for line in lines:
	tokens = line.strip().split("\t")
	strain_id = tokens[0]
	drug_tokens = set(tokens[1].split(","))

	if tokens[1] == "MDR":
		tls.append("Isoniazid\t"+strain_id+"\tR")
		tls.append("Rifampicin\t"+strain_id+"\tR")
	else:
		if "INH" in drug_tokens: tls.append("Isoniazid\t"+strain_id+"\tR")
		else: tls.append("Isoniazid\t"+strain_id+"\tS")
		if "RIF" in drug_tokens: tls.append("Rifampicin\t"+strain_id+"\tR")
		else: tls.append("Rifampicin\t"+strain_id+"\tS")
		if "EMB" in drug_tokens: tls.append("Ethambutol\t"+strain_id+"\tR")
		else: tls.append("Ethambutol\t"+strain_id+"\tS")
		if "STR" in drug_tokens: tls.append("Streptomycin\t"+strain_id+"\tR")
		else: tls.append("Streptomycin\t"+strain_id+"\tS")

for tl in tls:
	print(tl)

