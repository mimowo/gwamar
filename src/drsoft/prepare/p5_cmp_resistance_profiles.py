import sys
import os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.structs import resistance_profile
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils

profile_clusters = {
"Fluoroquinolones":["Gatifloxacin","Ofloxacin","Moxifloxacin","Levofloxacin","Ciprofloxacin"],
"Aminoglycosides":["Amikacin","Kanamycin","Capreomycin"],
"Rifampicin":["Rifampicin","Rifabutin"]
 }  

def combineResistanceProfiles(res_profiles):
    res_profiles_comb = []

    combined = set([])
    res_profiles_map = {}
    for drp in res_profiles: res_profiles_map[drp.drug_name] = drp

    for cluster_id in profile_clusters:
        cluster_drp = resistance_profile.ResistanceProfile(cluster_id, count = k)

        for drug_name in profile_clusters[cluster_id]:
            drp = res_profiles_map.get(drug_name, None)
            if drp!= None:
                combined.add(drp.drug_name)

        for i in range(k):
            res_count = 0
            int_count = 0
            susc_count = 0
            for drug_name in profile_clusters[cluster_id]:
                drp = res_profiles_map.get(drug_name, None)

                if drp != None:

                    if drp.full_profile[i] == "R": res_count += 1
                    elif drp.full_profile[i] == "I": int_count += 1
                    elif drp.full_profile[i] == "S": susc_count += 1

            if res_count > 0 and susc_count >0:
              #  print("conflict", cluster_id, profile_clusters[cluster_id], i, strains_list[i], susc_count, int_count, res_count)
                cluster_drp.changeState(i, "?")
            elif res_count >= 1 and susc_count == 0:
                cluster_drp.changeState(i, "R")
            elif res_count == 0 and susc_count >= 1:
                cluster_drp.changeState(i, "S")
            elif res_count == 0 and susc_count == 0 and int_count >=1:
                cluster_drp.changeState(i, "I")
            else:
                cluster_drp.changeState(i, "?")
        res_profiles_comb.append(cluster_drp)

    for drp in res_profiles:
        res_profiles_comb.append(drp)
    return res_profiles_comb

if __name__ == '__main__':
  print("p5_cmp_resistance_profiles.py: preparation of resistnce profiles for comparison of different association scores. Start.")

  gwamar_params_utils.overwriteParameters(sys.argv)
  parameters = gwamar_params_utils.readParameters()
  
  m_type = parameters["MT"]
  input_dir = parameters["DATASET_INPUT_DIR"]
  results_dir = parameters["RESULTS_DIR"]

  sel_profiles = parameters["CMP_PROFILES"].split(",")

  input_fh = open(input_dir + "/strains_ordered.txt")
  strains = gwamar_strains_io_utils.readStrains(input_fh)
  input_fh.close()
  
  strains_list = strains.allStrains()
  k = len(strains_list)

  input_fn = input_dir + "/res_profiles_ret.txt"
  if not os.path.exists(input_fn):
      input_fn =  input_dir + "/res_profiles.txt"
      if not os.path.exists(input_fn):
          print("File " + input_fn + " does not exist.")
          sys.exit()
  output_fn = input_dir + "/res_profiles_cmp.txt"
  if os.path.exists(output_fn):
      print("Output file: " + output_fn + " already exist.")
      print("In order to use them replace manually this file with: ")
      print(results_dir + "/res_profiles_cmp.txt")
  else:
      input_fh = open(input_fn)
      res_profiles = gwamar_res_io_utils.readResistanceProfiles(input_fh, strains_list=strains.allStrains(), load_int=True)
      input_fh.close()

      res_profiles_comb = combineResistanceProfiles(res_profiles)
      
      output_fh = open(output_fn, "w")
      gwamar_res_io_utils.saveResistanceProfiles(output_fh, res_profiles_comb, strains_list, compress=True, sel_profiles=sel_profiles)
      output_fh.close()

      print("Profiles used for comparison of the association scores are generated in the file: ")
      print(output_fn)
      print("In order to use them replace manually this file with: ")
      print(results_dir + "/res_profiles_cmp.txt")
  
  print("p5_cmp_resistance_profiles.py. Finished")
