import sys
import os
from multiprocessing import Pool
from src.drsoft.utils import gwamar_strains_io_utils, gwamar_params_utils,\
  gwamar_utils
from src.drsoft.utils.gwamar_ann_utils import readClusterGeneIDs

def selectResRelBinProfiles(bin_profile_details, drug_rel_muts, cluster_names, cluster_ids, cluster_ids_rev):
    sel_bin_profiles = {}
    
    for bin_profile_id in bin_profile_details:
        for (gene_name, gene_id, cluster_id, position, res_states_txt, mut_states_txt) in bin_profile_details[bin_profile_id]:
            if not cluster_id in drug_rel_muts:
                pass
            else:
                ret_ranges = drug_rel_muts.get(cluster_id, None)
                if ret_ranges == None:
                    if not bin_profile_id in sel_bin_profiles:
                        sel_bin_profiles[bin_profile_id] = set([])
                    sel_bin_profiles[bin_profile_id].add((gene_name, gene_id, cluster_id, position, res_states_txt, mut_states_txt))
                else:
                    for start, end in ret_ranges:
                        if position >= start and position <= end:
                            if not bin_profile_id in sel_bin_profiles:
                                sel_bin_profiles[bin_profile_id] = set([])
                            sel_bin_profiles[bin_profile_id].add((gene_name, gene_id, cluster_id, position, res_states_txt, mut_states_txt))                        
    return sel_bin_profiles

def readBinProfileDetails(input_fh):
    bin_profile_details = {}
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        bin_profile_id = tokens[0]
        if not bin_profile_id in bin_profile_details:
            bin_profile_details[bin_profile_id] = set([])
            
        if len(tokens) == 7:
            gene_name = tokens[1]
            gene_id = tokens[2]
            cluster_id = tokens[3]
            position = int(tokens[4])
            res_states_txt = tokens[5]
            mut_states_txt = tokens[6]
        elif len(tokens) == 6:
            gene_name = tokens[1]
            gene_id = tokens[2]
            cluster_id = tokens[3]
            position = 0
            res_states_txt = tokens[4]
            mut_states_txt = tokens[5]
        bin_profile_details[bin_profile_id].add((gene_name, gene_id, cluster_id, position, res_states_txt, mut_states_txt))
        
    return bin_profile_details

def readBinProfilesSorted(input_fh, rev_order=False):
    bin_profiles = []
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        bin_profile_id = tokens[0]
        bin_profile_score = float(tokens[1])
        bin_profiles.append((bin_profile_id, bin_profile_score))
        
    if rev_order == False:
        bin_profiles_sorted = sorted(bin_profiles, key=lambda (bin_profile_id, bin_profile_score):-bin_profile_score)
    else:
        bin_profiles_sorted = sorted(bin_profiles, key=lambda (bin_profile_id, bin_profile_score):bin_profile_score)
    
    return bin_profiles_sorted

def loadDrugResMuts(input_fh, cluster_ids_rev={}):

    drug_targets = {}
    lines= input_fh.readlines();
    for line in lines:
        line = line.strip()
        if not line: break
        tokens_hash = line.split("#")
        if len(tokens_hash) > 1: line = tokens_hash[0]
        tokens = line.split()
        
        
        if len(tokens) < 5: continue
        
        drug_name = tokens[0]
        if not drug_name in drug_targets:
            drug_targets[drug_name] = {}
        gene_id = tokens[2]
        position = int(tokens[3])
        ref_states = tokens[4]
        mut_states = tokens[5]
            
        cluster_id =  cluster_ids_rev.get(gene_id, gene_id)
        
        if not cluster_id in drug_targets[drug_name]:
            drug_targets[drug_name][cluster_id] = {}
        if not position in drug_targets[drug_name][cluster_id]:
            drug_targets[drug_name][cluster_id][position] = set([])
        drug_targets[drug_name][cluster_id][position].add((ref_states, mut_states))
    return drug_targets

def loadDrugResRel(input_fh, cluster_ids_rev={}):

    drug_targets = {}
    lines= input_fh.readlines();
    for line in lines:
        line = line.strip()
        if not line: break
        tokens_hash = line.split("#")
        if len(tokens_hash) > 1: line = tokens_hash[0]
        tokens = line.split()
        
        print(tokens)
        
        if len(tokens) < 5: continue
        
        drug_name = tokens[0]
        if not drug_name in drug_targets:
            drug_targets[drug_name] = {}
        gene_id = tokens[2]
        position = int(tokens[3])
          
        cluster_id =  cluster_ids_rev.get(gene_id, gene_id)
        #print(line, cluster_id)

        start = position
        end = position
        
        if not cluster_id in drug_targets[drug_name]:
            drug_targets[drug_name][cluster_id] = [] 
        drug_targets[drug_name][cluster_id].append((start, end))
    return drug_targets

def scoreParamsSet(parameters):
    
    dataset = parameters["D"]
    score = parameters["SCORE"]
    
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"

    input_fh = open(dataset_dir + "strain_profiles_ord.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    if os.path.exists(dataset_dir + "cluster_names.txt"):
        cluster_fh = open(dataset_dir + "cluster_names.txt")
        cluster_ids, cluster_ids_rev = readClusterGeneIDs(cluster_fh)
        cluster_fh.close()
    else:
        cluster_ids = {} 
        cluster_ids_rev = {}
    
    input_fh = open(dataset_dir + "tbdr_muts_all.txt")
    drug_res_rel_all = loadDrugResMuts(input_fh, cluster_ids_rev)
    input_fh.close()
    
    input_fh = open(dataset_dir + "tbdr_muts_high.txt")
    drug_res_rel_high = loadDrugResMuts(input_fh, cluster_ids_rev)
    input_fh.close()
    
    print(drug_res_rel_high)
    
    drug_res_rel_union = {}
    
    for drug_name in set(drug_res_rel_high.keys()) | set(drug_res_rel_all.keys()):
        drug_res_rel_union[drug_name] = {}
    for drug_name in drug_res_rel_union:
        for cluster_id in drug_res_rel_all.get(drug_name, []):
            if not cluster_id in drug_res_rel_union[drug_name]:
                drug_res_rel_union[drug_name][cluster_id] = {}
        for cluster_id in drug_res_rel_high.get(drug_name, []):
            if not cluster_id in drug_res_rel_union[drug_name]:
                drug_res_rel_union[drug_name][cluster_id] = {}
        
        for cluster_id in drug_res_rel_union[drug_name]:
            if drug_name == "Rifampicin" and cluster_id == "Rv0667":
                print("www", drug_res_rel_high.get(drug_name, {}).get(cluster_id, []))
                print("www", drug_res_rel_all.get(drug_name, {}).get(cluster_id, []))
            for position in drug_res_rel_high.get(drug_name, {}).get(cluster_id, []):
                if not position in drug_res_rel_union[drug_name][cluster_id]:
                    drug_res_rel_union[drug_name][cluster_id][position] = set([])
            for position in drug_res_rel_all.get(drug_name, {}).get(cluster_id, []):
                if not position in drug_res_rel_union[drug_name][cluster_id]:
                    drug_res_rel_union[drug_name][cluster_id][position] = set([])    
            print("all", drug_name, cluster_id, position, drug_res_rel_all.get(drug_name, {}).get(cluster_id, {}).get(position, set([])))
            print("hig", drug_name, cluster_id, position, drug_res_rel_high.get(drug_name, {}).get(cluster_id, {}).get(position, set([])))
            for position in drug_res_rel_union[drug_name][cluster_id]:
                drug_res_rel_union[drug_name][cluster_id][position].update(drug_res_rel_all.get(drug_name, {}).get(cluster_id, {}).get(position, set([])))
                drug_res_rel_union[drug_name][cluster_id][position].update(drug_res_rel_high.get(drug_name, {}).get(cluster_id, {}).get(position, set([])))
    
    
    print("xx", drug_res_rel_union)
    text_lines = []
    
    mut_details = set([])
    for drug_name in drug_res_rel_union:
        for cluster_id in drug_res_rel_union[drug_name]:
            for position in drug_res_rel_union[drug_name][cluster_id]:
                for (ref_states, mut_states) in drug_res_rel_union[drug_name][cluster_id][position]:
                    if (ref_states, mut_states) in drug_res_rel_all.get(drug_name, {}).get(cluster_id, {}).get(position, {}): in_all = "Y"
                    else: in_all = "N"
                    if (ref_states, mut_states) in drug_res_rel_high.get(drug_name, {}).get(cluster_id, {}).get(position, {}): in_high = "Y"
                    else: in_high = "N"
                    
                    mut_details.add((drug_name, cluster_id, position, ref_states, mut_states, in_all, in_high))
    
    mut_details_sorted = sorted(mut_details)
    
    output_fn = dataset_dir + "/" + "db_cmp.txt"    
    output_fh = open(output_fn, "w")
    for (drug_name, cluster_id, position, ref_states, mut_states, in_all, in_high) in mut_details_sorted:
        text_line = drug_name + "\t"
        text_line += cluster_id + "\t"
        text_line += str(position) + "\t"
        text_line += ref_states + "\t"
        text_line += mut_states + "\t"
        text_line += in_all + "\t"
        text_line += in_high
        if in_all == "N" and in_high == "Y":
            print(text_line)
        output_fh.write(text_line + "\n")
    output_fh.close()
        
    return None

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    
    dataset = "tbdr"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"
    
    gwamar_utils.ensure_dir(dataset_dir)
    #score_params = ["tscore","tscore_stoch", "tscore_comb", "wsupport", "support", "or", "mi","tscore_rand2","tscore_stoch_rand2", "tscore_comb_rand2", "wsupport_rand2"]
    score_params = [ "tscore"]
    type_params = ["S"]
    exp_prefix = "FINAL7"
    
    TASKS = []
    TASK = {}
    
    for type_param in type_params:
        for score_param in score_params:
            TASK["D"] = dataset
            TASK["EXP_PREFIX"] = exp_prefix
            TASK["SCORE"] = score_param
            TASK["TYPE"] = type_param

            TASKS.append(TASK.copy())
                    
                              
    for TASK in TASKS:
        print(TASK)
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)   
        for r in pool.imap(scoreParamsSet, TASKS):
            print(r)
    else:
        for T in TASKS:
            scoreParamsSet(T)
    print("END")
