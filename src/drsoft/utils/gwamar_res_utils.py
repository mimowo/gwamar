def findScStrains(res_profiles_list):
    ret = None
    for drp in res_profiles_list:
        if ret == None: ret = drp.sc_strains_ids
        else: ret &= (drp.sc_strains_ids)
    
    if len(ret) == 0:
        ret1 = None
        for drp in res_profiles_list:   
            if ret1 == None: ret1 = drp.sc_strains_ids
            else: ret1 |= (drp.sc_strains_ids)
        ret2 = None
        for res_profile in res_profiles_list:
            if ret2 == None:
                ret2 = res_profile.sc_strains_ids | res_profile.unknown_strains_ids
            else: 
                ret2 &= (res_profile.sc_strains_ids | res_profile.unknown_strains_ids)
        return ret2 & ret1
    return ret

def compareProfiles(bin_profile, dr_profile):
    n = len(bin_profile)
    s0,s1,r0,r1,i0,i1 = 0,0,0,0,0,0
    for i in range(n):
        if bin_profile[i] == "?" or dr_profile[i] == "?":
            continue
        else:
            if bin_profile[i] == '0':
                if dr_profile[i] == "S":
                    s0 += 1
                elif dr_profile[i] == "R":
                    r0 += 1
                else:
                    i0 += 1
            else:
                if dr_profile[i] == "S":
                    s1 += 1
                elif dr_profile[i] == "R":
                    r1 += 1
                else:
                    i1 += 1
    return s0, s1, i0, i1, r0, r1

def drugNamePermID(drug_name_id):
    tokens = drug_name_id.split("_")
    if len(tokens) <= 1: return 0
    return int(tokens[len(tokens)-1])

