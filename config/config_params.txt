D:mtu173                  #
#D:mtu_broad               #sau461# # sau461#mtutest2##mtu_broad###      # dataset name
#D:mtutest
W:1                       # number of workers
REPS:100                  # number of samples for p-value calculations
SCORES:mi,or,lh,ws,r-tgh  #_yRAXML#ws_yPHYML,ws,ws_yRAXML,or,mi,lh,r-tgh#      # scores to compute
OW:W
SP:Y
STATS:auc
SCORE_SORT:r-tgh

A:A                     # PB - preprocessing of Broad data
                        # P - prepare intermediate files
                        # S - compute scores
                        # SP - scores pvalues
                        # PV - permutation test for p-value
                        # A - analysis - lists of scored associations

INDEL:10                # preprocessing parameter
MT:P                    # experiment type: P: point mutations / G - gene gain/loss
EP:ex                   # experiment prefix
ED:EP:MT                # experiment folder


RAND:10             # number of repetitions for score comparision
NEG:1000            # number of negatives selected randomly
TOP:50              # number of top associations taked into account  
OW:N                # overwrite / not overwrite
SUBSETS:p14,p15     # methods of score comparison

DEFAULT_TREE_SOFT:phyml

A_OUTPUT_TYPE:txt

TREE_ID:
CMP_PROFILES:Fluoroquinolones,Streptomycin,Isoniazid,Ethambutol,Rifampicin,Pyrazinamide           # drug resistance profiles used for comparison
RAXML_REP:11
RAXML_TYPE:MR

PREBROAD_SUB:all
PREBROAD_DIR:GWAMAR_PATH:prebroad/
PREBROAD_DOWNLOAD_DIR:PREBROAD_DIR:download/
PREBROAD_TREE_DIR:PREBROAD_DIR:tree_:PREBROAD_SUB:/
PREBROAD_OUTPUT_DIR:PREBROAD_DIR:mtu_broad/

GWAMAR_DATASETS:GWAMAR_PATH:datasets/
GWAMAR_SRC:GWAMAR_PATH:src/
DATASET_DIR:GWAMAR_DATASETS:D:/

DATASET_INPUT_DIR:DATASET_DIR:input:/
DATASET_GOLD_DIR:DATASET_DIR:gold:/
DATASET_INTER_DIR:DATASET_DIR:intermediate:/
STATS_DIR:DATASET_DIR:stats:/
PHYLO_DIR:DATASET_DIR:phylo:/
LATEX_DIR:DATASET_DIR:latex:/
RESULTS_DIR:DATASET_DIR:ED:/

COMP_DATA:GWAMAR_PATH:other/comp_data/
RES_DATA:GWAMAR_PATH:other/res_data/
GEO_DATA:GWAMAR_PATH:other/geo_data/
TGH_DIR:DATASET_DIR:tgh:/
TGH2_DIR:DATASET_DIR:tgh2:/

RESULTS_SCORES_DIR:RESULTS_DIR:scores:/
RESULTS_GROUPING_DIR:RESULTS_DIR:grouping:/
RESULTS_RANKS_DIR:RESULTS_DIR:rankings:/
RESULTS_PERM_DIR:RESULTS_DIR:perm:/
RESULTS_PERM_PROFILES_DIR:RESULTS_PERM_DIR:profiles:/
RESULTS_PERM_SCORES_DIR:RESULTS_PERM_DIR:scores:/
RESULTS_PERM_RANKS_DIR:RESULTS_PERM_DIR:rankings:/
RESULTS_PERM_COMBS_DIR:RESULTS_PERM_DIR:combs:/
RESULTS_PERM_PVALUES_DIR:RESULTS_PERM_DIR:pvalues:/

RESULTS_ANALYSIS_DIR:RESULTS_DIR:analysis:/
RESULTS_A_LATEX_S_DIR:RESULTS_ANALYSIS_DIR:comb_latex:/
RESULTS_A_LATEX_S_SEL_DIR:RESULTS_ANALYSIS_DIR:comb_sel_latex:/
RESULTS_A_COMB_S_DIR:RESULTS_ANALYSIS_DIR:top_scoring:/
RESULTS_A_COMB_S_SEL_DIR:RESULTS_ANALYSIS_DIR:top_scoring_sel:/
RESULTS_A_COMB_R_DIR:RESULTS_ANALYSIS_DIR:top_ranked:/
RESULTS_A_COMB_R_SEL_DIR:RESULTS_ANALYSIS_DIR:top_ranked_sel:/
RESULTS_A_COMP_S_DIR:RESULTS_ANALYSIS_DIR:comp_mutations:/

RESULTS_CMP_DIR:RESULTS_DIR:comparison:/
RESULTS_CMP_ASSOCS_DIR:RESULTS_CMP_DIR:assocs:/
RESULTS_CMP_STATS_DIR:RESULTS_CMP_DIR:stats:/

