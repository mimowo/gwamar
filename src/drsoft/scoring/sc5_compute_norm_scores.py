import sys
import os
from multiprocessing import Pool
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_scoring,\
  gwamar_res_io_utils, gwamar_bin_utils, gwamar_utils, gwamar_progress_utils

def normalizeBinScores(bin_profiles_sorted):
    n = len(bin_profiles_sorted)
    norm_bin_profiles_sorted = []
    first_score = bin_profiles_sorted[0][1]
    last_score = bin_profiles_sorted[-1][1]
    for i in range(n):
        bin_profile_id, bin_profile_score = bin_profiles_sorted[i]
        if last_score == first_score: norm_bin_profile_score = 0.0
        else: norm_bin_profile_score = float(last_score - bin_profile_score)/float(last_score - first_score)
        norm_bin_profiles_sorted.append((bin_profile_id, norm_bin_profile_score))
    return norm_bin_profiles_sorted

def scoreDrug(params):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    drug_name = params["DRUG_NAME"]
    score = params["SCORE"]

    scores_dir = parameters["RESULTS_SCORES_DIR"]
    ranks_dir = parameters["RESULTS_RANKS_DIR"]
    results_dir = parameters["RESULTS_DIR"]

    rev_order = gwamar_scoring.revOrder(score)
    
    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    res_profiles = {}
    
    for res_profile in res_profiles_list:
        res_profiles[res_profile.drug_name] = res_profile
    input_fn = scores_dir + drug_name + "/" + score + ".txt"
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 10:
        print("NOT EXIST: ", input_fn)
        return drug_name, score

    gwamar_utils.ensure_dir(ranks_dir + drug_name)
            
    input_fh = open(input_fn)
    bin_profiles_sorted = gwamar_bin_utils.readBinProfilesSorted(input_fh, rev_order)
    input_fh.close()
    
    norm_bin_profiles_sorted = normalizeBinScores(bin_profiles_sorted)        

    output_fh = open(scores_dir + drug_name + "/n-" + score + ".txt", "w") 
    for (bin_profile_id, bin_profile_score) in norm_bin_profiles_sorted:
        output_fh.write(str(bin_profile_id) + "\t" + str(bin_profile_score) + "\n")
    output_fh.close()
    
    return drug_name, score
    
if __name__ == '__main__':
    print("sc5_compute_norm_scores.py. Normalization of scores.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    
    TASKS = []
    TASK = {}
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        for score_fn in os.listdir(scores_dir + drug_name):
            score = score_fn[:-4]
            if score.startswith("pv-") or score.startswith("n-"):
                continue
           
            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score

            TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Scores normalized")
    progress.setJobsCount(len(TASKS))
    
    WORKERS = min(WORKERS, len(TASKS))

    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(scoreDrug, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreDrug(T)
            progress.update(str(r))
    print("sc5_compute_norm_scores.py. Finished.")
    