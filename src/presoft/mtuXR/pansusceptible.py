input_fh = open("resdata.txt")
lines = input_fh.readlines()
input_fh.close()

tls = []

drugs_list = ["Isoniazid",
			"Rifampicin",
			"Rifabutin",
			"Ciprofloxacin",
			"Cycloserine",
			"Amikacin",
			"Streptomycin",
			"Kanamycin",
			"Ethambutol",
			"Ofloxacin",
			"Capreomycin",
			"Ethionamide",
			"Pyrazinamide",
			"PAS"]

strains_list = ["H37Rv","H37Ra","H37Ra_WGS","Erdman","C","K85","CPHL_A","EAS054","T92", "T17","T46","94_M4241A",
				"02_1987","CCDC5079","GM_1503","CTRI-2","KZN_4207","KZN_4207_Broad","SUMu001","SUMu002","SUMu003",
				"SUMu004","SUMu005","SUMu006","SUMu007","SUMu008","SUMu009","SUMu010","SUMu011",
				"SUMu012"]

for strain_id in strains_list:
	for drug_id in drugs_list:
		tls.append(drug_id+"\t"+strain_id+"\tS")
		
for tl in tls:
	print(tl)

