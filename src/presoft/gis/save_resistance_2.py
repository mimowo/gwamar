import sys
import os
from src.drsoft.utils import gwamar_params_utils

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = "em59"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"
    snps_dir = dataset_dir + "snps/"
    
    res_profiles = {}
    strain_id_map = {}
    
    input_fh = open(dataset_dir + "mapping.txt")
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("_")
        if len(tokens) < 4:
            continue
        strain_id_map[tokens[3]] = tokens[0]
    input_fh.close()
    print(strain_id_map)
    
    input_fh = open(dataset_dir + "res_data_tmp.txt")
    lines = input_fh.readlines()
    line0 = lines[0].strip()
    drug_names = line0.strip("\n").split("\t")[13:]
    
    for drug_name in drug_names:
        print(drug_name)
        res_profiles[drug_name] = {}
        res_profiles[drug_name]["R"] = set([])
        res_profiles[drug_name]["S"] = set([])
        res_profiles[drug_name]["I"] = set([])   
    
    for line in lines[1:]:
        tokens = line.strip("\n").split("\t")
        strain_id_tmp = tokens[0]
        if not strain_id_tmp in strain_id_map:
            print("missing: ", strain_id_tmp)
            continue
        strain_id = strain_id_map[strain_id_tmp]
        res_tokens = tokens[13:]
        
        for i in range(len(res_tokens)):
            res_token = res_tokens[i]
            drug_name = drug_names[i]
            if res_token.upper() == "R":
                res_profiles[drug_name]["R"].add(strain_id)
            elif res_token.upper() == "S":
                res_profiles[drug_name]["S"].add(strain_id)
            elif res_token.upper() == "I":
                res_profiles[drug_name]["I"].add(strain_id)
    input_fh.close()
    
    text_lines = []
    
    for drug_name in res_profiles:
        
        res_profile = res_profiles[drug_name]
        
        if len(res_profile["R"]) < 1 or (len(res_profile["S"]) < 1 and len(res_profile["I"]) < 1):
            continue

        text_line = drug_name+"\t"
        
        for strain_id in sorted(res_profile["R"]):
            if strain_id in res_profile["S"]:
                continue
            text_line += strain_id+";"
            
        if text_line.endswith(";"):
            text_line = text_line[:-1] + "\t"
        else:
            text_line += "\t"
        
        for strain_id in sorted(res_profile["S"]):
            if strain_id in res_profile["R"]:
                continue
            text_line += strain_id+";"
            
        if text_line.endswith(";"):
            text_line = text_line[:-1] + "\t"
        else:
            text_line += "\t"
        
        for strain_id in sorted(res_profile["I"]):
            text_line += strain_id+";"
            
        if text_line.endswith(";"):
            text_line = text_line[:-1]
        text_lines.append(text_line)
        
#        if len(res_profile["I"]) > 0:
#             
#            text_line = drug_name + "_SI_R"+"\t"
#            
#            for strain_id in sorted(res_profile["R"]):
#                if strain_id in res_profile["S"]:
#                    continue
#                text_line += strain_id+";"
#                
#            if text_line.endswith(";"):
#                text_line = text_line[:-1] + "\t"
#            else:
#                text_line += "\t"
#            
#            for strain_id in sorted(res_profile["S"]):
#                if strain_id in res_profile["R"]:
#                    continue
#                text_line += strain_id+";"
#            
#            for strain_id in sorted(res_profile["I"]):
#                text_line += strain_id+";"
#                
#            if text_line.endswith(";"):
#                text_line = text_line[:-1]
#            text_lines.append(text_line)
#    
#            text_line = drug_name + "_S_IR"+"\t"
#    
#            for strain_id in sorted(res_profile["R"]):
#                if strain_id in res_profile["S"]:
#                    continue
#                text_line += strain_id+";"
#                
#            for strain_id in sorted(res_profile["I"]):
#                text_line += strain_id+";"
#                    
#            if text_line.endswith(";"):
#                text_line = text_line[:-1] + "\t"
#            else:
#                text_line += "\t"
#            
#            for strain_id in sorted(res_profile["S"]):
#                if strain_id in res_profile["R"]:
#                    continue
#                text_line += strain_id+";"
#                
#            if text_line.endswith(";"):
#                text_line = text_line[:-1] + "\t"
#            else:
#                text_line += "\t"
#            
#            if text_line.endswith(";"):
#                text_line = text_line[:-1]
#            text_lines.append(text_line)
#    
    output_fh = open(dataset_dir + "res_profiles_ret.txt", "w")
    for text_line in text_lines:    
        output_fh.write(text_line + "\n")
    output_fh.close() 
    
        