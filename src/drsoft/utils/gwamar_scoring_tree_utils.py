import math

from src.drsoft.utils.gwamar_stat_utils import mean
from src.drsoft.utils import gwamar_bin_utils, gwamar_pws, gwamar_progress_utils
from src.drsoft.modelling import model_logic

def calcBinProfilesMinTreeScore(bin_profiles, drp, trees_list, verbose=False, alpha=0.0):
    scores = {}
    tstable = model_logic.getstable(0.5, 0.0, -1.0)

    if len(drp.getResistant()) == 0:
        return {}    
    
    for bin_profile_id in bin_profiles.keys():
        scores[bin_profile_id] = []
    
    if verbose:
        progress = gwamar_progress_utils.GWAMARProgress("tscore: " + drp.drug_name)
        progress.setJobsCount(len(trees_list)*len(bin_profiles))

    for tree in trees_list:    
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)
        
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(newtree)
        model_logic.calcSubnodesCount(newtree)
        model_logic.calcDetParsResModel(newtree)

        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            score = 0.0
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)
                model_logic.calcDetParsMutModel(newtree)
                
                for node in newtree.leaves:
                    node.tscore_tmp = 0.0
                
                for node in newtree.inner_nodes_bu:
                    node.tscore_tmp = 0.0 
                    
                    n,n0,n1 = 0,0,0
                    r0,s0,i0 = 0,0,0
                    r1,s1,i1 = 0,0,0
                    r,s,i = 0,0,0
                    
                    node_res = node.complete_res_state
                    node_mut = node.complete_mut_state
                    node_desc = node_res + node_mut

                    for ch_node in node.children:
                        node.tscore_tmp += ch_node.tscore_tmp
                        
                        ch_node_res = ch_node.complete_res_state
                        ch_node_mut = ch_node.complete_mut_state
                        
                        ch_desc = ch_node_res + ch_node_mut
                        if ch_desc == "R0": r += 1; n += 1; n0 += 1; r0 += 1
                        elif ch_desc == "R1": r += 1; n += 1; n1 += 1; r1 += 1
                        elif ch_desc == "S0": s += 1; n += 1; n0 += 1; s0 += 1
                        elif ch_desc == "S1": s += 1; n += 1; n1 += 1; s1 += 1
                        elif ch_desc == "I0": i += 1; n += 1; n0 += 1; i0 += 1
                        elif ch_desc == "I1": i += 1; n += 1; n1 += 1; i1 += 1

                    if   node_desc == "R0": node.tscore_tmp += min(1,r1)*tstable.get("RR01")
                    elif node_desc == "R1": node.tscore_tmp += min(1,r1)*tstable.get("RR11")
                    elif node_desc == "I0": node.tscore_tmp += min(1,r1)*tstable.get("IR01") + min(1,i1)*tstable.get("II01")
                    elif node_desc == "I1": node.tscore_tmp += min(1,r1)*tstable.get("IR11") + min(1,i1)*tstable.get("II11")
                    elif node_desc == "S0": node.tscore_tmp += min(1,r1)*tstable.get("SR01") + min(1,i1)*tstable.get("SI01") + s1*tstable.get("SS01")
                    elif node_desc == "S1": node.tscore_tmp += min(1,r1)*tstable.get("SR11") + min(1,i1)*tstable.get("SI11") + s1*tstable.get("SS11")
                
                    #print(node.tscore_tmp, node_desc, len(node.children), r0, r1, s0,s1,i0,i1)
                stmp = newtree.root.tscore_tmp
                score += max(stmp, 0.0) + alpha * min(stmp, 0.0)
            
            scores[bin_profile_id].append(max(0.0, score))
            if verbose:
                progress.update()

    scores_mean = {}
    
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = mean(scores[bin_profile_id])
        
    return scores_mean


def calcBinProfilesTreeScore(bin_profiles, drp, trees_list, theta = 0.5, zeta=0.0, beta=-1.0, verbose=False, tscore_type="P", alpha=0.0):
    scores = {}

#    print("Xxx", theta, zeta, beta)
    if len(drp.getResistant()) == 0:
        return {}
        
    for bin_profile_id in bin_profiles.keys():
        scores[bin_profile_id] = []
    
    if verbose:
        progress = gwamar_progress_utils.GWAMARProgress("tscore: " + drp.drug_name)
        progress.setJobsCount(len(trees_list)*len(bin_profiles))
    for tree in trees_list:
        
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)
        
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(newtree)
        model_logic.calcSubnodesCount(newtree)
        model_logic.calcDetParsResModel(newtree)
        
        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            score = 0.0

            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)
                model_logic.calcDetParsMutModel(newtree)

                score_tmp = model_logic.calcTransitionScore(newtree, theta, zeta, beta, tscore_type)
                score += max(score_tmp, 0.0) + alpha*min(score_tmp, 0.0)
            
            scores[bin_profile_id].append(max(0.0, score))
            if verbose:
                progress.update()

    scores_mean = {}
    
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = mean(scores[bin_profile_id])
        
    return scores_mean


def calcBinProfilesWeightedSupport(bin_profiles, drp, trees_list, alpha=0.0, norm=False):
    scores = {}
    
    if len(drp.getResistant()) == 0:
        return {}
    
    for bin_profile_id in bin_profiles.keys():
        scores[bin_profile_id] = []

    for tree in trees_list:
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)
        
        resistant = drp.getResistant()
        susceptible = drp.getSusceptible()
        beta = float(len(resistant)/float(len(susceptible)))
        
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        newtree.calcContributions(drp, beta)
        
        if norm: max_ws = newtree.maximalWeightedSupport()
        else: max_ws = 1.0
        
        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            score = 0.0
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                wsupport = newtree.calcWeightedSupport(gwamar_bin_utils.getMutated(bin_profile))
                if norm: wsupport = wsupport / max_ws
                score += max(wsupport, 0.0) + alpha*min(wsupport, 0.0)
            scores[bin_profile_id].append(max(0.0, score))

    scores_mean = {}
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = mean(scores[bin_profile_id])
    return scores_mean


def calcBinProfilesWeightedSupportPvalue(bin_profiles, drp, trees_list, alpha=0.0, norm=False):
    scores = {}
    
    if len(drp.getResistant()) == 0:
        return {}
    
    for bin_profile_id in bin_profiles.keys():
        scores[bin_profile_id] = []
    
    for tree in trees_list:
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)
        
        resistant = drp.getResistant()
        susceptible = drp.getSusceptible()
        known = drp.getKnown()

        beta = float(len(resistant)/float(len(susceptible)))
        
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        newtree.calcContributions(drp, beta)
        
        wmap = {}

        for leaf in newtree.leaves:
            if not leaf.contribution in wmap:
                wmap[leaf.contribution] = 0
            wmap[leaf.contribution] += 1

        res_map = gwamar_pws.wsupportTestMap(wmap)

        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            score = 0.0
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                mutated = gwamar_bin_utils.getMutated(bin_profile) & known
                ws = newtree.calcWeightedSupport(mutated)
                n = len(mutated)
                tc = 0.0
                sc = 0.0
                for s in res_map[n]:
                    if s >= ws: sc += res_map[n][s]
                    tc += res_map[n][s]
                
                if sc > 0:
                    pws = float(sc) / float(tc)
                    score += -math.log(pws)
            scores[bin_profile_id].append(score)

    scores_mean = {}
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = mean(scores[bin_profile_id])
    return scores_mean

def calcBinProfilesTreeScoreStoch(bin_profiles, drp, tree_list,
                                  eps_m=0.00001, psi_m=0.1, phi_m=0.0001,  
                                  eps_r=0.001, alpha_r=0.2, beta_r=0.0001, 
                                  theta=0.5, zeta=0.0, beta=-1.0,
                                  tscore_type="P", prob_profile=False, verbose=False, alpha=0.0):
    scores = {}

    for bin_profile_id, bin_profile in bin_profiles.items():
        scores[bin_profile_id] = []

    if verbose:
        progress = gwamar_progress_utils.GWAMARProgress("TreeScoreStoch " + drp.drug_name)
        progress.setJobsCount(len(bin_profiles)*len(tree_list))
            
    for tree in tree_list:
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        if prob_profile: 
            tree.setObservedResProbProfile(drp.res_profile_prob_map)
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)
    
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        model_logic.calulateCompleteResProfile(newtree, eps=eps_r, alpha=alpha_r, beta=beta_r)
        
        model_logic.calcProbAllLeafResistant(newtree)
        model_logic.calcSubleavesResCount(newtree)
        model_logic.calcSubnodesCount(newtree)
        
        for bin_profile_id, gen_bin_profile in bin_profiles.items():        
            score = 0.0
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)

                model_logic.calulateCompleteMutProfile(newtree, eps=eps_m, psi=psi_m, phi=phi_m)
                
                tscore_stoch = model_logic.calcTransitionScoreExprectedValue(newtree, theta, zeta, beta, tscore_type)
                score += max(tscore_stoch, 0.0) + alpha*min(tscore_stoch, 0.0)

            scores[bin_profile_id].append(max(0.0, score))
            if verbose:
                progress.update()

    scores_mean = {}
    
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = mean(scores[bin_profile_id])
        
    return scores_mean
