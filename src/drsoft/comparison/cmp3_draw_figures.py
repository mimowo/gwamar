import sys
import os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils

if __name__ == '__main__':
  print("cmp3_draw_figures.py: Start.")
  
  gwamar_params_utils.overwriteParameters(sys.argv)
  parameters = gwamar_params_utils.readParameters()
  
  dataset = parameters["D"]
  cmp_dir = parameters["RESULTS_CMP_DIR"]
  cmp_assocs_dir = parameters["RESULTS_CMP_ASSOCS_DIR"]
  Rscript_dir = parameters["GWAMAR_SRC"] + "visR/accuracy/"

  scores_p = parameters["SCORES"].split(",")

  scores_list = []
  for score in scores_p:
    score_fn = score + ".txt"
    scores_list.append(score)
  scores_arg = ','.join(scores_list)

  subsets = parameters.get("SUBSETS", "p14,p15")
  stats = parameters.get("STATS", "auc,auc10,auc20")

  rparams1 = " ".join([dataset, parameters["MT"], subsets, parameters["GWAMAR_PATH"], scores_arg, stats])
  print(rparams1)
  command = "Rscript " + Rscript_dir + "/auc_barplots.R " + rparams1
  print(command)
  os.system(command)
  print("cmp3_draw_figures.py: Finished.")
