import sys
import os
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils

def loadResData(input_fh):
    res_data_map = {}
    
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens) > 0:
            try:
                drug_name = tokens[0]
                if drug_name[0] == "#":
                    continue
                
                strain_id = tokens[1].replace("/", "")
                
                res_info = tokens[2]
                cite = tokens[3]
                if not strain_id in res_data_map:
                    res_data_map[strain_id] = {}
                if not drug_name in res_data_map[strain_id]:
                    res_data_map[strain_id][drug_name] = set([])
                res_data_map[strain_id][drug_name].add((res_info, cite))
            except:
                print("ERROR", line)
    return res_data_map

def translateDataIntoResProfiles(res_data):
    res_profiles = {}
    for strain_id in res_data:
        for drug_name in res_data[strain_id]:
            res_count = 0 
            susc_count = 0
            int_count = 0 
            unk_count = 0
            if not drug_name in res_profiles:
                res_profiles[drug_name] = {}
                res_profiles[drug_name]["R"] = set([])
                res_profiles[drug_name]["S"] = set([])
                res_profiles[drug_name]["I"] = set([])            
            for (drug_res_info, cite_info) in res_data[strain_id][drug_name]:
                tokens = drug_res_info.split(":")
                if tokens[0] == "R":
                    res_count += 1 
                elif tokens[0] == "S":
                    susc_count += 1
                elif tokens[0] == "I":
                    int_count += 1
                elif tokens[0] == "U":
                    unk_count += 1
            if res_count > 0 and susc_count == 0:
                res_profiles[drug_name]["R"].add(strain_id)
            elif res_count == 0 and susc_count > 0:
                res_profiles[drug_name]["S"].add(strain_id)
            elif res_count > 0 and susc_count > 0:
                pass
            elif int_count >0:
                res_profiles[drug_name]["I"].add(strain_id)
            elif unk_count > 0:
                pass
            else:
                pass
                                  
    return res_profiles

def loadCitations(genome_citations, input_fh):
    citations = set([])
    
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) >= 4:
            citation = tokens[3]
            if not citation in set(genome_citations):
                citations.add(citation)
        
    return citations    
    
def loadStrainStatuses(input_fh):
    status_map = {}
    citations_set = set([])
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens)>=2 and tokens[1] != "-":
            status_map[tokens[0]] = tokens[1]
            citations_set.add(tokens[1])
    return status_map, citations_set

if __name__ == '__main__':

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = "sta196"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset) + "/"
    
    input_fh = open(dataset_dir + "strain_profiles_ord.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(dataset_dir + "strains-citations.txt")
    seq_citations_map, citations_set = loadStrainStatuses(input_fh)
    input_fh.close()
    
    text = ""
    for strain_id in seq_citations_map:
        text += strain_id +"\\cite{"+seq_citations_map[strain_id]+"}"+", "
    
    print(text)
    
    input_fh = open(dataset_dir + "res-data.txt")
    citations = loadCitations(citations_set, input_fh)
    input_fh.close()
    
    print(citations)
    
    text = "\\cite{"
    for citation in citations:
        if citation != "":
            text+= citation+", "
    
    text += "}"
    print(text)
