import sys
import os
from multiprocessing import Pool
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_scoring, gwamar_utils,\
  gwamar_res_io_utils, gwamar_strains_io_utils, gwamar_res_utils,\
  gwamar_progress_utils

def scoreDrug(params):
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    drug_name = params["DRUG_NAME"]
    score_full = params["SCORE"]
    drug_res_profile = params["DRP"]

    drug_name_id = drug_res_profile.drug_name
    
    bin_profile_scores, rev_sorting = gwamar_scoring.computeScores(score_full, drug_name, parameters)

    if bin_profile_scores == None:
        return drug_name, score_full
    
    text_lines = []
    if rev_sorting:
        bin_profile_scores_sorted = sorted(bin_profile_scores, key=lambda bin_profile_id:bin_profile_scores[bin_profile_id])
    else:
        bin_profile_scores_sorted = sorted(bin_profile_scores, key=lambda bin_profile_id:-bin_profile_scores[bin_profile_id])
        
    for bin_profile_id in bin_profile_scores_sorted:
        bin_profile_score = bin_profile_scores[bin_profile_id]
        text_line = str(bin_profile_id) + "\t" + str(bin_profile_score) + "\n"
        text_lines.append(text_line)

    output_fn = perm_scores_dir + drug_name + "/" + score_full + "/" + drug_name_id + ".txt"
    output_fh = open(output_fn, "w") 
    for text_line in text_lines:
        output_fh.write(text_line)
    output_fh.close()
        
    return drug_name_id, score_full
    
if __name__ == '__main__':
    print("perm2_compute_perm_rep_scores.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    print("W", WORKERS)
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    perm_dir = parameters["RESULTS_PERM_DIR"]
    perm_scores_dir = parameters["RESULTS_PERM_SCORES_DIR"]
    perm_profiles_dir = parameters["RESULTS_PERM_PROFILES_DIR"]
    perm_rep = int(parameters["REPS"])
    results_dir = parameters["RESULTS_DIR"]
    
    score_params = parameters["SCORES"].split(",")
    
    TASKS = []
    TASK = {}
    
    gwamar_utils.ensure_dir(perm_scores_dir)
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(perm_scores_dir + drug_name)

        input_fh = open(perm_profiles_dir + "/" + drug_name + ".txt")
        res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
        input_fh.close()

        for score_param in score_params:
            gwamar_utils.ensure_dir(perm_scores_dir + drug_name + "/" + score_param)
    
            for drp in res_profiles_list:
    
                drug_name_id = drp.drug_name
                current_id = gwamar_res_utils.drugNamePermID(drug_name_id)
                if current_id >= perm_rep: continue
                    
                output_fn = perm_scores_dir + drug_name + "/" + score_param + "/" + drug_name_id +  ".txt"
                if os.path.exists(output_fn): continue
                
                TASK["DRUG_NAME"] = drug_name
                TASK["DRP"] = drp
                TASK["SCORE"] = score_param  
                TASKS.append(TASK.copy())
    print(len(TASKS))
    
    progress = gwamar_progress_utils.GWAMARProgress("Permutation scores")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))

    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for drug_name_id, score in pool.imap(scoreDrug, TASKS):
            progress.update(drug_name_id + " "+ score)
    else:
        for T in TASKS:
            drug_name_id, score = scoreDrug(T)
            progress.update(drug_name_id + " "+ score)

    print("perm2_compute_perm_rep_scores.py. Finished.")
