import sys,os
from src.drsoft.utils import gwamar_params_utils

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = "em59"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"
    snps_dir = dataset_dir + "snps/"
    
    res_profiles = {}
    strain_id_map = {}
    
    input_fh = open(dataset_dir + "mapping.txt")
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("_")
        if len(tokens) < 4:
            continue
        strain_id_map[tokens[3]] = tokens[0]
    input_fh.close()
    print(strain_id_map)
    
    
    text_lines = []
    
    input_fh = open(dataset_dir + "res_data_tmp.txt")
    lines = input_fh.readlines()
    line0 = lines[0].strip()
    
    for line in lines[1:]:
        tokens = line.strip("\n").split("\t")
        strain_id_tmp = tokens[0]
        if not strain_id_tmp in strain_id_map:
            print("missing: ", strain_id_tmp)
            continue
        strain_id = strain_id_map[strain_id_tmp]
        date = tokens[6]
        text_line = strain_id + "\t" + date + "\n"
        text_lines.append(text_line)
    input_fh.close()
    
    output_fh = open(dataset_dir + "res_dates.txt", "w")
    for text_line in text_lines:    
        output_fh.write(text_line)
    output_fh.close() 
    
        