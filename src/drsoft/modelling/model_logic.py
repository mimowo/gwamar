from collections import deque
from math import exp
from math import log
from src.drsoft.utils import gwamar_generator_utils
from src.drsoft.structs import res_tree

def changeProb(r, t):
    if t < 0:
        return r
    else:
        return 1.0 - exp(-r*t) 

def getstable(t=0.5, z=0.0, b=-1.0):
    score_tab = {
        "SS00": 0.0,"SS01": b  ,"SS10": 0.0,"SS11": b,
        "SR00": 0.0,"SR01": 1.0,"SR10": 0.0,"SR11": t,
        "RS00": 0.0,"RS01":-b  ,"RS10": 1.0,"RS11":-b,
        "RR00": 0.0,"RR01": t  ,"RR10": 0.0,"RR11": 0+z,
        "SI00": 0.0,"SI01": 0.5,"SI10": 0.0,"SI11": t/2,
        "RI00": 0.0,"RI01":-b  ,"RI10": 0.5,"RI11":-b/2,
        "II00": 0.0,"II01": t/2,"II10": 0.0,"II11": z/2,
        "IR00": 0.0,"IR01": 0.5,"IR10": 0.0,"IR11":-b }
    return score_tab

def stable(res_from='R', res_to='S', mut_from='0', mut_to='1', t=0.5, z=0.0, b=-1.0):
    comb_txt = res_from + res_to + mut_from + mut_to
    score_tab = getstable(t,z,b)
    return score_tab.get(comb_txt, 0.0)

def computeTopos(tree, r=False):
    for node in tree.leaves:
        if r== True and node.complete_res_state == "R":
            node.topos = "y"
        elif r== True:
            node.topos = "x"
        else:
            node.topos = "x"
        # print(node.node_id, node.topos)
    for node in tree.inner_nodes_bu:
        topo = "("
        for child in sorted(node.children, key=lambda ch:ch.topos):
            topo += child.topos
        topo += ")"
        node.topos = topo
        #print(node.node_id, node.topos)
    return tree.root.topos

def calcResCondProbabilities(phylo_tree, eps, alpha, beta):
    for node in phylo_tree.leaves:
        if hasattr(node, 'observed_res_prob_state'):
            node.cond_exp_res = node.observed_res_prob_state
            node.cond_exp_susc = 1.0 - node.cond_exp_res
        elif node.observed_res_state == 'R':
            node.cond_exp_res = 1.0-eps
            node.cond_exp_susc = eps
        elif node.observed_res_state == 'S':
            node.cond_exp_res = eps
            node.cond_exp_susc = 1.0-eps
        elif node.observed_res_state == 'I':
            node.cond_exp_res = 0.5
            node.cond_exp_susc = 0.5
        else:
            node.cond_exp_susc = 1.0
            node.cond_exp_res = 1.0
        
    for node in phylo_tree.inner_nodes_bu:    
        
        node.cond_exp_res = 0.0
        node.cond_exp_susc = 0.0
        children_list = list(node.children)
        
        if len(children_list) > 0:
            for res_subprofile in gwamar_generator_utils.generateResistanceProfiles(len(children_list)):
                prob_res = 1.0
                prob_susc = 1.0
                for j in range(len(children_list)):
                    if res_subprofile[j] == 'R':
                        prob_res *= children_list[j].cond_exp_res*(1 - changeProb(beta, children_list[j].e_len))
                        prob_susc *= children_list[j].cond_exp_res*changeProb(alpha, children_list[j].e_len)
                    elif res_subprofile[j] == 'S':
                        prob_res *= children_list[j].cond_exp_susc*changeProb(beta, children_list[j].e_len)
                        prob_susc *= children_list[j].cond_exp_susc*(1 - changeProb(alpha, children_list[j].e_len))
                    else:
                        print("error")
    
                node.cond_exp_res += prob_res
                node.cond_exp_susc += prob_susc
        
    return None

def calcResCondParentProbabilities(phylo_tree, alpha, beta):
    #queue = deque([])
    
    root = phylo_tree.root
    root.res_prob = 0.0
    
    for node in phylo_tree.inner_nodes_bu[::-1] + list(phylo_tree.leaves):
        p_node = node.parent
        if p_node == None: continue
        cp_alpha = changeProb(alpha, node.e_len)
        cp_beta = changeProb(beta, node.e_len)
        try:
            prob_res_res = node.cond_exp_res * (1-cp_beta) / (node.cond_exp_res * (1-cp_beta) + node.cond_exp_susc * (cp_beta))
            prob_susc_res = node.cond_exp_res * (cp_alpha) / (node.cond_exp_res * (cp_alpha) + node.cond_exp_susc * (1 - cp_alpha))
        except:
            prob_res_res = 1.0
            prob_susc_res = 0.0
    
        node.prob_res_res = prob_res_res
        node.prob_susc_res = prob_susc_res
        node.prob_res_susc = 1.0-prob_res_res
        node.prob_susc_susc = 1.0-prob_susc_res
        
        node.res_prob = prob_res_res * p_node.res_prob + prob_susc_res*(1-p_node.res_prob)

    return None

def calulateCompleteResProfile(phylo_tree, eps=0.02, alpha=0.05, beta=0.01):
    calcResCondProbabilities(phylo_tree, eps, alpha, beta)
    calcResCondParentProbabilities(phylo_tree, alpha, beta)
    
    return None


######################################################################


def calcMutCondProbabilities(phylo_tree, eps, psi, phi):
    for node in phylo_tree.leaves:
        if node.observed_mut_state == '1':
            node.cond_exp_pres = 1.0-eps
            node.cond_exp_abs = eps
        elif node.observed_mut_state == '0':
            node.cond_exp_pres = eps
            node.cond_exp_abs = 1.0-eps
        else:
            node.cond_exp_pres = 1.0
            node.cond_exp_abs = 1.0


    for node in phylo_tree.inner_nodes_bu:
        
        node.cond_exp_pres = 0.0
        node.cond_exp_abs = 0.0
        children_list = list(node.children)

        if len(children_list) > 0:
            for res_subprofile in gwamar_generator_utils.generateBinMutationProfiles(len(children_list)):
                prob_pres = 1.0
                prob_abs = 1.0
                for j in range(len(children_list)):
                    if str(res_subprofile[j]) == '1':
                        prob_pres *= children_list[j].cond_exp_pres*(1 - changeProb(phi, children_list[j].e_len))
                        prob_abs *= children_list[j].cond_exp_pres*changeProb(psi, children_list[j].e_len)
                    elif str(res_subprofile[j]) == '0':
                        prob_pres *= children_list[j].cond_exp_abs*changeProb(phi, children_list[j].e_len)
                        prob_abs *= children_list[j].cond_exp_abs*(1 - changeProb(psi, children_list[j].e_len))
                    else:
                        print("error")
                node.cond_exp_pres += prob_pres
                node.cond_exp_abs += prob_abs
        
    return None

def calcMutCondParentProbabilities(phylo_tree, psi, phi):
    root = phylo_tree.root
    root.mut_prob = 0.0
    
    for node in phylo_tree.inner_nodes_bu[::-1] + list(phylo_tree.leaves):
        p_node = node.parent
        if p_node == None: continue
        
        cp_psi = changeProb(psi, node.e_len)
        cp_phi = changeProb(phi, node.e_len)
        
        try:
            prob_pres_pres = node.cond_exp_pres * (1-cp_phi) / (node.cond_exp_pres * (1-cp_phi) + node.cond_exp_abs * (cp_phi))
            prob_abs_pres = node.cond_exp_pres * (cp_psi) / (node.cond_exp_pres * (cp_psi) + node.cond_exp_abs * (1 - cp_psi))
        except:
            prob_abs_pres = 0.0
            prob_pres_pres = 1.0

        node.prob_pres_pres = prob_pres_pres
        node.prob_abs_pres = prob_abs_pres
        node.prob_abs_abs = 1.0 - prob_abs_pres
        node.prob_pres_abs = 1.0 - prob_pres_pres
    
        node.mut_prob = prob_pres_pres * p_node.mut_prob + prob_abs_pres*(1-p_node.mut_prob)
        
    return None



def calulateCompleteMutProfile(phylo_tree, eps=0.001, psi=0.1, phi=0.01):
    calcMutCondProbabilities(phylo_tree, eps, psi, phi)
    calcMutCondParentProbabilities(phylo_tree, psi, phi)
    
    return None


def calcTransitionScoreExprectedValue(phylo_tree, theta=0.5, zeta=0.0, beta=-1.0, un = False):
    for node in phylo_tree.leaves:
        node.exp_score_res_abs = 0.0
        node.exp_score_res_pres = 0.0
        node.exp_score_susc_abs = 0.0
        node.exp_score_susc_pres = 0.0
    
    tstable = getstable(theta, zeta, beta)
        
    for node in phylo_tree.inner_nodes_bu:
        
        node.exp_score_res_abs = 0.0
        node.exp_score_res_pres = 0.0
        node.exp_score_susc_abs = 0.0
        node.exp_score_susc_pres = 0.0        
        
        children_list = list(node.children)
        if len(children_list) > 0:
            test_susc_pres = 0.0
            test_res_pres = 0.0
            test_susc_abs = 0.0
            test_res_abs = 0.0
            for res_subprofile in gwamar_generator_utils.generateResBinMutationProfiles(len(children_list)):
                score_res_abs = 0.0
                score_res_pres = 0.0
                score_susc_abs = 0.0
                score_susc_pres = 0.0
                prob_res_abs = 1.0
                prob_res_pres = 1.0
                prob_susc_abs = 1.0
                prob_susc_pres = 1.0
                for j in range(len(children_list)):    
                    node_j = children_list[j]  
                    
                    if res_subprofile[j] == "1R":
                        if un == True or node_j.un_subleaves_count < node_j.subleaves_count:
                            score_res_abs  += (tstable.get('RR01',0.0) + node_j.exp_score_res_pres)
                            score_res_pres += (tstable.get('RR11',0.0) + node_j.exp_score_res_pres)
                            score_susc_abs += (tstable.get('SR01',0.0) + node_j.exp_score_res_pres)
                            score_susc_pres+= (tstable.get('SR11',0.0) + node_j.exp_score_res_pres)
                        prob_res_abs   *= node_j.prob_abs_pres*node_j.prob_res_res
                        prob_res_pres  *= node_j.prob_pres_pres*node_j.prob_res_res
                        prob_susc_abs  *= node_j.prob_abs_pres*node_j.prob_susc_res
                        prob_susc_pres *= node_j.prob_pres_pres*node_j.prob_susc_res
                    elif res_subprofile[j] == "1S":
    
                        if un == True or node_j.un_subleaves_count < node_j.subleaves_count:
                            score_res_abs  += (tstable.get('RS01',0.0) + node_j.exp_score_susc_pres)
                            score_res_pres += (tstable.get('RS11',0.0) + node_j.exp_score_susc_pres)
                            score_susc_abs += (tstable.get('SS01',0.0) + node_j.exp_score_susc_pres)
                            score_susc_pres+= (tstable.get('SS11',0.0) + node_j.exp_score_susc_pres)
    
                        prob_res_abs   *= node_j.prob_abs_pres*node_j.prob_res_susc
                        prob_res_pres  *= node_j.prob_pres_pres*node_j.prob_res_susc
                        prob_susc_abs  *= node_j.prob_abs_pres*node_j.prob_susc_susc
                        prob_susc_pres *= node_j.prob_pres_pres*node_j.prob_susc_susc
                    elif res_subprofile[j] == "0R":
                        if un == True or node_j.un_subleaves_count < node_j.subleaves_count:
                            score_res_abs  += (tstable.get('RR00',0.0) + node_j.exp_score_res_abs)
                            score_res_pres += (tstable.get('RR10',0.0) + node_j.exp_score_res_abs)
                            score_susc_abs += (tstable.get('SR00',0.0) + node_j.exp_score_res_abs)
                            score_susc_pres+= (tstable.get('SR10',0.0) + node_j.exp_score_res_abs)
                        prob_res_abs   *= node_j.prob_abs_abs*node_j.prob_res_res
                        prob_res_pres  *= node_j.prob_pres_abs*node_j.prob_res_res
                        prob_susc_abs  *= node_j.prob_abs_abs*node_j.prob_susc_res
                        prob_susc_pres *= node_j.prob_pres_abs*node_j.prob_susc_res
                    elif res_subprofile[j] == "0S":
                        if un == True or node_j.un_subleaves_count < node_j.subleaves_count:
                            score_res_abs  += (tstable.get('RS00',0.0) + node_j.exp_score_susc_abs)
                            score_res_pres += (tstable.get('RS10',0.0) + node_j.exp_score_susc_abs)
                            score_susc_abs += (tstable.get('SS00',0.0) + node_j.exp_score_susc_abs)
                            score_susc_pres+= (tstable.get('SS10',0.0) + node_j.exp_score_susc_abs)
                        prob_res_abs   *= node_j.prob_abs_abs*node_j.prob_res_susc
                        prob_res_pres  *= node_j.prob_pres_abs*node_j.prob_res_susc
                        prob_susc_abs  *= node_j.prob_abs_abs*node_j.prob_susc_susc
                        prob_susc_pres *= node_j.prob_pres_abs*node_j.prob_susc_susc
    
                node.exp_score_res_abs  += score_res_abs*prob_res_abs
                node.exp_score_res_pres += score_res_pres*prob_res_pres
                node.exp_score_susc_abs += score_susc_abs*prob_susc_abs
                node.exp_score_susc_pres+= score_susc_pres*prob_susc_pres
                
                test_res_abs += prob_res_abs
                test_res_pres += prob_res_pres
                test_susc_abs += prob_susc_abs
                test_susc_pres += prob_susc_pres

    return phylo_tree.root.exp_score_susc_abs

def calcProbAllLeafResistant(phylo_tree):
    for node in phylo_tree.leaves:
        node.all_res_res = 1.0
        node.all_res_susc = 0.0
        
    for node in phylo_tree.inner_nodes_bu:
        
        node.all_res_res = 0.0
        node.all_res_susc = 0.0
        children_list = list(node.children)
        
        if len(children_list) > 0:
      
            for res_subprofile in gwamar_generator_utils.generateResistanceProfiles(len(children_list)):
                prob_res = 1.0
                prob_susc = 1.0
                for j in range(len(children_list)):
                    if res_subprofile[j] == 'R':
                        prob_res *= children_list[j].prob_res_res * children_list[j].all_res_res
                        prob_susc *= children_list[j].prob_susc_res * children_list[j].all_res_res
                    elif res_subprofile[j] == 'S':
                        prob_res *= children_list[j].prob_res_susc * children_list[j].all_res_susc
                        prob_susc *= children_list[j].prob_susc_susc * children_list[j].all_res_susc
                    else:
                        print("error")
                node.all_res_res += prob_res
                node.all_res_susc += prob_susc
    return None

def calcSubleavesResCount(phylo_tree):
    for node in phylo_tree.leaves:
        node.subleaves_count = 1

        if node.observed_res_state== 'R':
            node.res_subleaves_count = 1
            node.inter_subleaves_count = 0
            node.susc_subleaves_count = 0
            node.un_subleaves_count = 0
        elif node.observed_res_state == 'S':
            node.res_subleaves_count = 0
            node.inter_subleaves_count = 0
            node.susc_subleaves_count = 1
            node.un_subleaves_count = 0
        elif node.observed_res_state == 'I':
            node.res_subleaves_count = 0
            node.inter_subleaves_count = 1
            node.susc_subleaves_count = 0
            node.un_subleaves_count = 0
        else:
            node.res_subleaves_count = 0
            node.inter_subleaves_count = 0
            node.susc_subleaves_count = 0            
            node.un_subleaves_count = 1

    for node in phylo_tree.inner_nodes_bu:
              
        children_list = list(node.children)
        
        if len(children_list) > 0:
            node.res_subleaves_count = 0
            node.inter_subleaves_count = 0
            node.susc_subleaves_count = 0
            node.un_subleaves_count = 0
            node.subleaves_count = 0
            for j in range(len(children_list)):
                node.res_subleaves_count += children_list[j].res_subleaves_count
                node.inter_subleaves_count += children_list[j].inter_subleaves_count
                node.susc_subleaves_count += children_list[j].susc_subleaves_count
                node.un_subleaves_count += children_list[j].un_subleaves_count
                node.subleaves_count += children_list[j].subleaves_count
    
    return None 

def calcSubleavesResCountOther(phylo_tree):
    k = len(phylo_tree.root.observed_res_state_map)
    
    for node in phylo_tree.nodes.values():
        node.res_subleaves_count_map = [0]*k
        node.inter_subleaves_count_map = [0]*k
        node.susc_subleaves_count_map = [0]*k
        node.un_subleaves_count_map = [0]*k

    for node in phylo_tree.leaves:
        node.subleaves_count = 1

        for di in range(k):
            if node.observed_res_state_map[di] == 'R':
                node.res_subleaves_count_map[di] = 1
                node.inter_subleaves_count_map[di] = 0
                node.susc_subleaves_count_map[di] = 0
                node.un_subleaves_count_map[di] = 0
            elif node.observed_res_state_map[di] == 'S':
                node.res_subleaves_count_map[di] = 0
                node.inter_subleaves_count_map[di] = 0
                node.susc_subleaves_count_map[di] = 1
                node.un_subleaves_count_map[di] = 0
            elif node.observed_res_state_map[di] == 'I':
                node.res_subleaves_count_map[di] = 0
                node.inter_subleaves_count_map[di] = 1
                node.susc_subleaves_count_map[di] = 0
                node.un_subleaves_count_map[di] = 0
            else:
                node.res_subleaves_count_map[di] = 0
                node.inter_subleaves_count_map[di] = 0
                node.susc_subleaves_count_map[di] = 0            
                node.un_subleaves_count_map[di] = 1

    for node in phylo_tree.inner_nodes_bu:
             
        children_list = list(node.children)
      
        if len(children_list) > 0:
            node.subleaves_count = 0
            for j in range(len(children_list)):
                node.subleaves_count += children_list[j].subleaves_count
                
            for di in range(k):
                node.res_subleaves_count_map[di] = 0
                node.inter_subleaves_count_map[di] = 0
                node.susc_subleaves_count_map[di] = 0
                node.un_subleaves_count_map[di] = 0
                
                for j in range(len(children_list)):
                    node.res_subleaves_count_map[di] += children_list[j].res_subleaves_count_map[di]
                    node.inter_subleaves_count_map[di] += children_list[j].inter_subleaves_count_map[di]
                    node.susc_subleaves_count_map[di] += children_list[j].susc_subleaves_count_map[di]
                    node.un_subleaves_count_map[di] += children_list[j].un_subleaves_count_map[di]
    
    return None 

def calcSubleavesMutCount(phylo_tree):
    for node in phylo_tree.leaves:

        if node.observed_mut_state == '?':
            node.pres_subleaves_count = 0
            node.abs_subleaves_count = 0
            node.q_subleaves_count = 1
        elif node.observed_mut_state == '0':
            node.pres_subleaves_count = 0
            node.abs_subleaves_count = 1
            node.q_subleaves_count = 0
        else:
            node.pres_subleaves_count = 1
            node.abs_subleaves_count = 0
            node.q_subleaves_count = 0
            
    for node in phylo_tree.inner_nodes_bu:
                
        children_list = list(node.children)
      
        if len(children_list) > 0:
            node.pres_subleaves_count = 0
            node.abs_subleaves_count = 0
            node.q_subleaves_count = 0
    
            for j in range(len(children_list)):
                node.pres_subleaves_count += children_list[j].pres_subleaves_count
                node.abs_subleaves_count += children_list[j].abs_subleaves_count
                node.q_subleaves_count += children_list[j].q_subleaves_count

    return None    


def calcSubnodesCount(phylo_tree):
    for node in phylo_tree.leaves:
        node.subnodes_count = 1
        
    for node in phylo_tree.inner_nodes_bu:
        
        children_list = list(node.children)
        if len(children_list) > 0:
            node.subnodes_count = 1
            for j in range(len(children_list)):
                node.subnodes_count += children_list[j].subnodes_count
    return None    
 

def calcWeightedSuppotExprectedValue(phylo_tree, un=False):
    exp_ws = 0.0
    root = phylo_tree.root
    for node in phylo_tree.nodes.values():
        node.visited = 0
    
    for node in phylo_tree.leaves:
        if un == False and node.observed_res_state == '?':
            continue
        
        exp_w = 0.0
        
        queue = deque([])
        queue.append((node, node.parent))
        
        while queue:
            (node_j, node_i) = queue.popleft()
            if node_i == None:
                continue    
            
            children_list = list(node_i.children)
            prob_level = 0.0
            
            for res_subprofile in gwamar_generator_utils.generateResistanceProfiles(len(children_list)):
                prob_res = 1.0
                prob_susc = 1.0
                prob_all_res_other = 1.0
                
                for k in range(len(children_list)):            
                    if res_subprofile[k] == 'R':
                        prob_res *= children_list[k].prob_res_res                   
                        prob_susc *= children_list[k].prob_susc_res
                        if children_list[k].node_id == node_j.node_id:
                            prob_all_res_j = children_list[k].all_res_res
                        else:
                            prob_all_res_other *= children_list[k].all_res_res
                                                
                    elif res_subprofile[k] == 'S':
                        prob_res *= children_list[k].prob_res_susc                   
                        prob_susc *= children_list[k].prob_susc_susc                    
                        if children_list[k].node_id == node_j.node_id:
                            prob_all_res_j = children_list[k].all_res_susc
                        else:
                            prob_all_res_other *= children_list[k].all_res_susc
                    else:
                        print("error")

                prob_level += (prob_res * node_i.res_prob + prob_susc * (1 - node_i.res_prob))*prob_all_res_j*(1 - prob_all_res_other)
            exp_w += 1.0 / float(node_j.subleaves_count) * prob_level 
            queue.append((node_i, node_i.parent))
        
        exp_ws += (1-node.res_prob) * (-root.res_subleaves_count/root.susc_subleaves_count) * node.mut_prob
        exp_ws += exp_w * node.mut_prob

    return exp_ws

def calcDetParsResModel(phylo_tree):
    phylo_tree.setComepleteResProfile(['?']*phylo_tree.nodes_count)
        
    for node in phylo_tree.leaves:
        node.complete_res_state = node.observed_res_state
        
    for node_i in phylo_tree.inner_nodes_bu:
        if len(node_i.children) > 0:
            if node_i.un_subleaves_count == node_i.subleaves_count:
                node_i.complete_res_state = '?'
            elif node_i.susc_subleaves_count > 0:
                node_i.complete_res_state = 'S'
            elif node_i.inter_subleaves_count > 0:
                node_i.complete_res_state = 'I'
            else:
                node_i.complete_res_state = 'R'

    return None

def calcDetParsResModelOther(phylo_tree):
    k = len(phylo_tree.root.observed_res_state_map)
    
    for node in phylo_tree.nodes:
        node.complete_res_state_map = ["?"]*k
    
        
    for node in phylo_tree.leaves:
        for di in range(k):
            node.complete_res_state_map[di] = node.observed_res_state_map[di]
        
    for node_i in phylo_tree.inner_nodes_bu:

        if len(node_i.children) > 0:
            for di in range(k):
                if node_i.un_subleaves_count_map[di] == node_i.subleaves_count:
                    node_i.complete_res_state_map[di] = '?'
                elif node_i.susc_subleaves_count_map[di] > 0:
                    node_i.complete_res_state_map[di] = 'S'
                elif node_i.inter_subleaves_count_map[di] > 0:
                    node_i.complete_res_state_map[di] = 'I'
                else:
                    node_i.complete_res_state_map[di] = 'R'     
    return None

def calcDetParsMutModel(phylo_tree):
    phylo_tree.setComepleteMutProfile(['?']*phylo_tree.nodes_count)
        
    for node in phylo_tree.leaves:
        node.complete_mut_state = node.observed_mut_state
        
    for node_i in phylo_tree.inner_nodes_bu:
        
        if node_i.q_subleaves_count == node_i.subleaves_count:
            node_i.complete_mut_state = '?'
        elif node_i.abs_subleaves_count > 0:
            node_i.complete_mut_state = '0'
        else:
            node_i.complete_mut_state = '1'
        
    return None

def calcTransitionScore(phylo_tree, theta=0.5, zeta = 0.0, beta=-1.0, tscore_type = "P"):
    for node in phylo_tree.nodes.values():
        node.tscore_tmp = 0.0
        
    tstable = getstable(t=theta, z=zeta, b=beta)

    for node in phylo_tree.inner_nodes_bu:
        node.tscore_tmp = 0.0    
        
        node_res = node.complete_res_state
        node_mut = node.complete_mut_state

        for node_j in node.children:                
            node_j_res = node_j.complete_res_state
            node_j_mut = node_j.complete_mut_state
            
            if tscore_type == "P":
                score_tmp = tstable.get(node_res + node_j_res + node_mut + node_j_mut, 0.0)
                node.tscore_tmp += score_tmp + node_j.tscore_tmp
                #if int(node_j.node_id) in [1621,1623,1625,1622,1624,1620]:
                #    print(node.node_id, node_j.node_id, score_tmp, node_res + node_j_res + node_mut + node_j_mut)
            elif tscore_type == "A":
                if node_res == "S" and node_j_res == "R":
                    node.tscore_tmp += node_j.tscore_tmp + float(node_j.pres_subleaves_count) / float(node_j.subnodes_count)
                elif node_j_res == "S" and node_j_mut == "1":
                    #print("here2", node_res, node_j_res, node_mut, node_j_mut)
                    node.tscore_tmp += node_j.tscore_tmp - 1.0
                else:
                    node.tscore_tmp += node_j.tscore_tmp
#                if node_mut == "0" and node_j_mut == "1":
#                    node.tscore_tmp  += ((float(node_j.susc_subleaves_count)* (-2.0) + float(node_j.res_subleaves_count)) / float(node_j.un_subleaves_count + node_j.susc_subleaves_count + node_j.res_subleaves_count) + node_j.tscore_tmp)
#                else:
#                    node.tscore_tmp  += node_j.tscore_tmp
            elif tscore_type == "C":
                node.tscore_tmp  += (tstable.get(node_res+node_j_res+node_mut+node_j_mut,0.0) * (1.0+log(node_j.subnodes_count)) + node_j.tscore_tmp)
            elif tscore_type == "L":
                sc_tmp = tstable.get(node_res+node_j_res+node_mut+node_j_mut,0.0)
                if sc_tmp >= 0:
                    node.tscore_tmp  += (log(1+sc_tmp) + node_j.tscore_tmp)
                else:
                    node.tscore_tmp  += (log(0.5) + node_j.tscore_tmp)
            elif tscore_type == "E":
                node.tscore_tmp  += (tstable.get(node_res+node_j_res+node_mut+node_j_mut,0.0)*exp(-100*node_j.e_len) + node_j.tscore_tmp)
            elif tscore_type == "R":
                node.tscore_tmp  += (tstable.get(node_res+node_j_res+node_mut+node_j_mut,0.0) - node_j.e_len + node_j.tscore_tmp)                
            else:
                node.tscore_tmp  += (tstable.get(node_res+node_j_res+node_mut+node_j_mut,0.0) + node_j.tscore_tmp)
        
    return phylo_tree.root.tscore_tmp

def calcTransitionScoreMulti(phylo_tree, theta=0.5, zeta = 0.0, tscore_type = "P"):
    k = len(phylo_tree.root.complete_res_state_map) 
        
    for node in phylo_tree.nodes.values():
        node.tscore_tmp = 0.0
                
    for node in phylo_tree.inner_nodes_bu:

        node.tscore_tmp = 0.0    
        
        children_list = list(node.children)
        node_res = node.complete_res_state
        node_mut = node.complete_mut_state

        for j in range(len(children_list)):    
            node_j = children_list[j]  
                        
            node_j_res = node_j.complete_res_state
            node_j_mut = node_j.complete_mut_state
            
            other_score = 0.0
            
            if node_mut == "0" and node_j_mut == "1":                
                for di in range(k):
                    node_res_di = node.complete_res_state_map[di]
                    node_j_res_di = node_j.complete_res_state_map[di]
                    if node_res_di == "S" and node_j_res_di == "R":
                        other_score += 1.0
                    elif node_res_di == "R" and node_j_res_di == "R":
                        other_score += 0.5
                        
            node.tscore_tmp  += stable(node_res, node_j_res, node_mut, node_j_mut, theta, zeta) / (other_score + 1.0) + node_j.tscore_tmp
        
    return phylo_tree.root.tscore_tmp

def removeUnResNodeLeavesRes(node):     
    if node.subleaves_count == node.un_subleaves_count:
        return None
    elif len(node.children) == 0:
        newnode = res_tree.ResTreeNode(node.node_id)
        newnode.e_len = node.e_len
        newnode.label = node.label
        newnode.ecolor = node.ecolor
        newnode.observed_res_state = node.observed_res_state
    #    newnode.observed_mut_state = node.observed_mut_state
        return newnode
    else:
        child_new_nodes = []
        for child_node in node.children:
            child_newnode = removeUnResNodeLeavesRes(child_node)
            if child_newnode != None:
                child_new_nodes.append(child_newnode)
        if len(child_new_nodes) == 0:
            return None
        elif len(child_new_nodes) == 1:
            return child_new_nodes[0]
        else:
            newnode = res_tree.ResTreeNode(node.node_id)
            newnode.e_len = node.e_len
            newnode.ecolor = node.ecolor
            newnode.children = child_new_nodes
            for child_newnode in child_new_nodes:
                child_newnode.parent = newnode
        return newnode
        
    return newnode
    
def removeUnResLeaves(phylo_tree):
    newtree = res_tree.ResTree(phylo_tree.root.subleaves_count - phylo_tree.root.un_subleaves_count)
    newtree.root = removeUnResNodeLeavesRes(phylo_tree.root)
    newtree.root_id = phylo_tree.root_id
    
    queue = deque([])
    queue.append(newtree.root)
    
    newtree.leaves_count = 0
    newtree.leaf_ids = []
    newtree.leaves = []
    
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        
        newtree.nodes[node.node_id] = node
        if len(node.children) == 0:
            newtree.leaf_ids.append(node.node_id)
            newtree.leaves.append(node)
        for child_node in node.children:
            queue.append(child_node)
    
    newtree.leaves_count = len(newtree.leaf_ids)    
    newtree.nodes_count = len(newtree.nodes)
    return newtree
    
