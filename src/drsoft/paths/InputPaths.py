import os
datasets = "datasets"
app_dir = os.path.abspath(".")


def get_dataset(ds):
  return os.path.join(datasets, ds)

def get_input(ds):
  return os.path.join(get_dataset(ds), "input")
