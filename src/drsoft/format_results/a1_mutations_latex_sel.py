import os
import sys
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_bin_utils, gwamar_utils,\
  gwamar_res_io_utils, gwamar_progress_utils

colors_map = {"?": "gray", "0": "white", "1": "orange", "2": "blue",
               "3": "red", "4":"violet", "5": "green", "6": "brown",
               "7": "pink", "8": "purple"}

def saveMutsLatex(params):
    drug_name = params["DRUG_NAME"]
    dataset = params["D"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    analysis_comb_sel_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_latex_sel_dir = parameters["RESULTS_A_LATEX_S_SEL_DIR"]

    input_fn = analysis_comb_sel_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    line0 = lines[0]
    dr_profile = line0.strip().split("\t")[0]
    input_fh.close()

    mut_profiles = []
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_desc_full = tokens[-1]
        gene_id, position = mut_desc_full.split(" ")[2:4]
        mut_profiles.append((bin_profile, gene_id, position))

    mut_profiles_sorted = sorted(mut_profiles, key=lambda bin_profile_gene_id_position: (-gwamar_bin_utils.countNonZeros(bin_profile_gene_id_position[0]), abs(int(bin_profile_gene_id_position[2])-440), bin_profile_gene_id_position[1]))
    
    tls = []
    tls.append("\\documentclass[10pt]{article}\n")
    tls.append("\\usepackage{tikz}\n")
    if dataset == "broad_mtb":
        tls.append("\\usepackage[margin=0.2in, paperwidth=12in, paperheight=15in]{geometry}\n")
    else:
        tls.append("\\usepackage[margin=0.2in, paperwidth=3in, paperheight=7in]{geometry}\n")
    tls.append("\\begin{document}\n")
    tls.append("\\begin{tikzpicture}[x=0.5pt,y=0.5pt]\n")

    y = 20
    mlen = 5
    xoffset = 9*mlen + 100
    jm = min(100, len(mut_profiles)-1)

    tls.append("\\node [right] at (" + str(0) + "," + str(10 + jm*y) + ") {" + drug_name + "};\n")
    for i in range(len(dr_profile)):
        if dr_profile[i] == "R": c = "red"
        elif dr_profile[i] == "S": c = "green"
        elif dr_profile[i] == "I": c = "yellow"
        else: c = "gray"

        tl = "\\draw [color=" + c + "] (" + str(xoffset+i) + "," + str(jm*y)+ ") -- (" + str(xoffset+i) + "," + str((jm+1)*y) + ");\n"  
        tls.append(tl)

    #len(lines)
    for j in range(jm, 0, -1):
        jl = jm - j
        
        bin_profile, gene_id, position = mut_profiles_sorted[jl]
    
        tls.append("\\node [right] at (" + str(0) + "," + str(10 + (j-1)*y) + ") {" + gene_id + " " + str(position) + "};\n")
        
        for i in range(len(bin_profile)):
            c = colors_map.get(bin_profile[i], "black")

            if bin_profile[i] == "0":
                continue
            tl = "\\draw [color=" + c + "] (" + str(xoffset+i) + "," + str((j-1)*y)+ ") -- (" + str(xoffset+i) + "," + str((j)*y) + ");\n"  
            tls.append(tl)

    tls.append("\\end{tikzpicture}\n")
    tls.append("\\end{document}\n")

    output_fn = analysis_latex_sel_dir + "/" + drug_name + ".tex"
    output_fh = open(output_fn, "w")
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()

    os.chdir(analysis_latex_sel_dir)
    command = "pdflatex " + output_fn
    os.system(command)


    return drug_name

if __name__ == '__main__':

 
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    analysis_latex_sel_dir = parameters["RESULTS_A_LATEX_S_SEL_DIR"]
    analysis_latex_dir = parameters["RESULTS_A_LATEX_S_DIR"]

    gwamar_utils.ensure_dir(analysis_latex_sel_dir)
    gwamar_utils.ensure_dir(analysis_latex_dir)

    
    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(analysis_comp_dir + drug_name)
        TASK["DRUG_NAME"] = drug_name
        TASK["D"] = parameters["D"]

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Combined")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap(saveMutsLatex, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveMutsLatex(T)
            progress.update(str(r))
