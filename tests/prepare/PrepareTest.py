# -*- coding: utf-8 -*-

import unittest
import shutil
import sys
import os
from src.drsoft.paths import InputPaths

sys.path += [os.path.abspath(os.curdir)]

from tests._utils import TimestampUtils
from src.drsoft.prepare import Prepare
from src.drsoft.utils import gwamar_params_utils

class GeniusProcessingTest(unittest.TestCase):

  def setUp(self):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    self.dataset = "mtu173"
    self.target = TimestampUtils.createForTests("PrepareTest")
    
    src_ds = os.path.join(parameters["GWAMAR_DATASETS"], "mtu173")
    dst_ds = os.path.join(parameters["GWAMAR_DATASETS"], self.target)
    src_in = os.path.join(src_ds, "input")
    dst_in = os.path.join(dst_ds, "input")
    self.dst_res = os.path.join(dst_ds, "exP")
    self.profiles_fn = os.path.join(self.dst_res, "bin_profiles_P_det.txt")
    shutil.copytree(src_in, dst_in)
    print(self.profiles_fn)

  def test_prepare(self):
    parameters = gwamar_params_utils.overwriteParameters(sys.argv)
    parameters["D"] = self.target
    parameters = gwamar_params_utils.readParameters()

    Prepare.prepare(parameters)
    
    print(self.profiles_fn)
    self.assertTrue(os.path.exists(self.profiles_fn))

if __name__ == '__main__':
    unittest.main()
