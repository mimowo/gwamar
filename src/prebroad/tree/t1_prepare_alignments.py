import sys,os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_utils, gwamar_ann_utils,\
  gwamar_strains_io_utils, gwamar_muts_io_utils

def savePHYLIPalignements(alns, output_fh, sorting = None):
    if not "OUTSTRAIN" in alns:
        if sorting == None:
            strains_ord = list(alns)
        else:
            strains_ord = sorted(alns, key=sorting)
    else:
        if sorting == None:
            strains_ord = ["OUTSTRAIN"] + list(set(alns.keys()) - set(["OUTSTRAIN"]))
        else:
            strains_ord = ["OUTSTRAIN"] + sorted(set(alns.keys()) - set(["OUTSTRAIN"]), key=sorting)
                
    strain_f_id = strains_ord[0]
    aln_len = len(alns[strain_f_id])
    output_fh.write(str(len(alns))+" " + str(aln_len) + "\n")
    combined_lines = {}
    for strain_id in strains_ord:
        combined_lines[strain_id] = ""
        
    for i in range(0, aln_len, 50):
        for strain_id in strains_ord:
            if i < 50: 
                if strain_id != "OUTSTRAIN":
                    combined_lines[strain_id] = gwamar_utils.shift(strain_id[:10], 10) + " "
                else:
                    combined_lines[strain_id] = gwamar_utils.shift("OUTSTRAIN", 10) + " "
            else:
                combined_lines[strain_id] = gwamar_utils.shift(" ", 10) + " "
            combined_lines[strain_id] += alns[strain_id][i:min(i+10, aln_len)] + " "
            if i+10 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+10:min(i+20, aln_len)] + " "
            if i+20 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+20:min(i+30, aln_len)] + " "
            if i+30 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+30:min(i+40, aln_len)] + " "
            if i+40 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+40:min(i+50, aln_len)] + " "
        for strain_id in strains_ord:
            output_fh.write(combined_lines[strain_id] + "\n")
        output_fh.write("\n")
        for strain_id in strains_ord:
            combined_lines[strain_id] = ""
    return None

if __name__ == '__main__':
    print("t1_prepare_alignments.py. Start.")
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    m_type = parameters["PREBROAD_SUB"]
    input_download_dir = parameters["PREBROAD_DOWNLOAD_DIR"]
    phylo_dir = parameters["PREBROAD_TREE_DIR"]
    input_dir = parameters["PREBROAD_OUTPUT_DIR"]
    
    if os.path.exists(input_dir + "/cluster_gene_ref_ids.txt"):
        cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
        cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh)
        cluster_fh.close()
    else:
        cluster_ids = {} 
        cluster_ids_rev = {}
        
    input_fh = open(input_dir + "/strains.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
        
    input_fh = open(phylo_dir + "point_mutations_nt.txt")
    gene_profiles = gwamar_muts_io_utils.readPointMutations(input_fh, cluster_ids, cluster_ids_rev, gene_subset=None, m_type="a")
    input_fh.close()
    
    mutations_seqs = [""]*strains.count()
    
    for gene_id in sorted(gene_profiles):
        gp = gene_profiles[gene_id]
        if m_type == "sub":
            if gp.gene_id in ["Rv0006", "Rv0667", "Rv0682", "Rv1908c", "Rv3795", "Rvnr01", "Rvnr02"]: 
                continue
        
        for m_pos in sorted(gp.mutations):
            mutation = gp.mutations[m_pos]
            fp = mutation.full_profile

            nuc_counts = {}
            for i in range(strains.count()):
                state = fp[i]         
            
                if not state in nuc_counts: nuc_counts[state] = 0
                nuc_counts[state] += 1
            
            if len(nuc_counts) != 2: continue
            
            count_diff = 0
            for nuc in nuc_counts:
                if nuc_counts[nuc] >= 2:
                    count_diff += 1
                    
            if count_diff == 2:
                for i in range(strains.count()):
                    mutations_seqs[i] += fp[i]
    
    mutations_map = {}
    for i in range(strains.count()):
        strain_id = strains.getStrain(i)
        mutations_map[strain_id] = mutations_seqs[i]
    
    output_fh = open(phylo_dir + 'alignment_'+m_type+'.txt', "w");
    savePHYLIPalignements(mutations_map, output_fh, sorting = lambda strain_id:strain_id)
    output_fh.close()
    
    mutations_cluster_map = {}
    
    cluster_ids = {}
    cluster_ids_rev = {}
    cluster_seqs = {}
    cluster_seqs_rev = {}
    
    cluster_id = 0
    for strain_id in mutations_map:
        seq = mutations_map[strain_id]
        if seq in cluster_seqs_rev:
            cluster_id_ex = cluster_seqs_rev[seq]
            cluster_ids_rev[strain_id] = cluster_seqs_rev[seq]
            cluster_ids[cluster_id_ex].add(strain_id)
        else:
            cluster_id += 1
            cluster_seqs_rev[seq] = cluster_id
            cluster_ids_rev[strain_id] = cluster_id
            cluster_ids[cluster_id] = set([strain_id])
            cluster_seqs["c"+str(cluster_id)] = seq
            
    output_fh = open(phylo_dir + 'alignment_cluster_'+m_type+'.txt', "w");
    savePHYLIPalignements(cluster_seqs, output_fh, sorting = lambda cluster_id_str:int(cluster_id_str[1:]))
    output_fh.close()

    output_fh = open(phylo_dir + 'clusters_map_'+m_type+'.txt', "w");
    for cluster_id in sorted(cluster_ids):
        text_line = "c" + str(cluster_id) + "\t"
        for strain_id in cluster_ids[cluster_id]:
            text_line += strain_id + " "
        text_line = text_line[:-1] + "\n"
        output_fh.write(text_line)
    output_fh.close()

    print("t1_prepare_alignments.py. Finished.")
    