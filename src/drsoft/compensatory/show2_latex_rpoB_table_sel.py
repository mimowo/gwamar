import os
import sys
from src.drsoft.utils import gwamar_params_utils, gwamar_utils,\
  gwamar_strains_io_utils, gwamar_res_io_utils, gwamar_muts_utils

def readCompMutations2(input_fh):
    comp_mutations = {}
    source = ""
    gene_name = ""
    sources = []

    lines = input_fh.readlines()
    for line in lines:
        line = line.strip()
        line = line.split("#")[0].strip()
        if len(line) == 0:
            continue
        if line.startswith("%"):
            source = line[1:]
            if not source in sources:
                sources.append(source)
        elif line.startswith(">"):
            gene_name = line[1:].split()[0]
            if not gene_name in comp_mutations:
                comp_mutations[gene_name] = {}
        else:
            tokens = line.split()
            if len(tokens) < 3:
                continue
            position = int(tokens[0].split("|")[0])
            ref_state = tokens[1][0]
            if not (position, ref_state) in comp_mutations[gene_name]:
                comp_mutations[gene_name][(position, ref_state)] = {}
            comp_mutations[gene_name][(position, ref_state)][source] = tokens[2]
    return comp_mutations, sources


gene_shift = {"rpoA":50, "rpoB":300, "rpoC": 550}

gene_length = {"rpoA": 347, "rpoB": 1172, "rpoC": 1314}

source_color = {"comas_whole-genome_2012": "red", "de_vos_putative_2013":"blue", "casali_evolution_2014":"black",
 "mtu173":"orange", "broad_mtb":"violet", "brandis_fitness-compensatory_2012": "pink",
 "brandis_genetic_2013": "purple"}
sourse_shifts = {"comas_whole-genome_2012": 0, "de_vos_putative_2013": 10, "casali_evolution_2014": 20,
        "mtu173":30, "broad_mtb":40, "brandis_fitness-compensatory_2012":50, "brandis_genetic_2013":60
        }

x_offset = 10

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    stats_dir = parameters["STATS_DIR"]
    latex_dir = parameters["LATEX_DIR"]
    comp_dir = parameters["COMP_DATA"]

    gwamar_utils.ensure_dir(stats_dir)
    gwamar_utils.ensure_dir(latex_dir)

    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()

    input_fh = open(comp_dir + "/comp_data.txt")
    comp_mutations, sources = readCompMutations2(input_fh)
    input_fh.close()

    res_profiles_sorted = sorted(res_profiles_list, key=lambda drp: -len(drp.getKnown()))

    tls = []
    tls.append("\\documentclass[10pt]{article}\n")
    tls.append("\\usepackage{tikz}\n")
    tls.append("\\usepackage{booktabs}\n")
    
    tls.append("\\usepackage[margin=0.2in, paperwidth=8in, paperheight=25in]{geometry}\n")
    tls.append("\\usepackage{fixltx2e}\n")
    tls.append("\\usepackage{xcolor}\n")
    tls.append("\\begin{document}\n")

    tls.append("\\scriptsize\n")
    tls.append("\\begin{tabular}{ c c c c c c c c }\n")
    tls.append("\\toprule \n")
    tl = " gene & position & ref aa "
    for source in sources:
        tl += " & " + source.replace("_", " ")
    tl += " \\\\ \\midrule\n"
    tls.append(tl)

    for gene_name in ["rpoA", "rpoB", "rpoC"]:
        for (position, ref_state) in sorted(comp_mutations[gene_name]):
            rrdr = False
            color = "black"
            if gene_name=="rpoB" and position <= 452 and position >= 426:
                rrdr = True
                color = "red"
            count = 0
            for source in sources:
                if source in comp_mutations[gene_name][(position, ref_state)]:
                    count += 1
            if count <= 1:
                continue

            tl = " & ".join([gene_name, str(position), ref_state[0]])
            for source in sources:
                if source in comp_mutations[gene_name][(position, ref_state)]:
                    mut_txt = comp_mutations[gene_name][(position, ref_state)][source]
                    tl  += " & " + "\\textcolor{"+color+"}{" + gwamar_muts_utils.mutsDescToLatex(mut_txt) +"}"
                else:
                    tl  += " & "
            tl += "\\\\ \n"

            #if not rrdr:
            tls.append(tl)
        if gene_name != "rpoC":
            tls.append("\\midrule\n")
    tls.append("\\bottomrule\n")
    tls.append("\\end{tabular}\n")

    tls.append("\\end{document}\n")

    output1_fn = comp_dir + "/tmp_comp_rpoB_table2.tex"

    output_fh = open(output1_fn, "w")
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()

    os.chdir(comp_dir)
    command = "pdflatex " + output1_fn
    print(command)
    os.system(command)

    output2_fn = comp_dir + "/tmp_comp_rpoB_table2.pdf"
    output3_fn = comp_dir + "/comp_rpoB_table2.pdf"

    command = "pdfcrop --margins '5 5 5 5' " + output2_fn + " " + output3_fn
    os.system(command)

    command = "acroread " + output3_fn
    os.system(command)

