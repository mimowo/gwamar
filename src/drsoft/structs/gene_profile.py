class GeneProfile:
    def __init__(self, cluster_id, gene_id, gene_name,ref_start=-1,ref_end=-1,ref_strand="?", full_profile=None):
        self.cluster_id = cluster_id
        self.gene_id = gene_id
        self.gene_name = gene_name
        self.mutations = {}
        self.ref_start = ref_start
        self.ref_end = ref_end
        self.ref_strand = ref_strand
        self.full_profile = full_profile