import os
import sys

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_tree_io_utils

if __name__ == '__main__':
  print("p0_save_strains_ordered.py: saves strains in the order consistent with the phylogenetic tree. Start.")
  gwamar_params_utils.overwriteParameters(sys.argv)
  parameters = gwamar_params_utils.readParameters()
  
  input_dir = parameters["DATASET_INPUT_DIR"]

  input1_fn = input_dir + "/tree.txt"
  input2_fn = input_dir + "/strains_ordered.txt"
  input3_fn = input_dir + "/strains.txt"

  if os.path.exists(input1_fn) and not os.path.exists(input2_fn) and os.path.exists(input3_fn):
    input_fh = open(input1_fn)
    tree_line = input_fh.readline()
    input_fh.close()
    strains_list = gwamar_tree_io_utils.getTreeStrainList(tree_line)

    output_fh = open(input2_fn, "w")
    for strain_id in strains_list:
      output_fh.write(strain_id + "\n")
    output_fh.close()
  elif not os.path.exists(input1_fn) and os.path.exists(input3_fn): 
    input_fh = open(input3_fn)
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    strains_list = strains.allStrains()

    output_fh = open(input2_fn, "w")
    for strain_id in strains_list:
      output_fh.write(strain_id + "\n")
    output_fh.close()
  else:
    print("File " + input2_fn + " already exists. First remove it.")
  
  print("p0_save_strains_ordered.py. Finished.")

