import os
import sys
import filecmp
from src.drsoft.utils import gwamar_params_utils

from drsoft.utils.gwadrib_params_utils import *

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    data_dir = parameters["GWADRIB_DATASETS"]
    
    input1 = data_dir + "broad_mtb/snps/"
    input2 = data_dir + "broad_mtb2/snps/"
    print(input1)
    print(input2)
    
    for strain_id in os.listdir(input1):
        input1_fn = input1 + "/" + strain_id + "/" + strain_id + ".snps.annotated"
        input2_fn = input2 + "/" + strain_id + "/" + strain_id + ".snps.annotated"
        
        ret = filecmp.cmp(input1_fn, input2_fn)
        if not ret:
            print(ret, input1_fn)


