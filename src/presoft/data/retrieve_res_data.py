import sys
import os
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils

def loadDrugsATC(input_fh):
    #drugs_list = []
    drugs_map = {}
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens) == 0:
            continue
        drug_name = tokens[0]
        atc_name = tokens[1]
        if atc_name == "x":
            continue
        if not drug_name in drugs_map:
            drugs_map[drug_name] = atc_name
    drugs_list = sorted(drugs_map, key=lambda dn:drugs_map[dn])
    return drugs_list, drugs_map
    
def loadDrugsFromData(input_fh):
    drugs_list = []
    
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens) > 0:
            drug_name = tokens[0]
            strain_id = tokens[1]
            res_info = tokens[2]
            cite = tokens[3]
            if not drug_name in drugs_list:
                drugs_list.append(drug_name)
    
    return drugs_list

def loadResData(input_fh):
    res_data_map = {}
    
    for line in input_fh.readlines():
        if not line:
            continue
        tokens = line.strip().split("\t")
        
        if len(tokens) == 4:
            try:
                print("here")
                drug_name = tokens[0]
                if len(drug_name) > 0 and drug_name[0] == "#":
                    continue
                
                strain_id = tokens[1].replace("/", "")
                res_info = tokens[2]
                cite = tokens[3]
                if not strain_id in res_data_map:
                    res_data_map[strain_id] = {}
                if not drug_name in res_data_map[strain_id]:
                    res_data_map[strain_id][drug_name] = set([])
                res_data_map[strain_id][drug_name].add((res_info, cite))
            except:
                print("ERROR", line)
        elif len(tokens) == 5:
            
            try:
                print("here2")
                drug_name = tokens[0]
                if len(drug_name) > 0 and drug_name[0] == "#":
                    continue
                
                strain_id = tokens[1].replace("/", "")
                res_info = tokens[2] + ":" + tokens[3]
                cite = tokens[4]
                if not strain_id in res_data_map:
                    res_data_map[strain_id] = {}
                if not drug_name in res_data_map[strain_id]:
                    res_data_map[strain_id][drug_name] = set([])
                res_data_map[strain_id][drug_name].add((res_info, cite))
            except:
                print("ERROR", line)   
    return res_data_map

def translateDataIntoResProfiles(res_data):
    res_profiles = {}
    for strain_id in res_data:
        for drug_name in res_data[strain_id]:
            res_count = 0 
            susc_count = 0
            int_count = 0 
            unk_count = 0
                
            if not drug_name in res_profiles:
                res_profiles[drug_name] = {}
                res_profiles[drug_name]["R"] = set([])
                res_profiles[drug_name]["S"] = set([])
                res_profiles[drug_name]["I"] = set([])            
            for (drug_res_info, cite_info) in res_data[strain_id][drug_name]:
                tokens = drug_res_info.split(":")
                
                if tokens[0] == "R":
                    res_count += 1 
                elif tokens[0] == "S":
                    susc_count += 1
                elif tokens[0] == "I":
                    int_count += 1
                elif tokens[0] == "U":
                    unk_count += 1
            if res_count > 0 and susc_count == 0:
                res_profiles[drug_name]["R"].add(strain_id)
            elif res_count == 0 and susc_count > 0:
                res_profiles[drug_name]["S"].add(strain_id)
            elif res_count > 0 and susc_count > 0:
                pass
            elif int_count >0:
                res_profiles[drug_name]["I"].add(strain_id)
            elif unk_count > 0:
                pass
            else:
                pass
                                  
    return res_profiles

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = "str238"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset) + "/"
    
    input_fh = open(dataset_dir + "strains-all.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(dataset_dir + "res-data.txt")
    res_data = loadResData(input_fh)
    input_fh.close()
    
    res_profiles = translateDataIntoResProfiles(res_data)
    
    text_lines = []
    
    for drug_name in res_profiles:
        
        print(drug_name)
        res_profile = res_profiles[drug_name]
        
        if len(res_profile["R"]) < 3 or len(res_profile["S"]) < 3:
            continue

        text_line = drug_name + "\t" 
        for strain_id in sorted(res_profile["R"]):
            if strain_id in res_profile["S"]:
                continue
            text_line += strain_id+";"
        if text_line.endswith(";"):
            text_line = text_line[:-1] + "\t"
        else:
            text_line += "\t"

        for strain_id in sorted(res_profile["S"]):
            if strain_id in res_profile["R"]:
                continue
            text_line += strain_id+";"
        if text_line.endswith(";"):
            text_line = text_line[:-1] + "\t"
        else:
            text_line += "\t"

        for strain_id in sorted(res_profile["I"]):
            if strain_id in res_profile["R"]:
                continue
            text_line += strain_id+";"
        if text_line.endswith(";"):
            text_line = text_line[:-1] + "\t"
        else:
            text_line += "\t"
            
        text_line += "\n"
        
        text_lines.append(text_line)
        
    output_fh = open(dataset_dir + "res_profiles_ret_new.txt", "w")
    for text_line in text_lines:
        output_fh.write(text_line)
    output_fh.close()
    
    
    
    
    
        