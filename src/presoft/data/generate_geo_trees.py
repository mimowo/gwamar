import sys
import os
from src.drsoft.utils import gwamar_params_utils, gwamar_tree_io_utils

def loadGeoData(input_fh):
    geo_data_map = {}

    lines = input_fh.readlines()
    for line in lines:
        line = line.strip()
        if line.startswith("#") or len(line) < 2:
            continue
        tokens = line.split("\t")
        if len(tokens) < 1: continue
        elif len(tokens) in [1,2]:
            strain_id = tokens[0]
            country = "Unknown"
            continent = "Unknown"
        else:
            strain_id = tokens[0]
            country = tokens[1]
            continent = tokens[2]
        geo_data_map[strain_id]= (country, continent)
    return geo_data_map
        
# def drugsList(res_data):
#     drugs_map = {}
#     for (strain_id, drug_name) in res_data:
#         if not drug_name in drugs_map:
#             drugs_map[drug_name] = 0
#         drugs_map[drug_name] += 1
#     drugs_list = sorted(drugs_map, key=lambda drug_name:-drugs_map[drug_name])
#     return drugs_list

# def strainsList(res_data):
#     strains_set = set([])
#     for (strain_id, drug_name) in res_data:
#         strains_set.add(strain_id)
#     strains_list = sorted(strains_set)
#     return strains_list

bgcolor_map = {
    "Asia":"red",
    "Europe":"green",
    "North America": "yellow",
    "Central America": "blue",
    "South America": "orange",
    "Australia": "pink",
    "Africa": "purple"
    }

def generateGeoTrees(output_fh, tree, geo_data):
    tls = []

    tls.append("digraph "+ "G" + " {\n")

    tree.calcInnerNodesBU()

    for node in tree.nodes.values():

        if node.is_leaf():
            country, continent = geo_data.get(node.label, ("",""))
            print(country, continent)
            color ="black"
            bgcolor = bgcolor_map.get(continent, "gray")
        else:
            color = "black"
            bgcolor = "white"


        pdf_label_tmp = str(node.label).replace("_", "").replace("-","")
        if pdf_label_tmp[0] in ["0","1","2","3","4","5","6","7","8","9"]:
            pdf_label = "n"+pdf_label_tmp
        else: pdf_label = pdf_label_tmp            
        tls.append(str(node.node_id) + " [label="+pdf_label+",style=filled,color="+color+",fillcolor=" + bgcolor + "];\n")

    for node in tree.inner_nodes_bu: 
        for node_c in node.children:
            tls.append(str(node.node_id) + "->" + str(node_c.node_id) + ";\n")

    tls.append("}\n")

    for tl in tls:
        output_fh.write(tl + "\n")
    return None

if __name__ == '__main__':
    dataset = "mtu"
    tree_ids= ["t1", "t2"]

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    gwadrib_dir = parameters["GWADRIB_PATH"]
    input_dir = gwadrib_dir + "/geo_data/" + dataset + "/"

    print("xxx")
    input_fh = open(input_dir + "/geo-data.txt")
    geo_data = loadGeoData(input_fh)
    input_fh.close()
    print("yyy")

    for tree_id in tree_ids:

        input_fh = open(input_dir + "/"+tree_id+".txt")
        trees = gwamar_tree_io_utils.readPhyloTrees(input_fh)
        input_fh.close()

        tree = trees[0]

        output_dot_fn = input_dir + "/geo-tree-"+tree_id+".dot"
        output_pdf_fn = os.path.relpath(input_dir + "/geo-tree-"+ tree_id+ ".pdf")
        
        output_fh = open(output_dot_fn, "w")
        generateGeoTrees(output_fh, tree, geo_data)
        output_fh.close()

        os.system("dot -Tpdf " + output_dot_fn + " -o " + output_pdf_fn)
