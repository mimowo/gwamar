import sys
import os
from multiprocessing import Pool
from src.drsoft.utils import gwamar_params_utils, gwamar_ann_utils,\
  gwamar_muts_io_utils, gwamar_comp_utils, gwamar_stat_utils, gwamar_utils,\
  gwamar_res_io_utils, gwamar_progress_utils
from src.drsoft.structs import rel_graph

def readDrugResAssoc(input_fh, drug_name = ""):
    drug_res_assoc = {}
    for line in input_fh.readlines():
        if line.startswith("#"): continue

        tokens = line.strip().split()
        if len(tokens) < 5: continue

        assoc = tokens[0]
        dn = tokens[1]
        if dn != drug_name: continue
        gene = tokens[2]
        position = tokens[4]
        drug_res_assoc[(gene,position)] = assoc
    return drug_res_assoc

def saveScoresCombined(params):
    drug_name = params["DRUG_NAME"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    input_dir = parameters["DATASET_INPUT_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    gold_dir = parameters["DATASET_GOLD_DIR"]

    if os.path.exists(input_dir + "/cluster_gene_ref_ids.txt"):
        cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
        cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh, gene_ref_ids=True)
        cluster_fh.close()
    else:
        cluster_ids = {}
        cluster_ids_rev = {}

    input_fn = gold_dir + "/gold_assocs_H.txt"
    if not os.path.exists(input_fn):
        print("File does not exist:" + input_fn)
        drug_res_assoc = {}
    else:
        input_fh = open(input_fn)
        drug_res_assoc = gwamar_muts_io_utils.readGoldAssociations(input_fh, cluster_ids_rev)
        input_fh.close()
    
    print(drug_res_assoc)
        
    # input_fh = open(input_fn)
    # drug_res_assoc = readDrugResAssoc(input_fh, drug_name=drug_name)
    # input_fh.close()
    # print(drug_res_assoc)

    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    gene_muts, rdr_sets = gwamar_comp_utils.selectPutativeComp(input_fh)
    input_fh.close()

    nodes_map = {}
    snodes = set([])
    ncount = 0
    rel_graph = rel_graph.RelGraph()

    for gene_name in gene_muts.keys():
        gene1_muts_list = gene_muts[gene_name]
        for gene1_mut in gene1_muts_list:
            position1, mut1_desc, mut1_profile = gene1_mut
            gene_id, cluster_id, gene_name, position_str, ref_aa, mut_aa = mut1_desc.split()
            mut_set = gwamar_comp_utils.getBinSet(mut1_profile)
            nlabel = " ".join([gene_name[3], position_str, ref_aa, mut_aa])

            if gwamar_comp_utils.isMutInRRDR(mut1_desc):
                ncolor = "red"
            else:
                ncolor = "blue"
            node = rel_graph.RelGraphNode(str(ncount), label=nlabel, ncolor=ncolor, mprofile=mut1_profile)
            if ncolor == "red": 
                snodes.add(node)
            nodes_map[node.node_id] = node
            ncount += 1
            rel_graph.addNode(node)

    for node1_id in nodes_map.keys():
        node1 = nodes_map[node1_id]
        m1_profile = node1.mprofile
        m1_set = gwamar_comp_utils.getBinSet(m1_profile)
        for node2_id in nodes_map.keys():
            if node1_id == node2_id:
                continue
            
            node2 = nodes_map[node2_id]
            m2_profile = node2.mprofile
            m2_set = gwamar_comp_utils.getBinSet(m2_profile)

            N = len(m1_profile)
            n = len(m1_set)
            m = len(m2_set)
            k = len(m1_set & m2_set)

            pvalue = gwamar_stat_utils.hyperTestPvalue(N, m, n, k)
            
            if m1_set.issubset(m2_set) and len(m1_set) > 1:
                elabel = str(len(m1_set & m2_set))
                edge = rel_graph.RelGraphEdge(node2, label=elabel, ecolor="blue")
                edge_rev = rel_graph.RelGraphEdge(node1, label=elabel, ecolor="blue")
                node1.addEdge(edge)
                node2.addEdgeRev(edge_rev)

    gwamar_utils.ensure_dir(analysis_comp_dir + drug_name)
    output1_fn = analysis_comp_dir + drug_name + "/graph.dot"
    output2_fn = analysis_comp_dir + drug_name + "/tmp_graph.pdf"
    output3_fn = analysis_comp_dir + drug_name + "/graph.pdf"


    subnodes = rel_graph.visible_nodes(snodes)

    rel_graph.saveRelGraph(rel_graph, output1_fn, subnodes=subnodes)
    
   # output_fn = os.path.relpath()
    command = "dot -Tpdf "+output1_fn + " -o " + output2_fn
    os.system(command)

    command = "pdfcrop --margins '3 3 3 3' " + output2_fn + " "+output3_fn
    os.system(command)

    return drug_name
    
if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    gwamar_utils.ensure_dir(analysis_dir)
    gwamar_utils.ensure_dir(analysis_comb_dir)
    gwamar_utils.ensure_dir(analysis_comp_dir)

    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()

    drug_names = ["Rifampicin"]
    
    for drug_name in drug_names:
        TASK["DRUG_NAME"] = drug_name

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Combined")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveScoresCombined, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveScoresCombined(T)
            progress.update(str(r))

#print(hyperTestPvalue(1000, 10, 20, 4))