from src.drsoft.utils.gwamar_muts_utils import compressProfile, decompressProfile,convertFullProfile
from src.drsoft.structs.gene_profile import GeneProfile
from src.drsoft.structs.mutation_profile import MutationProfile
from src.drsoft.utils.gwamar_strains_io_utils import strainAureusNames,\
    strainsMapRev

def writeMutationsHeader(output_fh, strains_subset = None, only_strains = False, sep="\t"):
    if not only_strains: header_line = "cc_id" + "\t" + "aln_fn_id" + "\t" + "ann_id" + "\t" + "MT" + "\t" + "positions" + "\t" + "ref_positions"
    else: header_line = ""
    if strains_subset == None: return None
    for strain_id in strains_subset:      
        header_line += strain_id + sep
    header_line = header_line[:-1] + "\n" 
    output_fh.write(header_line);

def savePointMutations(output_fh, mutations_map, gene_names, gene_anns, ref_states, strains_list, cluster_gene_ref_ids={}, cluster_gene_names = {}, compress = True):
    tls = []
    
    for gene_id in sorted(mutations_map):
        gene_name = gene_names[gene_id]
        tl = ">" + gene_id + "\t" + gene_id + '\t' + gene_name
        if gene_id in gene_anns:
            (end, length, strand) = gene_anns[gene_id]
            if strand == "+": start = end - length + 1
            else: start = start = end + length - 1
            tl += "\t" + str(start) + "\t" + str(end) + "\t" + strand + "\n"
        else: tl += "\n"
            
        for position in sorted(mutations_map[gene_id]):
            ref_state = ref_states[(gene_id, position)]
            if len(gene_id) <= 1: gene_id = "x" 
            
            if position <= 0: mut_t = "p"
            else:
                mut_t = "s"
                for strain_id in mutations_map[gene_id][position]:
                    new_state, mut_type = mutations_map[gene_id][position][strain_id]
                    if mut_type in ["N", "CN"]:
                        mut_t = "n"
                        break        
                if mut_t == "s":
                    continue
                
            full_profile = ""
            for strain_index in range(len(strains_list)):
                strain_id = strains_list[strain_index]
                if strain_id in mutations_map[gene_id][position]:
                    new_state, mut_type = mutations_map[gene_id][position][strain_id]
                    full_profile += new_state
                else:
                    full_profile += ref_state
  
            if compress: desc_tl = compressProfile(full_profile)
            else: desc_tl = full_profile

            #print("here:", desc_tl)
            if desc_tl.endswith(" "): continue

            tl += str(position) + "\t"
            tl += str(mut_t) + "\t"
            tl += desc_tl.strip() + "\n"
        tls.append((gene_id, tl))

    writeMutationsHeader(output_fh, strains_list, only_strains = True, sep=" ")
    for (gene_id, tl) in sorted(tls):
        output_fh.write(tl)
    return None

def readGoldAssociations(input_fh, cluster_ids_rev={}, drug_names=None, genes_level=False):
    gold_assocs = {}
    drug_name = ""
    gene_id = ""

    lines = input_fh.readlines()
    for line in lines:
        line = line.strip()
        line = line.split("#")[0].strip()
        if len(line) == 0:
            continue
        tokens = line.split()
        if len(tokens) == 6:
            drug_name,gene_name,gene_id,position,ref_state,mut_state = tokens
            position = int(position)
            if not drug_name in gold_assocs:
                gold_assocs[drug_name] = {}
            if not gene_id in gold_assocs[drug_name]:
                if genes_level == False:
                    gold_assocs[drug_name][gene_id] = {}
                else:
                    gold_assocs[drug_name][gene_id] = None
            if genes_level == True:
                continue
            if not position in gold_assocs[drug_name][gene_id]:
                gold_assocs[drug_name][gene_id][position] = []
            gold_assocs[drug_name][gene_id][position].append((ref_state, mut_state))

        elif line.startswith("%"):
            drug_name = line[1:]
            gold_assocs[drug_name] = {}
        elif line.startswith(">"):
            tmpline = line[1:]
            tmptokens = tmpline.split()
            if len(tmptokens) == 1:
                gene_id = tmptokens[0]
            else:
                gene_id = tmptokens[1]
            if genes_level == False:
                gold_assocs[drug_name][gene_id] = {}
            else:
                gold_assocs[drug_name][gene_id] = None
        else:
            if genes_level == True:
                continue
            tokens = line.split()
            if len(tokens) < 3:
                continue
            position = int(tokens[0].split("|")[0])
            ref_state = tokens[1]
            mut_states = tokens[2].split(",")
            for mut_state in mut_states:
                if not position in gold_assocs[drug_name][gene_id]:
                    gold_assocs[drug_name][gene_id][position] = []
                gold_assocs[drug_name][gene_id][position].append((ref_state, mut_state))

    if drug_names != None:
        gold_assocs_sel = {}
        for drug_name in gold_assocs:
            if not drug_name in drug_names:
                continue
            gold_assocs_sel[drug_name] = gold_assocs[drug_name]
        return gold_assocs_sel
    return gold_assocs

def loadDrugResRels(input_fh, cluster_ids_rev={}, genes_level=False):
    drug_targets = {}
    lines= input_fh.readlines();
    for line in lines:
        line = line.strip()

        tokens_hash = line.split("#")
        if len(tokens_hash) > 1: line = tokens_hash[0]
        tokens = line.split()
        
        if len(tokens) < 5: continue
        
        drug_name = tokens[0]
        if not drug_name in drug_targets:
            drug_targets[drug_name] = {}

        gene_id = tokens[2]
        cluster_id =  cluster_ids_rev.get(gene_id, gene_id)

        if genes_level == False:
            position = int(tokens[3])
            ref_states = tokens[4]
            mut_states = tokens[5]
            if not cluster_id in drug_targets[drug_name]:
                drug_targets[drug_name][cluster_id] = {}
            if not position in drug_targets[drug_name][cluster_id]:
                drug_targets[drug_name][cluster_id][position] = set([])
            drug_targets[drug_name][cluster_id][position].add((ref_states, mut_states))
        else:
            if not cluster_id in drug_targets[drug_name]:
                drug_targets[drug_name][cluster_id] = None

    return drug_targets

def readPointMutations(input_fh, cluster_ids={}, cluster_ids_rev={}, gene_subset=None, strains_list=None, m_type="a"):
    gene_profiles = {}
    
    lines = input_fh.readlines()
    line0 = lines[0].strip()
    strain_ids_tmp = line0.split()
    strain_ids = []
    for strain_id_tmp in strain_ids_tmp:
        strain_ids.append(strainAureusNames(strain_id_tmp))
    n = len(strain_ids)

    if strains_list == None:
        strains_map_rev = None
    else:
        strains_map_rev = strainsMapRev(strains_list)

    gene_profile = None
    
    for line in lines[1:]:
        line = line.strip()
        m_pos_details = True
        if line.startswith(">"):
            line = line[1:]
            m_pos_details = False

        tokens = line.split("\t")
        if len(tokens) == 0: continue
        elif len(tokens) == 1: sequence = tokens[0]
        elif m_pos_details and len(tokens) == 3:    
            m_position = int(tokens[0])
            
            m_type = tokens[1]
            m_desc = tokens[2]
            if gene_profile == None: continue
            try:
                full_profile_tmp = decompressProfile(m_desc, n)
            except:
                print(m_desc)
                exit()
            full_profile = convertFullProfile(full_profile_tmp, strain_ids, strains_map_rev)
            mutation = MutationProfile(m_position, m_type, full_profile)
            mutation.gene_profile = gene_profile
            gene_profile.mutations[m_position] = mutation
        elif  m_pos_details and len(tokens) == 5: 
            m_position = int(tokens[0])
            m_type = tokens[1]
            m_bin_profile_id = tokens[2]
            ref_state = tokens[3]
            m_desc = tokens[4]
            
            if gene_profile == None: continue
            try:
                full_profile_tmp = decompressProfile(m_desc, n)
            except:
                print("x", n, m_desc)
                exit()
            full_profile = convertFullProfile(full_profile_tmp, strain_ids, strains_map_rev)
            mutation = MutationProfile(m_position, m_type, full_profile)
            mutation.ref_state = ref_state
            mutation.gene_profile = gene_profile
            mutation.bin_profiles = {}
            mutation.bin_profiles[m_bin_profile_id] = ""
            gene_profile.mutations[m_position] = mutation
        else:
            gene_id = tokens[0]
            cluster_id = tokens[1]
            gene_name = simpleGeneName(tokens[2])
            gene_profile_id = gene_id + "_" + cluster_id
            
            if gene_profile_id in gene_profiles: 
                gene_profile = gene_profiles.get(gene_profile_id)
                continue

            if len(tokens) >= 6:
                ref_start = int(tokens[3])
                ref_end = int(tokens[4])
                ref_strand = tokens[5]
            else:
                ref_start,ref_end,ref_strand = -1,-1,"?"
                
            gene_profile = GeneProfile(cluster_id, gene_id, gene_name, ref_start, ref_end, ref_strand)
            gene_profiles[gene_profile_id] = gene_profile

    return gene_profiles

def readGeneGainLossProfiles(input_fh, cluster_ids={}, cluster_ids_rev={}, gene_subset=None, strains_list=None, m_type="a"):
    gene_profiles = {}
    
    line0 = input_fh.readline().strip()
    strain_ids_tmp = line0.split()
    strain_ids = []
    for strain_id_tmp in strain_ids_tmp:
        strain_ids.append(strainAureusNames(strain_id_tmp))
        
    n = len(strain_ids)

    if strains_list == None:
        strains_map_rev = None
    else:
        strains_map_rev = strainsMapRev(strains_list)

    gene_profile = None
    
    for line in input_fh.readlines():
        line = line.strip()

        tokens = line.split("\t")
        if len(tokens) == 0: continue
        elif len(tokens) == 1: sequence = tokens[0]
        else:
            gene_id = tokens[0]
            cluster_id = tokens[1]
            gene_name = simpleGeneName(tokens[2])
            m_desc = tokens[-1]
            
            gene_profile_id = gene_id + "_" + cluster_id
                        
            try:
                full_profile_tmp = decompressProfile(m_desc, n)
            except:
                print(m_desc)
                exit()

            ref_start,ref_end,ref_strand = -1,-1,"?"

            full_profile = convertFullProfile(full_profile_tmp, strain_ids, strains_map_rev)                
            gene_profile = GeneProfile(cluster_id, gene_id, gene_name, ref_start, ref_end, ref_strand, full_profile=full_profile)
            gene_profiles[gene_profile_id] = gene_profile

    return gene_profiles

def simpleGeneName(gene_name):
    gene_name = gene_name.replace("/", "_")
    gene_name = gene_name.replace("'", "")
    return gene_name
