import sys
from src.drsoft.utils import gwamar_params_utils, gwamar_comp_utils,\
  gwamar_muts_utils, gwamar_utils, gwamar_res_io_utils, gwamar_stat_utils

def retrieveCompMutations(drug_name):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    comp_dir = parameters["COMP_DATA"]

    print(analysis_comb_dir + drug_name + ".txt")
    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    gene_muts, rdr_muts, dr_profile = gwamar_comp_utils.selectPutativeComp(input_fh, only_comp = True)
    input_fh.close()

    dr_set = gwamar_comp_utils.getResSet(dr_profile)
    
    tab_res = {}

    for rdr1_mut_descs, rdr1_mut_profile in sorted(rdr_muts, key=lambda x:x[0].split()[3]):
        tokens = rdr1_mut_descs.split()
        print("\\item " + tokens[4][0] + tokens[3] + gwamar_muts_utils.mutsDescToMathLatex(tokens[5]))

    for i in range(len(rdr_muts)-1):
        rdr1_mut_descs, rdr1_mut_profile = rdr_muts[i]
        for j in range(i+1, len(rdr_muts), 1):
            
            rdr2_mut_descs, rdr2_mut_profile = rdr_muts[j]
            rdr1_set = gwamar_comp_utils.getBinSet(rdr1_mut_profile)
            rdr2_set = gwamar_comp_utils.getBinSet(rdr2_mut_profile)
            pval = gwamar_stat_utils.hyperTestPvalue(len(dr_set), len(rdr2_set), len(rdr1_set), len(rdr1_set & rdr2_set), twoends=True)
            
            # if pval < 0.05:
            #     print(rdr1_mut_descs, rdr2_mut_descs)
            #     print(pval)
            #     print(len(dr_set), len(rdr1_set), len(rdr2_set), len(rdr1_set & rdr2_set))

    for rdr_mut_descs, rdr_mut_profile in rdr_muts:
        for rdr_mut_desc in rdr_mut_descs.strip().split(";"):

            m1_gene_id, m2_c_id, m1_gene_name, m1_p_str, m1_ref_txt, m1_mut_txt = rdr_mut_desc.split()
            m1_mut_latex = gwamar_muts_utils.mutsDescToLatex(m1_mut_txt)
            m1_set = gwamar_comp_utils.getBinSet(rdr_mut_profile)

            if not gwamar_comp_utils.isMutInRRDR(rdr_mut_desc):
                continue

            if not rdr_mut_desc in tab_res:
                tab_res[rdr_mut_desc] = []

            for gene_name in gene_muts:
                for gene_mut_descs, gene_mut_profile in gene_muts[gene_name]:
                    for gene_mut_desc in gene_mut_descs.strip().split(";"):
                        m2_gene_id, m2_c_id, m2_gene_name, m2_p_str, m2_ref_txt, m2_mut_txt = gene_mut_desc.split()
                        m2_mut_latex = gwamar_muts_utils.mutsDescToLatex(m2_mut_txt)
                        m2_set = gwamar_comp_utils.getBinSet(gene_mut_profile)

                        pval = gwamar_stat_utils.hyperTestPvalue(len(dr_set), len(m2_set), len(m1_set), len(m1_set & m2_set), twoends=False)
                        
                        if pval < 0.001:
                            print(rdr_mut_desc, gene_mut_desc)
                            print(pval)
                            print(len(dr_set), len(m2_set), len(m1_set), len(m1_set & m2_set))

                        if m2_set.issubset(m1_set):
                            tab_res[rdr_mut_desc].append(gene_mut_desc)

    tls = []

    for m1_desc in sorted(tab_res, key=lambda m1:-len(tab_res[m1])):
    #    if len(tab_res[m1_desc]) == 0:
    #        continue
        m1_gene_id, m2_c_id, m1_gene_name, m1_p_str, m1_ref_txt, m1_mut_txt = m1_desc.split()
        m1_mut_latex = gwamar_muts_utils.mutsDescToLatex(m1_mut_txt)
        m1_desc_out = " ".join([m1_ref_txt[0], m1_p_str, m1_mut_txt])
        tls.append(">"+" ".join([m1_gene_name, m1_desc_out]) + "\n")

        for m2_desc in sorted(tab_res[m1_desc], key=lambda m2: (m2.split()[2], int(m2.split()[3]))):
            m2_gene_id, m2_c_id, m2_gene_name, m2_p_str, m2_ref_txt, m2_mut_txt = m2_desc.split()
            m2_mut_latex = gwamar_muts_utils.mutsDescToLatex(m2_mut_txt)
            m2_desc_out = " ".join([m2_ref_txt[0], m2_p_str, m2_mut_txt])
            tls.append(" ".join([m2_gene_name, m2_desc_out]) + "\n")

    gwamar_utils.ensure_dir(comp_dir + "/" + drug_name)
    output1_fn = comp_dir + "/" + drug_name + "/rels_" + parameters["D"] + ".tet"

    output_fh = open(output1_fn, "w")
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    comp_dir = parameters["COMP_DATA"]

    gwamar_utils.ensure_dir(comp_dir)
    print(comp_dir)
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()

    drug_names = ["Rifampicin"]
    
    for drug_name in drug_names:
        retrieveCompMutations(drug_name)
