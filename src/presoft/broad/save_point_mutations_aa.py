import sys
from src.drsoft.utils import gwamar_params_utils, gwamar_ann_utils,\
  gwamar_strains_io_utils, gwamar_muts_io_utils

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset_dir = parameters["GWADRIB_DATASETS"] + "/broad_mtb/"
    snps_dir = dataset_dir + "snps/"
    input_dir = dataset_dir + "input/"
    
    input_fh = open(input_dir + "/ref_annotation.txt")
    gene_anns = gwamar_ann_utils.readRefAnns(input_fh)
    input_fh.close()
    
    print(gene_anns.keys())
    
    input_fh = open(input_dir + "/strain_profiles_ord.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    strains_list = strains.strain_ids
    
    mutations_map = {}
    ref_states = {}
    ref_states_strain = {}
    gene_names = {}
    
    for strain_id in strains_list:
        input_fh = open(snps_dir + strain_id + "/" + strain_id + ".snps.annotated")
        for line in input_fh.readlines():
            line = line.strip()
            tokens = line.split("\t")
            ref_strain = tokens[0]
            gene_id = tokens[1]
            if gene_id == "Rv2427Ac":
                gene_id = "Rv2427c"
            gene_name_org = tokens[2]

            md_tokens = tokens[3].split("_")
            mut_type = md_tokens[1]
            ref_position = int(md_tokens[2])

            if gene_name_org.startswith("promoter_"): gene_name = gene_name_org[len("promoter_"):]
            else: gene_name = gene_name_org

            if gene_name == "rpsL": gene_id = "Rv0682"
            elif gene_name == "embA-embB": gene_id = "Rv3794"
            elif gene_name == "fabG1-inhA": gene_id = "Rv1483"
            elif gene_name == "gyrB-gyrA": gene_id = "Rv0005"
            elif gene_name == "iniB-iniA-iniC": gene_id = "Rv0341"
            elif gene_name == "pncA": gene_id = "Rv2043c"
            elif gene_name == "ethA": gene_id = "Rv3854c"
            elif gene_name == "ahpC": gene_id = "Rv2428"
            elif gene_name == "thyA": gene_id = "Rv2764c"
            elif gene_name == "gid": gene_id = "Rv3919c"
            elif gene_name == "rpoB": gene_id = "Rv0667"
            elif gene_name == "ndh": gene_id = "Rv1854c"
            
            #print(gene_name_org, gene_name, gene_id)

            if gene_name_org.startswith("promoter_"):            
                nuc1 = md_tokens[3][0]
                nuc2 = md_tokens[3][-1]
                if len(md_tokens[3]) > 2: position = int(md_tokens[3][1:-1])
                else: position = 0
                ref_state = nuc1
                new_state = nuc2
                
            elif len(md_tokens) == 6:
                nuc1 = md_tokens[3][0]
                nuc2 = md_tokens[3][1]
            
                aa1 = md_tokens[5][0]
                aa2 = md_tokens[5][-1]
                
                position_aa = int(md_tokens[5][1:-1])
                position = position_aa
        
                ref_state = aa1
                new_state = aa2
                
            elif len(md_tokens) == 5:
                nuc1 = md_tokens[3][0]
                nuc2 = md_tokens[3][-1]
                position = int(md_tokens[3][1:-1])
                
                ref_state = nuc1
                new_state = nuc2
            
            if len(gene_id) <= 1:
                print("ZLE", gene_id, gene_name, line)
        
            if not gene_id in gene_names: gene_names[gene_id] = gene_name
            if not gene_id in mutations_map: mutations_map[gene_id] = {}
            if not (gene_id, position) in ref_states:
                ref_states[(gene_id, position)] = ref_state
                ref_states_strain[(gene_id, position)] = (ref_state, strain_id)
            else:
                ref_state_old = ref_states[(gene_id, position)]
                if ref_state_old !=  ref_state:
                    print("JUZ JEST INNY!", ref_state, ref_state_old, gene_id, position, strain_id, ref_states_strain[(gene_id, position)])
                
            if not position in mutations_map[gene_id]: mutations_map[gene_id][position] = {}
            mutations_map[gene_id][position][strain_id] = (new_state, mut_type)

        input_fh.close()
            
    output_fh = open(input_dir + "/point_mutations_aa.txt", "w")
    gwamar_muts_io_utils.savePointMutations(output_fh, mutations_map, gene_names, gene_anns, ref_states, strains_list, compress=True)
    output_fh.close()        
