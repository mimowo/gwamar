import sys
from src.drsoft.utils import gwamar_params_utils


def loadResData(input_fh):
    res_data_map = {}
    citation = ""

    lines = input_fh.readlines()
    for line in lines:
        line = line.strip()
        if line.startswith("#") or len(line) < 2:
            continue
        tokens = line.split("\t")
   #     print(tokens)
        if len(tokens) < 1: continue
        elif len(tokens) <= 2:
            if len(line) < 40:
                print("citation", line)
            citation = tokens[0]
        else:
            drug_name = tokens[0]
            strain_id = tokens[1]
            res_info = tokens[2]

            if not (strain_id, drug_name) in res_data_map:
                res_data_map[(strain_id, drug_name)] = set([])
            res_data_map[(strain_id, drug_name)].add((res_info, citation))
            
    #print("xxx", res_data_map)
    return res_data_map

def generateLatexHeader():
    tls = []
    tls.append("\\documentclass[10pt]{article}")
    tls.append("\\usepackage{amsmath}")
    tls.append("\\usepackage{amssymb}")
    tls.append("\\usepackage{graphicx}")
    tls.append("\\usepackage[table]{xcolor}")
    tls.append("\\usepackage{cite}")
    tls.append("\\usepackage{color} ")
    if dataset == "sau":
        tls.append("\\usepackage[margin=1in, paperwidth=108in, paperheight=45in]{geometry}")
        tls.append("\\textwidth 30in ")
        tls.append("\\textheight 42in")
    elif dataset == "mtu":
        tls.append("\\usepackage[margin=1in, paperwidth=16in, paperheight=37in]{geometry}")
        tls.append("\\textwidth 13in ")
        tls.append("\\textheight 34in")    
    elif dataset == "spn":
        tls.append("\\usepackage[margin=1in, paperwidth=25in, paperheight=15in]{geometry}")
        tls.append("\\textwidth 22in ")
        tls.append("\\textheight 13in")

    tls.append("\\topmargin 0.0cm")
    tls.append("\\oddsidemargin 0.5cm")
    tls.append("\\evensidemargin 0.5cm")
    tls.append("\\usepackage[labelfont=bf,labelsep=period,justification=raggedright]{caption}")
    tls.append("\\makeatletter")
    tls.append("\\renewcommand{\\@biblabel}[1]{\\quad#1.}")
    tls.append("\\makeatother")
    tls.append("\\date{}")
    tls.append("\\pagestyle{myheadings}")
    tls.append("\\begin{document}")
    return tls

def generateLatexBottom():
    tls = []
    tls.append("\\small")
    if dataset == "sau":
        tls.append("\\section*{Comments}")
        tls.append("\\begin{itemize}")
        tls.append("\\item legend: red: drug resistant; green: drug susceptible; yellow: intermediate drug resistant; gray: contrary information of no CLSI interpretation; white: not information is found,")
        tls.append("\\item drug susceptibility information for the strain TW20 was confirmed by e-mail correspondence with the authors of \\cite{edgeworth_outbreak_2007}")
        tls.append("\\item drug susceptibility information for the strain ST398 was confirmed by e-mail correspondence with the authors of \\cite{schijffelen_whole_2010}")
        tls.append("\\item accordingly to \\cite{herron-olson_molecular_2007} 'ET3-1 (RF122) is susceptible to most antibiotics, excluding Spectinomycin',")
        tls.append("according to \\cite{mwangi_tracking_2007} JH1 is susceptible to Rifampicin, with MIC 0.012 $\\mu g/ml$. We use information from \\cite{mwangi_tracking_2007}, treating it as more reliable, because this study is newer and provides the exact MIC value.")
        tls.append("\\end{itemize}")
    tls.append("\\bibliography{references}")
    tls.append("\\bibliographystyle{plain}")
    tls.append("\\end{document}")
    return tls

def determineResistance(res_data_infos):
    res_count = 0
    susc_count = 0
    int_count = 0
    unk_count = 0
    for (res_info, cite_info) in res_data_infos:
        tokens = res_info.split(":")
        if tokens[0] == "R":
            res_count += 1 
        elif tokens[0] == "S":
            susc_count += 1
        elif tokens[0] == "I":
            int_count += 1
        elif tokens[0] == "U":
            unk_count += 1
    if res_count > 0 and susc_count == 0:
        return "R"
    elif res_count == 0 and susc_count > 0:
        return "S"
    elif res_count > 0 and susc_count > 0:
        return "?"
    elif int_count >0:
        return "I"
    elif unk_count > 0:
        return "?"
    else:
        return "?"
    return "?"


def determineCellColor(res_data_infos):
    color = "white"
    res_count = 0
    susc_count = 0
    int_count = 0
    unk_count = 0
    for (res_info, cite_info) in res_data_infos:
        tokens = res_info.split(":")
        if tokens[0] == "R":
            res_count += 1 
        elif tokens[0] == "S":
            susc_count += 1
        elif tokens[0] == "I":
            int_count += 1
        elif tokens[0] == "U":
            unk_count += 1
    if res_count > 0 and susc_count == 0:
        color = "red"
    elif res_count == 0 and susc_count > 0:
        color = "green"
    elif res_count > 0 and susc_count > 0:
        color = "gray"
    elif int_count >0:
        color = "yellow"
    elif unk_count > 0:
        color = "gray"
    else:
        color = "white"
    return color
        
def drugsList(res_data):
    drugs_map_susc = {}
    drugs_map_res_int = {}

    for (strain_id, drug_name) in res_data:
        res_status = determineResistance(res_data[(strain_id, drug_name)])
        if res_status == "R" or res_status == "I":
            if not drug_name in drugs_map_res_int:
                drugs_map_res_int[drug_name] = 0
            drugs_map_res_int[drug_name] += 1
        elif res_status == "S":
            if not drug_name in drugs_map_susc:
                drugs_map_susc[drug_name] = 0
            drugs_map_susc[drug_name] += 1

    drugs_set = set(drugs_map_susc.keys()) | set(drugs_map_res_int.keys())
    drugs_list = sorted(drugs_set, key=lambda drug_name:-min(drugs_map_susc.get(drug_name,0), drugs_map_res_int.get(drug_name,0)))
    return drugs_list

def strainsList(res_data):
    strains_set = set([])
    for (strain_id, drug_name) in res_data:
        strains_set.add(strain_id)
    strains_list = sorted(strains_set)
    return strains_list

def generateDataTable(res_data):
    tls = []

    drugs_list = drugsList(res_data)
    strains_list = strainsList(res_data)

    #Ciprofloxacin\tprint(drugs_list)
    #print(strains_list)

    k = len(drugs_list)
    n = len(strains_list)

    tls.append("\\begin{table}[!ht]")
    tls.append("\\caption{\\bf{Complete collected data on drug resistance.}}")
    
    tls.append("\\begin{tabular}{|" + ''.join(["c|"]*(k+1)) + "}\\hline")
    tl = "Strain"
    for drug_name in drugs_list: 
        tl += " & " + drug_name
    tl += "\\\\ \\hline"
    tls.append(tl)

    for strain_id in strains_list:
        tl = ""
        strain_name = strain_id.replace("_", " ")
        tl += strain_name

        for drug_name in drugs_list:
            tl += " & "
            res_data_infos = res_data.get((strain_id, drug_name), [])
            color = determineCellColor(res_data_infos)
            for (res_info, cite_info) in res_data_infos:
                cite_info_tex = cite_info
                res_info_tex = res_info.replace("<=", "\leq")
                res_info_tex = res_info_tex.replace("=<", "\leq")
                res_info_tex = res_info_tex.replace(">=", "\geq")
                res_info_tex = res_info_tex.replace("=>", "\geq")
                tl += "$"+res_info_tex+"$"
                tl += "\cite{"+cite_info_tex+"}"
                if color != "white":
                    tl += "\cellcolor{"+color+"}"
        tl += "\\\\ \\hline"
        tls.append(tl)
    tls.append("\\end{tabular}")
    tls.append("\\end{table}")
    return tls

def generateLatexFile(output_fh, res_data):
    tls = []
    tls += generateLatexHeader()
    tls += generateDataTable(res_data)
    tls += generateLatexBottom()
    print(len(tls))
    for tl in tls:
        output_fh.write(tl + "\n")
    return None

if __name__ == '__main__':

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    gwadrib_dir = parameters["GWADRIB_PATH"]
    for dataset in ["spn", "mtu", "sau"]:
        input_dir = gwadrib_dir + "/res_data/" + dataset + "/"

        print("xxx")
        input_fh = open(input_dir + "/res-data.txt")
        res_data = loadResData(input_fh)
        input_fh.close()
        print("yyy")

        output_fh = open(input_dir + "/data-res.tex", "w")
        generateLatexFile(output_fh, res_data)
        output_fh.close()
        print("zzz")
