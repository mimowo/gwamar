import os, time, sys
from src.drsoft.utils import gwamar_utils

python_cmd = sys.executable  

def scores_std(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "scoring")
  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", "MT", "EP"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/sc1_compute_std_scores.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/sc2_save_std_rankings.py" + passed_params): exit()
  t1_time = time.time()
  gwamar_utils.logExecutionTime(t1_time - t0_time, "scores_std")

def scores_ranks(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "scoring")
  params = ["D", "REVORDER", "SCORES", "OW", "W", "GEN_BIN_PROFILES", "MT", "EP"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  print(passed_params)
  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/sc2_save_std_rankings.py" + passed_params): exit()
  t1_time = time.time()
  
  gwamar_utils.logExecutionTime(t1_time - t0_time, "scores_ranks")
  
def scores_pvalues(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "scoring")
  
  params = ["D", "REVORDER", "SCORES",  "OW", "W", "GEN_BIN_PROFILES", "MT", "EP"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)

  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/sc3_compute_perm_scores.py" + passed_params): exit()
  if os.system(python_cmd + " " + srcdir + "/sc4_save_perm_rankings.py" + passed_params): exit()
  t1_time = time.time()

  gwamar_utils.logExecutionTime(t1_time - t0_time, "scores_pvalues")
  
def scores_norm(parameters):
  appdir = os.path.abspath(os.curdir)
  srcdir = os.path.join(appdir, "src", "drsoft", "scoring")

  params = ["D", "REVORDER", "SCORES",  "OW", "W", "GEN_BIN_PROFILES", "MT", "EP"]
  passed_params = gwamar_utils.createCommandParams(params, parameters)
  
  t0_time = time.time()
  if os.system(python_cmd + " " + srcdir + "/sc5_compute_norm_scores.py" + passed_params): exit()
  t1_time = time.time()

  gwamar_utils.logExecutionTime(t1_time - t0_time, "scores_norm")
