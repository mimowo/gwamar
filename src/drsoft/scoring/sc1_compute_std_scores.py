import sys
import os
import multiprocessing
from multiprocessing import Pool

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_scoring, gwamar_utils,\
  gwamar_res_io_utils, gwamar_progress_utils

def scoreDrug(params):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    drug_name = params["DRUG_NAME"]
    score_full = params["SCORE"]

    bin_profile_scores, rev_sorting = gwamar_scoring.computeScores(score_full, drug_name, parameters)

    if bin_profile_scores == None:
        print("Something has gone wrong: "+ drug_name + " " + score_full)
        return drug_name, score_full
    
    tls = []
    bin_profile_scores_sorted = sorted(bin_profile_scores, key=lambda bin_profile_id:rev_sorting*bin_profile_scores[bin_profile_id])

    for bin_profile_id in bin_profile_scores_sorted:
        bin_profile_score = bin_profile_scores[bin_profile_id]
        tl = "\t".join([str(bin_profile_id), str(bin_profile_score)])
        tls.append(tl)

    gwamar_utils.ensure_dir(parameters["RESULTS_SCORES_DIR"] + drug_name + "/")

    output_fh = open(parameters["RESULTS_SCORES_DIR"] + drug_name + "/" + score_full + ".txt", "w") 
    output_fh.write("\n".join(tls))
    output_fh.close()
    
    return drug_name, score_full
    
if __name__ == '__main__':
    print("sc1_compute_std_scores.py. Computation of scores...")

    parameters = gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    
    gwamar_utils.ensure_dir(results_dir)
    gwamar_utils.ensure_dir(scores_dir)
    gwamar_utils.ensure_dir(parameters["TGH_DIR"])
    
    score_params = parameters["SCORES"].split(",")
    print("Scores to compute: " + " ".join(score_params))
    
    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()

    print("Drug resistance profiles: " + ' '.join(drug_names))
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(scores_dir + drug_name)
        for score_param in score_params:
            if parameters["OW"] == "N" and os.path.exists(parameters["RESULTS_SCORES_DIR"] + drug_name + "/" + score_param + ".txt"):
                continue
            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score_param
            TASKS.append(TASK.copy())

    print("Total number of jobs: " + str(len(TASKS)))

    progress = gwamar_progress_utils.GWAMARProgress("Scores computed")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))

    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(scoreDrug, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreDrug(T)
            progress.update(str(r))

    print("sc1_compute_std_scores.py. Finished")
