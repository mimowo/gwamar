import os
import sys
from src.drsoft.structs import res_tree
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_tree_io_utils

def transformTree(tree, clusters_map):
    for leaf_node in tree.root.getLeaves():
        strains_tmp = list(clusters_map[leaf_node.label].split(","))
        if len(strains_tmp) == 1:
            leaf_node.label = strains_tmp[0]
            leaf_node.node_id = strains_tmp[0]
        else:
            for strain_id in strains_tmp:
                new_leaf = res_tree.ResTreeNode(strain_id, leaf_node, strain_id)
                new_leaf.e_len = 0.00001
                leaf_node.addChildren(new_leaf)

    return tree

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    m_type = "sub"
    
    dataset_dir = parameters["DATASET_DIR"]
    phylo_dir = parameters["PHYLO_DIR"]
    phylo_dir = dataset_dir + "phylo/"

    genes = [] 
    for gene_fn in os.listdir(phylo_dir + '/genes'):
        gene_id = gene_fn.split(".")[0].split("_")[-1]
        if gene_id.startswith("Rv"): genes.append(gene_id)
    
    for gene_id in genes:   

        input_fh = open(phylo_dir + "genes/clusters_map_"+gene_id+".txt")
        cluster_strains = gwamar_strains_io_utils.readStrains(input_fh)
        input_fh.close()
        
        clusters_map = {}
        input_fh = open(phylo_dir + "genes/clusters_map_"+gene_id+".txt")
        for line in input_fh.readlines():
            tokens = line.strip().split("\t")
            clusters_map[tokens[0]] = tokens[1].replace(" ", ",")
        input_fh.close()
        
        print(cluster_strains.count(), cluster_strains.allStrains())
        
        input_fn = phylo_dir + 'genes/phylotree_'+gene_id+'.txt'
        output_fn = phylo_dir + 'genes/phylotree_'+gene_id+'_flat.txt'
        input_fh = open(input_fn)
        output_fh = open(output_fn, "w")
        tree = gwamar_tree_io_utils.readPhyloTrees(input_fh, cluster_strains)[0]
        tree_org = transformTree(tree, clusters_map)
        #print(tree.toString(lengths=True, strains=cluster_strains, int_labels=False))
        flat_tree = tree_org.flatten(0.00001)
        print(tree_org.toString(lengths=True, strains=None, int_labels=False))
        text_line = flat_tree.toString(lengths=True, strains=None, int_labels=False) + ";\n"
        output_fh.write(text_line);
        output_fh.close();
        input_fh.close()
    
