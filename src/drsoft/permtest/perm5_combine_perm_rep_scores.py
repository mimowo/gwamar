import sys
import os
from multiprocessing import Pool
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_res_io_utils,\
  gwamar_res_utils, gwamar_utils, gwamar_progress_utils

const = 100000.0

def scoreDrug(params):
    score = params["SCORE"]
    drug_name = params["DRUG_NAME"]
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    perm_scores_dir = parameters["RESULTS_PERM_SCORES_DIR"]
    perm_profiles_dir = parameters["RESULTS_PERM_PROFILES_DIR"]
    perm_combs_dir = parameters["RESULTS_PERM_COMBS_DIR"]
    perm_rep = int(parameters["REPS"])

    input_fh = open(perm_profiles_dir + "/" + drug_name + ".txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    bin_profile_scores = {}
    
    for drug_name_id in drug_names:
        current_id = gwamar_res_utils.drugNamePermID(drug_name_id)
        if current_id >= perm_rep: continue
        
        input_fn = perm_scores_dir + drug_name + "/" + score + "/" + drug_name_id +  ".txt"
        if not os.path.exists(input_fn): continue        
        
        input_fh = open(input_fn) 
        for line in input_fh.readlines():
            tokens = line.strip().split()
            if len(tokens) < 2: continue
            score_float = float(tokens[1])
            score_int = int(const*score_float)
            if not score_int in bin_profile_scores:
                bin_profile_scores[score_int] = 0
            bin_profile_scores[score_int] += 1
        input_fh.close()

    bin_profile_scores_sorted = sorted(bin_profile_scores.keys())
    
    output_fh = open(perm_combs_dir + drug_name + "/" + score + ".txt", "w") 
    for score_int in bin_profile_scores_sorted:
        count = bin_profile_scores[score_int]
        score_float = float(score_int) / const
        output_fh.write(str("%.6f" % score_float) + "\t" + str(count) + "\n")
    output_fh.close()

    return drug_name, score
    
if __name__ == '__main__':
    print("perm5_combine_perm_rep_scores.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    perm_combs_dir = parameters["RESULTS_PERM_COMBS_DIR"]

    gwamar_utils.ensure_dir(perm_combs_dir)
    
    score_params = parameters["SCORES"].split(",")
    #score_params += ["tmow", "tmo", "tmw", "tow", "mow"]
    #score_params += ["tm", "to", "tw", "mo", "mw", "ow"]
    #score_params += ["mtw", "mtmow", "mtow",  "rand_tmow", "rand_tmow_stoch"]

    TASKS = []
    TASK = {}
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(perm_combs_dir + drug_name)
        for score_param in score_params:

            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score_param
            
            TASKS.append(TASK.copy())
        

    progress = gwamar_progress_utils.GWAMARProgress("Combine scores")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)   
        for r in pool.imap(scoreDrug, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreDrug(T)
            progress.update(str(r))

            
    print("perm5_combine_perm_rep_scores.py. Finished.")
