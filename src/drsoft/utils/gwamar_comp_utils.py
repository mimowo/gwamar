def getResSet(dr_profile, inc_q=False, inc_susc=True):
    res_set = set([])
    for i in range(len(dr_profile)):
        if dr_profile[i] in ["R","I"] or (inc_q == True and dr_profile[i] == "?"):
            res_set.add(i)
        elif dr_profile[i] == "S" and inc_susc == True:
            res_set.add(i)
    return res_set
 
def getSuscSet(dr_profile, inc_q=False):
    res_set = set([])
    for i in range(len(dr_profile)):
        if dr_profile[i] == "S" or (inc_q == True and dr_profile[i] == "?"):
            res_set.add(i)
    return res_set

def getBinSetsMap(dr_profile):
    bin_sets_map = {}
    for i in range(len(dr_profile)):
        if dr_profile[i] != "0" and dr_profile[i] != "?":
            char = dr_profile[i]
            if not char in bin_sets_map: bin_sets_map[char] = set([])
            bin_sets_map[char].add(i)
    return bin_sets_map
 
def getBinSet(dr_profile):
    bin_set = set([])
    bin_map = getBinSetsMap(dr_profile)
    for c in bin_map:
        bin_set = bin_set | bin_map[c]
    return bin_set


def isMutInRRDR(mut_desc):
    gene_id, cluster_id, gene_name, position_str, ref_state, mut_states = mut_desc.split()
    position = int(position_str)
    if gene_name == "rpoB" and position <= 452 and position >= 426:
        return True
    return False

def getRRDRmuts(lines):
    rdr_muts = []
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_descs = tokens[-1]
        for mut_desc in mut_descs.split(";"):
            if isMutInRRDR(mut_desc):
                rdr_muts.append((mut_desc, bin_profile))
    return rdr_muts

def getRRDRsets(lines):
    rrdr_sets = []
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_descs = tokens[-1]
        mut_set = getBinSet(bin_profile)
        for mut_desc in mut_descs.split(";"):
            if isMutInRRDR(mut_desc):
                rrdr_sets.append(mut_set)
    return rrdr_sets

def isSubset(s1, s_list):
    for s2 in s_list:
        if s1.issubset(s2):
            return True
    return False

def selectPutativeComp(input_fh, drug_name="Rifampicin", inc_prom=False, only_comp=False):
    lines = input_fh.readlines()
    line0 = lines[0]
    dr_profile = line0.strip().split("\t")[0]

    dr_set = getResSet(dr_profile, inc_q=False)

    if drug_name == "Rifampicin":
        rdr_muts = getRRDRmuts(lines)
    else:
        rdr_muts = [("res_profile", dr_profile)]

    rdr_sets = [getBinSet(rdr_mut_profile) for (rdr_mut_desc, rdr_mut_profile) in rdr_muts]

    gene_muts = {}
    
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_descs = tokens[-1]
        mut_set = getBinSet(bin_profile)
        if len(mut_set & dr_set) == 0:
            continue
        if not isSubset(mut_set, rdr_sets):
            continue

        for mut_desc in mut_descs.split(";"):
            gene_id, cluster_id, gene_name, position, ref_state, mut_states = mut_desc.split()


            if not gene_name in gene_muts:
                gene_muts[gene_name] = []
            if inc_prom == False and int(position) < 0:
                continue
            if (only_comp == False) or not isMutInRRDR(mut_desc):
                gene_muts[gene_name].append((mut_desc, bin_profile))

    return gene_muts, rdr_muts, dr_profile


def saveRelGraph(rel_graph, output_fn, subnodes=None):
    tls = []
    tls.append("digraph G {\n");

    if subnodes == None:
        outnodes = rel_graph.nodes.values()
    else:
        outnodes = subnodes

    for node in outnodes:
        tls.append("\t" + node.node_id +' [label="'+node.label+'",color='+node.ncolor+'];' + "\n")
    
    for snode in outnodes:
        for edge in snode.edges:
            tnode = edge.tnode
            if not tnode in outnodes:
                continue
            tls.append('\t' + snode.node_id + "->" + tnode.node_id + ' [label="' + edge.label + '",color=' + edge.ecolor + '];\n')
    tls.append("}\n")

    output_fh = open(output_fn, "w")
    for tl in tls:
        output_fh.write(tl);
    output_fh.close()
    return None
