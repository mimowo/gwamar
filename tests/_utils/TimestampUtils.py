import time, datetime


def createForTests(test=""):
  timestamp = time.time()
  timestamp_str = datetime.datetime.fromtimestamp(timestamp).strftime('%H.%M.%S.%f')
  return "test." + timestamp_str + "." + test
  
