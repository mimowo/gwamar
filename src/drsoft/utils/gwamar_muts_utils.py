import re

def profileSubset(full_profile, subset):
    subset_profile = []
    for i in sorted(subset):
        subset_profile.append(full_profile[i])
    return subset_profile

def convertFullProfile(full_profile_tmp, strain_ids, strains_map_rev):
    if strains_map_rev == None or strain_ids == None:
        return full_profile_tmp

    n = len(full_profile_tmp)
    m = len(strains_map_rev)

    full_profile = [""]*m

    for i in range(n):
        val = full_profile_tmp[i]
        strain_name = strain_ids[i]
        if strain_name in strains_map_rev:
            j = strains_map_rev[strain_name]
            full_profile[j] = val
        else: continue

    return full_profile

def decompressProfile(m_desc, n, strain_index_map=None):
    if m_desc.count(" ") > 0:    
        sel_state, desc = m_desc.split() 
        full_profile = [sel_state]*n
        desc_tokens = desc.split(";")
        for desc_token in desc_tokens:
            curr_state, strains_desc = desc_token.split(":")
            strains_tokens = strains_desc.split(",")
            for strain_token in strains_tokens:
                strain_token_dets = strain_token.split("_")
                if len(strain_token_dets) == 1:
                    strain_index = int(strain_token_dets[0])
                    if strain_index_map == None:
                        full_profile[strain_index] = curr_state
                    else:
                        full_profile[strain_index_map[strain_index]] = curr_state
                elif len(strain_token_dets) == 2:
                    for strain_index in range(int(strain_token_dets[0]), int(strain_token_dets[1])+1, 1):
                        if strain_index_map == None:
                            full_profile[strain_index] = curr_state
                        else:
                            full_profile[strain_index_map[strain_index]] = curr_state
                else:
                    print("error", strain_token_dets)
    else:
        full_profile = []
        for i in range(n): full_profile.append(m_desc[i])
    return full_profile

def compressProfile(full_profile):
    n = len(full_profile)
            
    state_strains = {}
    for strain_index in range(n):
        state = full_profile[strain_index]
        if not state in state_strains:
            state_strains[state] = []
        state_strains[state].append(strain_index)

    sel_state = sorted(state_strains, key=lambda state:-len(state_strains[state]))[0]

    comp_profile = sel_state + " "
    
    for state in state_strains:
        if state == sel_state: continue
        comp_profile += state + ":"
        
        k = len(state_strains[state])
        i = 0
        while i < k:
            strain_index = state_strains[state][i]
            comp_profile += str(strain_index)
            j = 1
            while i+j<k and state_strains[state][i+j] == strain_index+j:
                j = j + 1
            i = i + j
            
            if j == 1: comp_profile += ","
            elif j == 2: comp_profile += "," + str(state_strains[state][i-1]) + ","
            else: comp_profile += "_" + str(state_strains[state][i-1]) + ","
                       
        if comp_profile.endswith(","): comp_profile = comp_profile[:-1] + ";"

    if comp_profile.endswith(";"): comp_profile = comp_profile[:-1]
    return comp_profile

def subsets(my_set):
    result = [[]]
    for x in my_set:
        result = result + [y + [x] for y in result]
    return result

def calcMutStatesMutation(m_profile, ref_state, n, non_mut_states="?-", ref_counts=False, mut_counts=True):
    state_pos = {}
    state_counts = {}
    if m_profile[1] == " ":
        tokens = m_profile.split()

        total_count = 0

        sel_state = tokens[0]
        sel_positions = set(range(n+1))
        tokens2 = tokens[1].split(";")

        for state_token in tokens2:
            state, pos_tokens_tmp = state_token.split(":")
            pos_tokens = pos_tokens_tmp.split(",")
            pos_count = 0
            for pos_token in pos_tokens:
                if pos_token.count("_") == 1:
                    pos_from, pos_to = map(int, pos_token.split("_"))
                    pos_count += pos_to - pos_from + 1
                    state_pos[state] = pos_to
                    sel_positions -= set(range(pos_from, pos_to+1, 1))
                else:
                    pos = int(pos_token)
                    pos_count += 1
                    state_pos[state] = pos
                    sel_positions.remove(pos)

            state_counts[state] = pos_count
            total_count += pos_count
        state_counts[sel_state] = n - total_count
        state_pos[sel_state] = min(sel_positions)

    else:
        for i in range(n):
            state = m_profile[i]
            if not state in state_counts:
                state_counts[state] = 0
            state_counts[state] += 1

            if not state in state_pos:
                state_pos[state] = i
    mut_states_sorted = sorted(state_pos, key=lambda state: state_pos[state])
    mut_states_txt_list = []


    for state in mut_states_sorted:
        if state == ref_state: continue
        if state in non_mut_states: continue
        if state_counts[state] != 1 and mut_counts:
            mut_states_txt_list.append(state + ":" + str(state_counts[state]))
        else:
            mut_states_txt_list.append(state)
    mut_states_txt = ",".join(mut_states_txt_list)

    #print(state_counts[ref_state], ref_counts)
    if state_counts[ref_state] != 1 and ref_counts:
        ref_state_txt = ref_state + ":" + str(state_counts[ref_state])
    else:
        ref_state_txt = ref_state

    return ref_state_txt, mut_states_txt

# import __builtin__
# __builtin__.__dict__.update(locals())

def calcRefStateMutation(full_profile, strain_subset=None):
    non_ref_states = ["?","*",'-']
    state_counts = {};
    n = len(full_profile)
    if strain_subset == None: strain_subset = range(n)
    for strain_index in strain_subset:
        state = full_profile[strain_index]
        if state in non_ref_states:
            continue
        if not state in state_counts:
            state_counts[state] = 0
        state_counts[state] += 1;
    
    if len(state_counts)== 0:
        strain_subset = range(n)
        for strain_index in strain_subset:
            state = full_profile[strain_index]
            if state in non_ref_states:
                continue
            if not state in state_counts:
                state_counts[state] = 0
            state_counts[state] += 1;
            
    if len(state_counts)== 0:
        return None
        
    state_counts_sorted = sorted(state_counts, key=lambda state: -state_counts[state])
    max_state = state_counts_sorted[0]
    
    return max_state

def calcBinProfileMutation(full_profile, ref_state, gen_bin_profile=False):
    n = len(full_profile)
    bin_profile_list = []
    if not gen_bin_profile:
        for i in range(n):
            state = full_profile[i]
            if state == "?": bin_profile_list.append("?")
            elif state == ref_state or state == '-': bin_profile_list.append("0")
            else: bin_profile_list.append("1")
    else:
        current_k = 1
        k_map = {}
        for i in range(n):
            state = full_profile[i]
            if state == "?": bin_profile_list.append("?")
            elif state == ref_state or state == '-': bin_profile_list.append("0")
            elif state in k_map: bin_profile_list.append(str(k_map[state]))
            else:
                k_map[state] = current_k
                bin_profile_list.append(str(current_k))
                current_k += 1
                
    return bin_profile_list

def getRefMutStates(full_profile, bin_profile):
    ref_states = set([])
    mut_states = set([])
    n = len(full_profile)
    for i in range(n):
        bin_state = bin_profile[i]
        state = full_profile[i]
        if state == "?": continue
        if bin_state == '1': mut_states.add(state)
        elif bin_state == '0': ref_states.add(state)
    return ref_states, mut_states
    
def calcRefPosition(position, start, end, strand):
    left_bound = min(start, end)
    right_bound = max(start, end)
    if strand == "+":
        if position <= 0: ref_position = left_bound + position
        else: ref_position = left_bound + (position-1)*3 + 1
    else:
        if position <= 0: ref_position = right_bound - position + 1
        else: ref_position = right_bound - (position-1)*3 - 1 
    
    return ref_position

def reverseBinProfileIDs(gene_profiles, muts=True):
    bin_profile_ids = {}
    for gene_profile in gene_profiles.values():
        for mutation in gene_profile.mutations.values():
            for bin_profile_id in mutation.bin_profiles:
                if not bin_profile_id in bin_profile_ids:
                    bin_profile_ids[bin_profile_id] = set([])
                bin_profile_ids[bin_profile_id].add(mutation)
    return bin_profile_ids

def mutsDescToLatex(mut_states_txt, rem_star=True):
    pat = "\w:\d+|\*:\d+|\w"
    matches = re.findall(pat, mut_states_txt)

    lm = {}
    res = ""
    for m in matches:
        tokens = m.split(":")
        if len(tokens) == 1: k = 1
        else: k = int(tokens[1])
        letter = tokens[0]
        if letter == "*" and rem_star:
            continue
        lm[letter] = "\\textsubscript{"+str(k)+"}"

    for letter in sorted(lm):
        res += letter+lm[letter]
    return res

def mutsDescToMathLatex(mut_states_txt, rem_star=True):
    pat = "\w:\d+|\*:\d+|\w"
    matches = re.findall(pat, mut_states_txt)

    res = ""
    for m in matches:
        tokens = m.split(":")
        if len(tokens) == 1: k = 1
        else: k = int(tokens[1])
        letter = tokens[0]
        if letter == "*" and rem_star:
            continue
        res += letter+"$_{"+str(k)+"}$"
    return res
