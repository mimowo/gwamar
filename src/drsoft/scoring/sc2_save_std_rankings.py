import sys
import os
import multiprocessing
from multiprocessing import Pool

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_scoring,\
  gwamar_res_io_utils, gwamar_bin_utils, gwamar_utils,\
  gwamar_progress_utils

sys.path.append("../../")


def scoreParamsSet(params):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    score = params["SCORE"]

    rev_order = gwamar_scoring.revOrder(score)
    
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    ranks_dir = parameters["RESULTS_RANKS_DIR"]
    results_dir = parameters["RESULTS_DIR"]

    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    input_fh = open(results_dir + '/bin_profiles_' + parameters["MT"] + '_count.txt')
    bin_profile_count = gwamar_bin_utils.readBinProfileCounts(input_fh)
    input_fh.close()

    for drug_res_profile in res_profiles_list:
        drug_name = drug_res_profile.drug_name
        gwamar_utils.ensure_dir(ranks_dir + drug_name)
        
        input_fn = scores_dir + drug_name + "/" + score + ".txt"
        if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 10:
            print("NOT EXIST: ", input_fn)
            continue
        
        input_fh = open(input_fn)
        bin_profiles_sorted = gwamar_bin_utils.readBinProfilesSorted(input_fh, rev_order)
        input_fh.close()
        
        bin_profiles_ranks = gwamar_bin_utils.computeRankings(bin_profiles_sorted, bin_profile_count)

        tls = []
        for bin_profile_id, _ in bin_profiles_sorted:
            ranks_list = list(bin_profiles_ranks[bin_profile_id])

            tl = "\t".join(map(str, [bin_profile_id] + ranks_list)) + "\n"
            tls.append(tl)
    
        output_fn = ranks_dir + str(drug_name) + "/" + str(score) + ".txt"    
        output_fh = open(output_fn, "w")
        for tl in tls:
            output_fh.write(tl)
        output_fh.close()
        
    return score

if __name__ == '__main__':
    print("sc2_save_std_rankings.py: computation of rankings.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ranks_dir = parameters["RESULTS_RANKS_DIR"]
    gwamar_utils.ensure_dir(ranks_dir)
    
    score_params = parameters["SCORES"].split(",")
    
    TASKS = []
    TASK = {}
    
    for score_param in score_params:
        TASK["SCORE"] = score_param

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Rankings computed")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))

    if WORKERS > 1:
        pool = Pool(processes=WORKERS)   
        for r in pool.imap(scoreParamsSet, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreParamsSet(T)
            progress.update(str(r))
            
    print("sc2_save_std_rankings.py. Finished")
    