import sys
import os
import math
from src.drsoft.utils import gwamar_strains_io_utils

def readParameters(param_tab):
    parameters = {}
    for param_el in param_tab:
        tokens = param_el.split("=")
        if len(tokens)==2:
            key = tokens[0]
            value = tokens[1]
            parameters[key] = value
        
    return parameters

def loadDrugsATC(input_fh):
    #drugs_list = []
    drugs_map = {}
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens) == 0:
            continue
        drug_name = tokens[0]
        atc_name = tokens[1]
        if atc_name == "x":
            continue
        if not drug_name in drugs_map:
            drugs_map[drug_name] = atc_name
    drugs_list = sorted(drugs_map, key=lambda dn:drugs_map[dn])
    return drugs_list, drugs_map
    
def loadDrugsFromData(input_fh):
    drugs_list = []
    
    for line in input_fh.readlines():
        if not line:
            continue
        line = line.strip()
        tokens = line.split()
        if len(tokens) > 0:
            drug_name = tokens[0]
            strain_id = tokens[1]
            res_info = tokens[2]
            cite = tokens[3]
            if not drug_name in drugs_list:
                drugs_list.append(drug_name)
    
    return drugs_list

def loadResData(input_fh):
    res_data_map = {}
    
    for line in input_fh.readlines():
        if not line:
            continue
        tokens = line.strip().split("\t")
        
        if len(tokens) == 4:
            try:
                print("here")
                drug_name = tokens[0]
                if len(drug_name) > 0 and drug_name[0] == "#":
                    continue
                
                strain_id = tokens[1].replace("/", "")
                res_info = tokens[2]
                cite = tokens[3]
                if not strain_id in res_data_map:
                    res_data_map[strain_id] = {}
                if not drug_name in res_data_map[strain_id]:
                    res_data_map[strain_id][drug_name] = set([])
                res_data_map[strain_id][drug_name].add((res_info, cite))
            except:
                print("ERROR", line)
        elif len(tokens) == 5:
            
            try:
                drug_name = tokens[0]
                if len(drug_name) > 0 and drug_name[0] == "#":
                    continue
                
                strain_id = tokens[1].replace("/", "")
                res_info = tokens[2] + ":" + tokens[3]
                cite = tokens[4]
                if not strain_id in res_data_map:
                    res_data_map[strain_id] = {}
                if not drug_name in res_data_map[strain_id]:
                    res_data_map[strain_id][drug_name] = set([])
                res_data_map[strain_id][drug_name].add((res_info, cite))
            except:
                print("ERROR", line)   
    return res_data_map

def translateDataIntoProbResProfiles(res_data, breakpoints_map):
    res_profiles = {}
    for strain_id in res_data:
        for drug_name in res_data[strain_id]:
            
            if not drug_name in breakpoints_map:
                continue
            
            mic_bp = breakpoints_map[drug_name]
            
            if not drug_name in res_profiles:
                res_profiles[drug_name] = []           
            for (drug_res_info, cite_info) in res_data[strain_id][drug_name]:
                tokens = drug_res_info.split(":")
                if (len(tokens) == 1):
                    res_profiles[drug_name].append((strain_id, tokens[0], None))
                else:
                    mic_value_str = tokens[1].strip("<").strip(">").strip("=")
                    if len(mic_value_str) == 0:
                        res_profiles[drug_name].append((strain_id, tokens[0], None))
                        continue
                    mic_value = float(mic_value_str)
                    c = math.log(2) / mic_bp
                    prob = 1.0 - math.exp(-c*mic_value)
                    res_profiles[drug_name].append((strain_id, tokens[0], prob))
               #     print(strain_id, drug_name, mic_value, mic_bp, prob)
                                  
    return res_profiles

breakpoints_map = {"Penicillin2" : 4,
                   "Penicillin1" : 2,
                   "Tetracycline" : 2,
                   "Erythromycin": 1,
                   "Chloramphenicol": 8
                   }

if __name__ == '__main__':
    species = "spe_res"
    input_dir = os.path.abspath("../../../data_input/"+species+"/")+"/"
    output_dir = os.path.abspath("../../../data_input/"+species+"/")+"/"
    
    input_fh = open(input_dir + "strains-all.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(input_dir + "res-data.txt")
    res_data = loadResData(input_fh)
    input_fh.close()
    
    res_profile_probs = translateDataIntoProbResProfiles(res_data, breakpoints_map)
    
    output_fh = open(input_dir + "res_profiles_ret_prob.txt", "w")
    for drug_name in res_profile_probs:
        res_profile = res_profile_probs[drug_name]

        output_fh.write(drug_name+"\t")
        
        text = ""
        for (strain_id, desc, prob) in res_profile:
            if prob == None:
                text += strain_id+":"+desc+";"
            else:
                text += strain_id+":"+desc+":"+str("%2f" % prob)+";"
        text = text[:-1]
        output_fh.write(text + "\n")
    output_fh.close() 
    
    
    
    
    
        