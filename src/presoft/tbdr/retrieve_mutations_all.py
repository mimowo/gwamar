import sys
import os
from src.drsoft.utils import gwamar_params_utils, gwamar_utils

codon_map = {"Phe":"F", 
             "Leu":"L",
             "Ile": "I",
             "Met": "M",
             "Val": "V",
             "Ser": "S",
             "Pro": "P",
             "Thr": "T",
             "Ala": "A",
             "Tyr": "Y",
             "His": "H",
             "Gln": "Q",
             "Asn": "N",
             "Lys": "K",
             "Asp": "D",
             "Glu": "E",
             "Cys": "C",
             "Trp": "W",
             "Arg": "R",
             "Ser": "S",
             "Gly": "G"
             }


if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    #species = "sta_plas_184"
    text_lines = []
    dataset = "tbdr"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"
    gwamar_utils.ensure_dir(dataset_dir)
    muts_list = []
    
    drug_name = ""
    
    mut_details = set([])
    genes_set = set([])
    genes_map = {}
    
    input_fn = dataset_dir + "tbdr_proc_all.txt"
    input_fh = open(input_fn)
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if line.count("[") > 0:
            drug_name = line.split()[0].title()
            continue
        if line.startswith("#"): continue
        elif line.count("reference") > 0: continue
        elif line.count("EMB-induced gene") > 0: continue
        elif len(tokens) < 7: continue
        else:
            gene_name = tokens[1]
            if gene_name == "N/A": continue
            
           # print(tokens)
            if len(tokens[3]) == 0: mut_pos_nt = ""
            else: mut_pos_nt = tokens[3].split()[0]
            mut_desc = tokens[4]
            if len(tokens[5]) == 0: mut_pos_aa = ""
            else: mut_pos_aa = tokens[5].split()[0]
            mut_desc_aa = tokens[6]
            
            
            if mut_pos_nt.startswith("-") or mut_desc.startswith("-") or gene_name == "rrs":
                mut_desc = mut_desc.replace("/", " ")
                mut_desc = mut_desc.replace(",", " ")
                mut_desc = mut_desc.replace("(", " ")
                mut_desc = mut_desc.replace(")", " ")
                mut_desc_tokens = mut_desc.strip().split()
                
                if len(mut_desc_tokens[0]) > 1: continue
                position = mut_pos_nt
                if mut_desc_tokens[0] in ["del", "ins"]:
                    ref_states = "X"
                    mut_states = "*"
                else:
                    ref_states = mut_desc_tokens[0]
                    mut_states = mut_desc_tokens[1]
            else:
                mut_desc_aa = mut_desc_aa.replace("/", " ")
                mut_desc_aa = mut_desc_aa.replace(",", " ")
                mut_desc_aa = mut_desc_aa.replace("(", " ")
                mut_desc_aa = mut_desc_aa.replace(")", " ")
                mut_desc_tokens = mut_desc_aa.split()

                if len(mut_desc_tokens) == 0: continue
                ref_states = mut_desc_tokens[0]
                #if drug_name.upper() == "PYRAZINAMIDE":
                    #print(mut_desc_tokens, line)
                if mut_desc.endswith("STOP") or len(mut_desc_tokens) < 2: 
                    if len(ref_states) > 3 or ref_states in ["del", "ins"]: ref_states = "X"
                    mut_states = "*"
                    position = mut_pos_aa
                    
                    if position.count("-") > 0: continue
                    elif len(mut_pos_aa) == 0 and len(mut_pos_nt) > 0: 
                        mut_pos_nt = mut_pos_nt.replace("-", " ")
                        mut_pos_nt = mut_pos_nt.split()[0]
                        position = str((int(mut_pos_nt)-3)/3 + 1)
                        
                    if len(position) ==0: continue
                    ref_states = codon_map.get(ref_states, ref_states)
                    mut_states = codon_map.get(mut_states, mut_states)
                    
                    #print(position, line)
                    
                    if len(ref_states) > 1: ref_states = "X"
                    if len(mut_states) > 1: mut_states = "*"
                else: 
                    if len(ref_states) > 3 or ref_states in ["del", "ins"]: ref_states = "X"     
                    if len(mut_desc_tokens[1]) > 3: mut_states = "*"
                    else: mut_states = mut_desc_tokens[1]
                    position = mut_pos_aa
                    if position.count("-") > 0: continue
                    elif len(mut_pos_aa) == 0 and len(mut_pos_nt) > 0: 
                        mut_pos_nt = mut_pos_nt.replace("-", " ")
                        mut_pos_nt = mut_pos_nt.split()[0]
                        position = str((int(mut_pos_nt)-3)/3 + 1)                        
                    
                    if len(position) ==0: continue
                    ref_states = codon_map.get(ref_states, ref_states)
                    mut_states = codon_map.get(mut_states, mut_states)
                    if len(ref_states) > 1: ref_states = "X"
                    if len(mut_states) > 1: mut_states = "*"
                    
            
            if gene_name == "rpoB" and int(position) > 0:
             #   print("b", position)
                position_int = int(position) - (526-445)
                position = str(position_int)
              #  print("a", position)
                    
            if gene_name.startswith("Rv"): gene_id = gene_name
            elif gene_name == "thyA": gene_id = "Rv2764c"
            elif gene_name == "katG": gene_id = "Rv1908c"
            elif gene_name == "mabA, fabG1": gene_name, gene_id = "fabG1-inhA", "Rv1483"
            elif gene_name == "mabA": gene_name, gene_id = "fabG1-inhA", "Rv1483"
            elif gene_name == "inhA": gene_id = "Rv1484"
            elif gene_name == "ahpC": gene_id = "Rv2428"
            elif gene_name == "rpoB": gene_id = "Rv0667"
            elif gene_name == "gyrA": gene_id = "Rv0006"
            elif gene_name == "gyrB": gene_id = "Rv0005"
            elif gene_name == "rrs": gene_id = "Rvnr01"
            elif gene_name == "rrl": gene_id = "Rvnr02"
            elif gene_name == "pncA": gene_id = "Rv2043c"
            elif gene_name == "OxyR": gene_id = "Rv2427c"
            elif gene_name == "tlyA": gene_id = "Rv1694"
            elif gene_name == "embB": gene_id = "Rv3795"
            elif gene_name == "embA": gene_id = "Rv3794"
            elif gene_name == "embC": gene_id = "Rv3793"
            elif gene_name == "embR": gene_id = "Rv1267c"
            elif gene_name == "rpsL": gene_id = "Rv0682"
            elif gene_name == "iniA": gene_id = "Rv0342"
            elif gene_name == "iniB": gene_id = "Rv0341"
            elif gene_name == "iniC": gene_id = "Rv0343"
            elif gene_name == "rmlA2": gene_id = "Rv3264c"
            elif gene_name == "ethA/etaA": gene_name, gene_id = "ethA", "Rv3854c"
            elif gene_name == "accD6": gene_id = "Rv2247"
            elif gene_name == "efpA": gene_id = "Rv2846c"
            elif gene_name == "ndh": gene_id = "Rv1854c"
            elif gene_name == "nhoA": gene_id = "Rv3566c"
            elif gene_name == "fabD": gene_id = "Rv2243"
            elif gene_name == "fbpC": gene_id = "Rv0129c"
            elif gene_name == "furA": gene_id = "Rv1909c"
            elif gene_name == "kasA": gene_id = "Rv2245"
            elif gene_name == "OxyR-ahpC intergenic": gene_name, gene_id = "ahpC", "Rv2428"
            elif gene_name == "rmlD": gene_id = "Rv3266c"
            elif gene_name == "gidB": gene_name, gene_id = "gid", "Rv3919c"
            elif gene_name.count("homolog") > 0: continue
            else: gene_id = "x"
            
         #   print(drug_name)
            if drug_name == "Para-Aminosalisylic":
                genes_set.add((drug_name, gene_name, gene_id))
                if not drug_name in genes_map: genes_map[drug_name] = set([])
                genes_map[drug_name].add(gene_id)
                mut_details.add(("Para-Aminosalisylic_acid", gene_name, gene_id, int(position), ref_states, mut_states))
            elif drug_name == "Fluoroquinolones":
                for drug_name_tmp in ["Ciprofloxacin", "Ofloxacin", "Moxifloxacin", "Levofloxacin"]:
                    genes_set.add((drug_name_tmp, gene_name, gene_id))
                    if not drug_name in genes_map: genes_map[drug_name] = set([])
                    genes_map[drug_name].add(gene_id)
                    mut_details.add((drug_name_tmp, gene_name, gene_id, int(position), ref_states, mut_states))
            elif drug_name == "Aminoglycosides":
                for drug_name_tmp in ["Amikacin", "Capreomycin", "Kanamycin"]:
                    genes_set.add((drug_name_tmp, gene_name, gene_id))
                    if not drug_name in genes_map: genes_map[drug_name] = set([])
                    genes_map[drug_name].add(gene_id)
                    mut_details.add((drug_name_tmp, gene_name, gene_id, int(position), ref_states, mut_states))
            else:
                genes_set.add((drug_name, gene_name, gene_id))
                if not drug_name in genes_map: genes_map[drug_name] = set([])
                genes_map[drug_name].add(gene_id)
                mut_details.add((drug_name, gene_name, gene_id, int(position), ref_states, mut_states))
    input_fh.close()
    
    mut_details_sorted = sorted(mut_details)
    
    output_fh = open(dataset_dir + "tbdr_muts_all.txt", "w")
    for (drug_name, gene_name, gene_id, position, ref_states, mut_states) in mut_details_sorted:
        text_line = drug_name + "\t" + gene_name + "\t" + gene_id + "\t" + str(position) + "\t" + ref_states + "\t" + mut_states + "\n"
        output_fh.write(text_line)
    output_fh.close()
    
    genes_details_sorted = sorted(genes_set)
    
    output_fh = open(dataset_dir + "tbdr_genes_all.txt", "w")
    for drug_name in genes_map:
        text_line = drug_name + "\t" 
        for gene_id in genes_map[drug_name]:
            text_line += gene_id + ";"
        if text_line.endswith(";"): text_line = text_line[:-1] + "\n"
        output_fh.write(text_line)
    output_fh.close()
        