from collections import deque

from drsoft.modelling.model_logic import *
from drsoft.utils.gwamar_stat_utils import *
from src.drsoft.utils import gwamar_generator_utils, gwamar_stat_utils

def calcGeneResTreeSupports(gene_profile, res_profile, tree):
    if gene_profile.mutationsCount() == 1 and gene_profile.getMutation().res_tree_support > 0:
        return gene_profile.getMutation().res_tree_support
    elif (len(gene_profile.getMutated() & res_profile.getSusceptible()) >0):
        return 0;
    else:
        tree.setResMutation(gene_profile.bin_profile, res_profile)
        return tree.calcChanges()      
    
def calcWnmMAT(tree, n, m):
    queue = deque([])
    count_map = {}
    
    for node in tree.nodes.values():
        node.visited = 0
        count_map[node.node_id] = {}
        for r in range(n+1):
            for s in range(m + 1):
                if (r == 0 and s == 0):
                    count_map[node.node_id][(r,s,1)] = 1
                    count_map[node.node_id][(r,s,0)] = 1
                else:
                    count_map[node.node_id][(r,s,1)] = 0
                    count_map[node.node_id][(r,s,0)] = 0                

    for i in tree.leaf_ids:
        node = tree.nodes[i]
        queue.append(node.parent)
        
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        node.visited += 1
        if node.visited < len(node.children):
            continue
        i= node.node_id
        
        children_list = list(node.children)
        
        nx = min(n, node.subnodes_count)
        mx = min(m, node.subnodes_count)
        
        for nt in range(nx+1):
            for mt in range(mx+1):
                wnm = 0
                
                for r in range(nt + 1):
                    for gain_profile in gwamar_generator_utils.generateSumProfiles(len(children_list), r, 1):
                        for gain_subprofile in gwamar_generator_utils.generateSumProfiles(len(children_list), nt-r, nt):
                            wnm_tmp = 1
                            for j in range(len(children_list)):
                                node_id_j = children_list[j].node_id
                                
                                wnm_tmp *= count_map[node_id_j].get((gain_subprofile[j], 0, gain_profile[j]), 0)
                                if wnm_tmp == 0:
                                    break
                            wnm += wnm_tmp
                count_map[node.node_id][(nt,mt,0)] = wnm
        queue.append(node.parent)
    return count_map[tree.root.node_id]

    
def calcBpqnmMAT(tree, p, q, n, m):
    queue = deque([])
    count_map = {}
    
    p = min(p,n)
    q = min(q,m)
    
    for node in tree.nodes.values():
        node.visited = 0
        count_map[node.node_id] = {}
        for r in range(n+1):
            for s in range(m+1):
                for t in range(0, min(p,r)+1, 1):
                    for u in range(0, min(q,s)+1, 1):
                        if (t == 0 and u == 0 and r == 0 and s == 0):
                            count_map[node.node_id][(t,u,r,s,1)] = 1
                            count_map[node.node_id][(t,u,r,s,0)] = 1
                        else:
                            count_map[node.node_id][(t,u,r,s,1)] = 0
                            count_map[node.node_id][(t,u,r,s,0)] = 0                

    for i in tree.leaf_ids:
        node = tree.nodes[i]
        queue.append(node.parent)
        
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        node.visited += 1
        if node.visited < len(node.children):
            continue
        i = node.node_id
        
        children_list = list(node.children)
        
        nx = min(n, node.subnodes_count)
        mx = min(m, node.subnodes_count)
        px = min(nx,p)
        qx = min(mx,q)
        
   #     print("XXX", px, qx, nx, mx)

        for nt in range(nx+1):
            for mt in range(mx+1):        
                for pt in range(0, min(px,nt)+1, 1):
                    for qt in range(0, min(qx,mt)+1, 1):
                        wnm = 0
                        
                        for r in range(nt + 1):
                            for gain_r_profile in gwamar_generator_utils.generateSumProfiles(len(children_list), r, 1):
                                ur = 0
                                for j in range(len(children_list)):
                                    node_j = children_list[j]
                                    if gain_r_profile[j] == 1 and node.complete_res_state == "S" and node_j.complete_res_state == "R":
                                        ur += 1
                                        
                                for gain_r_subprofile in gwamar_generator_utils.generateSumProfiles(len(children_list), nt-r, nt):
                                    for gain_u_subprofile in gwamar_generator_utils.generateSumProfiles(len(children_list), pt-ur, pt):
                                        wnm_tmp = 1
                                        for j in range(len(children_list)):
                                            node_id_j = children_list[j].node_id
                                            
                                            if gain_u_subprofile[j] > gain_r_subprofile[j]:
                                                wnm_tmp = 0
                                                break
                                            wnm_tmp *= count_map[node_id_j].get((gain_u_subprofile[j], 0, gain_r_subprofile[j], 0, gain_r_profile[j]),0)
                                            if wnm_tmp == 0:
                                                break
                                        wnm += wnm_tmp
                    #    print(pt,qt,nt, mt, 0, wnm)
                        count_map[node.node_id][(pt,qt,nt, mt, 0)] = wnm
        queue.append(node.parent)
        
    return count_map[tree.root.node_id]
        
def calcRSPQ(tree):
    p=0
    q=0
    r=0
    s=0
    queue = deque([])
    
    for node in tree.nodes.values():
        node.visited = 0

    for i in tree.leaf_ids:
        node = tree.nodes[i]      
        queue.append(node.parent)
        
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        node.visited += 1
        if node.visited < len(node.children):
            continue
        i = node.node_id
        
        children_list = list(node.children)
        
        for j in range(len(children_list)):
            node_j = children_list[j]
            if node.complete_res_state == "S" and node_j.complete_res_state == "R":
                if node.complete_mut_state == '0' and node_j.complete_mut_state == "1":
                    p += 1       
            elif node.complete_res_state == "R" and node_j.complete_res_state == "S":
                if node.complete_mut_state == '1' and node_j.complete_mut_state == "0":
                    q += 1
            if node.complete_mut_state == "0" and node_j.complete_mut_state == "1":
                r += 1
            elif node.complete_mut_state == "1" and node_j.complete_mut_state == "0":
                s += 1
                print("SSS", node.node_id, node_j.node_id, node.complete_mut_state, node_j.complete_mut_state)
                for i in sorted(tree.nodes.keys()):
                    node_t = tree.nodes[i]
                    print("E", node_t.node_id, node_t.complete_mut_state, node_t.q_subleaves_count, node_t.abs_subleaves_count, node_t.pres_subleaves_count, node_t.subleaves_count)
                print(tree)
            
        #print(i,node.visited, len(children_list), r, s, p, q, n, m)
                    #    print(pt,qt,nt, mt, 0, wnm)
        queue.append(node.parent)

    return r,s,p,q


    
def calcNM(tree):
    n=0
    m=0
    queue = deque([])
    
    for node in tree.nodes.values():
        node.visited = 0

    for i in tree.leaf_ids:
        node = tree.nodes[i]      
        queue.append(node.parent)
        
    while queue:
        node = queue.popleft()
        if node == None:
            continue
        node.visited += 1
        if node.visited < len(node.children):
            continue
        i = node.node_id
        
        children_list = list(node.children)
        
        for j in range(len(children_list)):
            node_j = children_list[j]
            if node.complete_res_state == "S" and node_j.complete_res_state == "R":
                n += 1   
            elif node.complete_res_state == "R" and node_j.complete_res_state == "S":
                m += 1  
        queue.append(node.parent)

    return n,m

def calcFastN(tree):
    for node in tree.leaves:
        node.maxp = 0
    for node in tree.inner_nodes_bu:
        node.maxp = 0
        for node_j in node.children:
            if node.complete_res_state == "S" and node_j.complete_res_state == "R":
                node.maxp += 1
            else:
                node.maxp += node_j.maxp
    return tree.root.maxp

def calcFastWnMAT(tree, maxrparam=None, opt=False):
    for node in tree.leaves:
        node.count_map = {}                             
        node.count_map[0] = 1
        node.maxn = 0

    for node in tree.inner_nodes_bu:
        node.count_map = {}
        children_list = node.children
        k = len(node.children)

        node.maxn = 0
        for node_j in node.children:
            node.maxn += max(node_j.maxn, 1)

        if maxrparam == None: maxrp = node.maxn
        else: maxrp = min(maxrparam, node.maxn)

        for nt in range(maxrp+1):
          #  print("x", node.node_id, nt,k )
            maxr = min(nt, k-1, node.maxn)
            
            if opt and (k+1 == node.subnodes_count):
                if k == nt: wnm = 0
                else: wnm = gwamar_stat_utils.binom(k, nt)
            else:
                wnm = 0
                for r in range(maxr+1):
                    for gain_profile in gwamar_generator_utils.generateSumProfiles(k, r, 1):
                        kz = 0;
                        gain_bound = []
                        for j in range(k): 
                            if gain_profile[j] == 0: 
                                kz += 1
                                gain_bound.append(node.maxn)
                        for gain_subprofile in gwamar_generator_utils.generateSumProfiles(kz, nt-r, nt-r, gain_bound[::-1]):
                            jz = 0
                            wnm_tmp = 1
                            for j in range(k):
                                if gain_profile[j] == 0:
                                    node_j = node.children[j]
                                    wnm_tmp *= node_j.count_map.get(gain_subprofile[jz], 0)
                                    jz += 1
                                    if wnm_tmp == 0:
                                        break    
                            wnm += wnm_tmp
            node.count_map[nt] = wnm
    return tree.root.count_map

def calcFastRP(tree):
    p,r=0,0
    for node in tree.inner_nodes_bu:
        for node_j in node.children:
            if node.complete_mut_state == '0' and node_j.complete_mut_state == "1":
                r += 1
                if node.complete_res_state == "S" and node_j.complete_res_state == "R":
                    p += 1
    return r,p

def calcFastBpnMAT(tree, p, maxrparam, opt=False):
    for node in tree.leaves:
        node.count_map = {}
        node.count_map[(0,0)] = 1

    t = 0
    for node in tree.inner_nodes_bu:
        t += 1   
        node.count_map = {}   
        children_list = list(node.children)
        k = len(children_list)

        if maxrparam == None: maxrp = node.maxn
        else: maxrp = min(maxrparam, node.maxn)

        for nt in range(maxrp + 1): # liczba wybranych
            print("y", t, len(tree.inner_nodes_bu), nt, maxrp, node.maxn, k)
            maxp = min(node.maxp, nt)
            for pt in range(maxp + 1):     # liczba pokrytych

                if opt and (k+1 == node.subnodes_count):
                    if k == nt: wnm = 0
                    else: wnm = gwamar_stat_utils.binom(node.maxp, pt) * gwamar_stat_utils.binom(k-node.maxp, nt-pt)
                else:
                    wnm = 0
                    maxr = min(k-1, node.maxn, nt)

                    for r in range(maxr + 1):
                        for gain_r_profile in gwamar_generator_utils.generateSumProfiles(k, r, 1):
                            ur = 0 # liczba trafionych
                            kz = 0 # liczba 0
                            gain_r_bound = []
                            gain_u_bound = []
                            for j in range(k):
                                node_j = children_list[j]
                                if gain_r_profile[j] == 1 and node.complete_res_state == "S" and node_j.complete_res_state == "R":
                                    ur += 1
                                elif gain_r_profile[j] == 0:
                                    kz += 1
                                    gain_r_bound.append(node_j.maxn)
                                    gain_u_bound.append(node_j.maxp)

                            for gain_r_subprofile in gwamar_generator_utils.generateSumProfiles(kz, nt-r, nt-r, gain_r_bound[::-1]):
                                gain_p_bound = []
                                for jz in range(kz):
                                    gain_p_bound.append(min(gain_u_bound[jz], gain_r_subprofile[jz]))
                                for gain_u_subprofile in gwamar_generator_utils.generateSumProfiles(kz, pt-ur, pt-ur, gain_p_bound[::-1]):
                                    wnm_tmp = 1
                                    jz = 0
                                    for j in range(k):
                                        if gain_r_profile[j] == 0:
                                            node_j = children_list[j]
                                            wnm_tmp *= node_j.count_map.get((gain_u_subprofile[jz], gain_r_subprofile[jz]),0)
                                            if wnm_tmp == 0:
                                                break
                                            jz += 1

                                    wnm += wnm_tmp

                node.count_map[(pt,nt)] = wnm
    return tree.root.count_map
