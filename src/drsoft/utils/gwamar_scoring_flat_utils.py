import os
import math

from src.drsoft.utils.gwamar_stat_utils import hyperTestPvalue
from src.drsoft.utils import gwamar_bin_utils

def calcBinProfilesHyperTest(bin_profiles, drp):
    scores = {}
    resistant = drp.getResistant()
    susceptible = drp.getSusceptible()
    intermediate = drp.getInterresistant()
    
    info_strains = resistant | susceptible | intermediate
    N = len(info_strains)
    m = len(drp.getResistant())
        
    for bin_profile_id, gen_bin_profile in bin_profiles.items():
        score = 0.0
        for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
            mutated = gwamar_bin_utils.getMutated(bin_profile) & info_strains
            n = len(mutated)
            k = len(mutated & resistant)
            
            try:
                hyper = hyperTestPvalue(N,n,m,k)
            except:
                hyper = 0.0
                print("error", N,n,m,k)
            if hyper > 0.0: score += -math.log(hyper)
        scores[bin_profile_id] = score

    return scores


def calcBinProfilesMI(bin_profiles, drp):
    scores = {}
    resistant = drp.getResistant()
    susceptible = drp.getSusceptible()
    intermediate = drp.getInterresistant()
    
    info_strains = resistant | susceptible | intermediate
    n = len(info_strains)
    
    for bin_profile_id, gen_bin_profile in bin_profiles.items():
        score = 0.0
        for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
            mutated = gwamar_bin_utils.getMutated(bin_profile) & info_strains
            not_mutated = gwamar_bin_utils.getNotMutated(bin_profile) & info_strains
    
            pr0 = float(len(not_mutated & resistant)) / n
            pr1 = float(len(mutated & resistant)) / n
            ps0 = float(len(not_mutated & susceptible)) / n
            ps1 = float(len(mutated & susceptible)) / n
            pi0 = float(len(not_mutated & intermediate)) / n
            pi1 = float(len(mutated & intermediate)) / n
            pr = float(len(resistant)) / n
            ps = float(len(susceptible)) / n
            pi = float(len(intermediate)) / n
            p0 = float(len(not_mutated)) / n
            p1 = float(len(mutated)) / n
        
            mi = 0.0
            if pr0 > 0.0: mi += pr0*math.log(pr0/(pr*p0))
            if pr1 > 0.0: mi += pr1*math.log(pr1/(pr*p1))
            if ps0 > 0.0: mi += ps0*math.log(ps0/(ps*p0))
            if ps1 > 0.0: mi += ps1*math.log(ps1/(ps*p1))
            if pi0 > 0.0: mi += pi0*math.log(pi0/(pi*p0))
            if pi1 > 0.0: mi += pi1*math.log(pi1/(pi*p1))
            
            score += mi
        scores[bin_profile_id] = score

    return scores


def calcBinProfilesSupports(bin_profiles, drp, alpha=0.0):
    scores = {}
    
    resistant = drp.getResistant()
    susceptible = drp.getSusceptible()
    
    beta = float(len(resistant)/float(len(susceptible)))
    
    for bin_profile_id, gen_bin_profile in bin_profiles.items():
        score = 0.0
        for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
            mutated = gwamar_bin_utils.getMutated(bin_profile)
            not_mutated = gwamar_bin_utils.getNotMutated(bin_profile)
            
            mut_res = float(len(mutated & resistant))
            mut_susc = float(len(not_mutated & susceptible))
            support = mut_res - beta*mut_susc
            score += max(support, 0.0) + alpha * min(support, 0.0)
        scores[bin_profile_id] = max(0, score)
        
    return scores      

def calcBinProfilesRanksSumScore(bin_profiles, drug_name, ranks_dir, subscores=["mi", "or", "ts", "ws"]):
    scores = {}

    for bin_profile_id in bin_profiles.keys():
        scores[bin_profile_id] = 0.0
    
    for score_name in subscores:
        input_fn = ranks_dir + drug_name + "/" + score_name + ".txt"
        if not os.path.exists(input_fn):
            print("File not found: " + input_fn)
            return {}
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 2: continue
            bin_profile_id = tokens[0]
            r_min_pos, r_max_pos, r_min_i, r_max_i = map(int, tokens[1:])
            rank_avg_pos = float(r_min_pos + r_max_pos) / 2.0
            rank_avg_i = float(r_min_i + r_max_i) / 2.0
            scores[bin_profile_id] += rank_avg_i
        input_fh.close()
    return scores

def calcBinProfilesPermRanksSumScore(bin_profiles, drug_name, drug_name_id, ranks_dir, subscores=["mi", "or", "tscore", "wsupport"]):
    scores = {}

    for bin_profile_id in bin_profiles.keys():
        scores[bin_profile_id] = 0.0
    
    for score_name in subscores:
        input_fn = ranks_dir + drug_name + "/" + score_name + "/" + drug_name_id + ".txt"
        if not os.path.exists(input_fn):
            return {}
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 2: continue
            bin_profile_id = tokens[0]
            rank = float(tokens[1])
            scores[bin_profile_id] += rank
        input_fh.close()
    return scores

def calcBinProfilesOddsRatio(bin_profiles, drp, alpha=0.0):
    scores = {}
    resistant = drp.getResistant()
    susceptible = drp.getSusceptible()
    
    for bin_profile_id, gen_bin_profile in bin_profiles.items():
        score = 0.0
        for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
            mutated = gwamar_bin_utils.getMutated(bin_profile)
            not_mutated = gwamar_bin_utils.getNotMutated(bin_profile)
            
            rm = float(len(resistant & mutated))
            sm = float(len(susceptible & mutated))
            rnm = float(len(resistant & not_mutated))
            snm = float(len(susceptible & not_mutated))
    
            odds_r = rm * snm / (max(1, rnm) * max(1, sm))
            score += max(odds_r, 0.0) + alpha * min(odds_r, 0.0)
            
        scores[bin_profile_id] = max(0.0, score)
    
    return scores
    