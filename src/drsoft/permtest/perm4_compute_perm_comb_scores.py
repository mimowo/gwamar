import sys
import os
from multiprocessing import Pool
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils, gwamar_bin_utils, gwamar_res_utils,\
  gwamar_scoring_flat_utils, gwamar_utils, gwamar_progress_utils

def scoreDrug(params):
    score = params["SCORE"]
    drug_name = params["DRUG_NAME"]
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    input_dir = parameters["DATASET_INPUT_DIR"]
    perm_scores_dir = parameters["RESULTS_PERM_SCORES_DIR"]
    perm_profiles_dir = parameters["RESULTS_PERM_PROFILES_DIR"]
    perm_ranks_dir = parameters["RESULTS_PERM_RANKS_DIR"]    
    perm_rep = int(parameters["REPS"])
    
    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    input_fh = open(perm_profiles_dir + drug_name + ".txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()

    input_fh = open(results_dir + '/bin_profiles_' + parameters["MT"] + '.txt')
    bin_profiles = gwamar_bin_utils.readBinProfiles(input_fh, strains.count(), gen_bin_profiles=True)
    input_fh.close()
    
    for drug_name_id in drug_names:
        current_id = gwamar_res_utils.drugNamePermID(drug_name_id)
        if current_id >= perm_rep: continue
        
        rev_sorting = True
    
        subscores = score.split(",")

        bin_profile_scores = gwamar_scoring_flat_utils.calcBinProfilesPermRanksSumScore(bin_profiles, drug_name, drug_name_id, perm_ranks_dir, subscores=subscores)
                
        text_lines = []
        if rev_sorting:
            bin_profile_scores_sorted = sorted(bin_profile_scores, key=lambda bin_profile_id:bin_profile_scores[bin_profile_id])
        else:
            bin_profile_scores_sorted = sorted(bin_profile_scores, key=lambda bin_profile_id:-bin_profile_scores[bin_profile_id])
            
        for bin_profile_id in bin_profile_scores_sorted:
            bin_profile_score = bin_profile_scores[bin_profile_id]
            text_line = str(bin_profile_id) + "\t" + str(bin_profile_score) + "\n"
            text_lines.append(text_line)
    
    
        output_fn = perm_scores_dir + drug_name + "/" + score + "/" + drug_name_id +  ".txt"
        if os.path.exists(output_fn): continue
        
        output_fh = open(output_fn, "w") 
        for text_line in text_lines:
            output_fh.write(text_line)
        output_fh.close()
    
    return drug_name, score
    
if __name__ == '__main__':
    print("perm4_compute_perm_comb_scores.py. Start.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    perm_dir = parameters["RESULTS_PERM_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    perm_scores_dir = parameters["RESULTS_PERM_SCORES_DIR"]
    perm_ranks_dir = parameters["RESULTS_PERM_RANKS_DIR"]
    
    score_params = ["mi_0,or_0,ws_0,mts_0"]

    TASKS = []
    TASK = {}
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        for score_param in score_params:
            gwamar_utils.ensure_dir(perm_scores_dir + drug_name + "/" + score_param)

            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score_param
            
            TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Combined scores")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)   
        for r in pool.imap(scoreDrug, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreDrug(T)
            progress.update(str(r))

    print("perm4_compute_perm_comb_scores.py. Finished.")
