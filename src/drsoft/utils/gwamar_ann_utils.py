def readRefAnns(input_fh):
    ref_anns = {}
    
    for line in input_fh.readlines():
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split("\t")
        mg_tokens = tokens[1].split()
        gene_id = tokens[2]
        
        if len(gene_id) <= 1:
            continue
        
        len_tokens = tokens[3].split()
        end = int(mg_tokens[0])
        strand = mg_tokens[1]
       
        length = 0
        
        for len_token in len_tokens:
            if not len_token.startswith("#"):
                continue
            len_token = len_token[1:]
            if len_token.startswith("*"):
                len_token = len_token[1:]
            len_token = len_token.split(":")[0]
            length = int(len_token)
        ref_anns[gene_id] = (end, length, strand)    
    return ref_anns

def readClusterGeneIDs(input_fh, gene_ref_ids=False):
    input_lines = input_fh.readlines()
    
    cluster_ids = {}
    cluster_ids_rev = {}
    
    for line in input_lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2: continue
        cluster_id = tokens[0]
        gene_name = tokens[1]
        
        gene_tokens = tokens[2:]
        gene_ref_id = "x"
        
        for i in range(len(gene_tokens)):
            gene_id = gene_tokens[i]
            if len(gene_id) > 1: 
                if gene_ref_ids == False:
                    if not (cluster_id) in cluster_ids:
                        cluster_ids[cluster_id] = (gene_name, gene_id)
                    cluster_ids_rev[gene_id] = cluster_id
                else:
                    if len(gene_ref_id) <= 1: gene_ref_id = gene_id
                    if not gene_ref_id in cluster_ids:
                        cluster_ids[gene_ref_id] = (gene_name, gene_ref_id)
                    cluster_ids_rev[gene_id] = gene_ref_id
    return cluster_ids, cluster_ids_rev
