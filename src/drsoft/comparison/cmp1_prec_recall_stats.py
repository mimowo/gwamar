import random
import os
import sys
import multiprocessing
from multiprocessing import Pool

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_progress_utils, gwamar_utils,\
  gwamar_params_utils, gwamar_scoring


def selectAssocs(input_fn, subset="a1"):
    positives = []
    negatives = []
    all_negatives = []

    subset_tokens = subset[1:].split("_")
    subset_type = subset_tokens[0]
    if len(subset_tokens) >= 2:
        neg_frac = float(subset_tokens[1])
    else:
        neg_frac = 1.0

    if not os.path.exists(input_fn):
        return None, None

    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()

    k = len(lines)
    for i in range(k):
        tokens = lines[i].split()
        if len(tokens) < 4: continue
        drug_name_id, mutkey_id, all_db, hc_db, hc_neigh_db, gene_all_db, gene_hc_db, gene_broad = tokens[:8]
        val = float(tokens[8])
        s0, s1, i0, i1, r0, r1 = map(int, tokens[9:])

        recordY = ("Y", val, i)
        recordN = ("N", val, i)

        if subset_type == "0":
            if all_db == "Y": positives.append(recordY)
            else: negatives.append(recordN)

        elif subset_type == "1":
            if hc_db == "Y":  positives.append(recordY)
            else: negatives.append(recordN)

        elif subset_type == "2":
            if all_db == "Y": positives.append(recordY)
            if gene_all_db == "N" and all_db == "N": negatives.append(recordN)

        elif subset_type == "3":
            if hc_db == "Y": positives.append(recordY)
            if gene_hc_db == "N" and hc_db == "N": negatives.append(recordN)

        elif subset_type == "4":
            if all_db == "Y" and r1>0: positives.append(recordY)
            if gene_all_db == "N" and all_db == "N": negatives.append(recordN)
            if subset.startswith("p") and all_db == "N": all_negatives.append(recordN)

        elif subset_type == "5":
            if hc_db == "Y"  and r1>0: positives.append(recordY)
            if gene_hc_db == "N" and hc_db == "N": negatives.append(recordN)
            if subset.startswith("p") and hc_db == "N": all_negatives.append(recordN)

        elif subset_type == "6":
            if gene_all_db == "Y" and r1>0: positives.append(recordY)
            if gene_all_db == "N" and all_db == "N": negatives.append(recordN)

        elif subset_type == "7":
            if gene_hc_db == "Y" and r1>0: positives.append(recordY)
            if gene_hc_db == "N" and hc_db == "N": negatives.append(recordN)
            
        elif subset_type == "8":
            if gene_all_db == "Y": positives.append(recordY)
            if gene_all_db == "N" and all_db == "N": negatives.append(recordN)

        elif subset_type == "9":
            if gene_hc_db == "Y": positives.append(recordY)
            if gene_hc_db == "N" and hc_db == "N": negatives.append(recordN)

        elif subset_type == "10":
            if gene_all_db == "Y" and all_db == "Y": positives.append(recordY)
            if gene_all_db == "Y" and all_db == "N": negatives.append(recordN)
            if subset.startswith("p") and all_db == "N": all_negatives.append(recordN)

        elif subset_type == "11":
            if gene_hc_db == "Y" and hc_db == "Y": positives.append(recordY)
            if gene_hc_db == "Y" and hc_db == "N": negatives.append(recordN)
            if subset.startswith("p") and hc_db == "N": 
                all_negatives.append(recordN)

        elif subset_type == "12":
            if gene_broad == "Y" and all_db == "Y": positives.append(recordY)
            if gene_broad == "Y" and gene_all_db == "N" and all_db == "N": negatives.append(recordN)
            if subset.startswith("p") and all_db == "N": all_negatives.append(recordN)

        elif subset_type == "13":
            if gene_broad == "Y" and hc_db == "Y": positives.append(recordY)
            if gene_broad == "Y" and gene_hc_db == "N" and hc_db == "N": negatives.append(recordN)
            if subset.startswith("p") and hc_db == "N": all_negatives.append(recordN)

        elif subset_type == "14":
            if gene_broad == "Y" and all_db == "Y": positives.append(recordY)
            if gene_broad == "Y" and gene_all_db == "Y" and all_db == "N": negatives.append(recordN)
            if subset.startswith("p") and all_db == "N": all_negatives.append(recordN)

        elif subset_type == "15":
            if gene_broad == "Y" and hc_db == "Y": positives.append(recordY)
            if gene_broad == "Y" and gene_hc_db == "Y" and hc_db == "N": negatives.append(recordN)
            if subset.startswith("p") and hc_db == "N": all_negatives.append(recordN)

        elif subset_type == "16":
            if gene_broad == "Y" and all_db == "Y" and r1>0: positives.append(recordY)
            if gene_broad == "Y" and all_db == "N" and r1>0: negatives.append(recordN)
            if subset.startswith("p") and all_db == "N" and r1>0: all_negatives.append(recordN)

        elif subset_type == "17":
            if gene_broad == "Y" and hc_db == "Y" and r1>0: positives.append(recordY)
            if gene_broad == "Y" and hc_db == "N" and r1>0: negatives.append(recordN)
            if subset.startswith("p") and hc_db == "N" and r1>0: all_negatives.append(recordN)

        elif subset_type == "18":
            if gene_broad == "Y" and all_db == "Y" and r1>0: positives.append(recordY)
            if gene_broad == "Y" and gene_all_db == "Y" and all_db == "N" and r1>0: negatives.append(recordN)
            if subset.startswith("p") and all_db == "N" and r1>0: all_negatives.append(recordN)

        elif subset_type == "19":
            if gene_broad == "Y" and hc_db == "Y" and r1>0: positives.append(recordY)
            if gene_broad == "Y" and gene_hc_db == "Y" and hc_db == "N" and r1>0: negatives.append(recordN)
            if subset.startswith("p") and hc_db == "N" and r1>0: all_negatives.append(recordN)

    if subset.startswith("a"):
        negatives_sel = negatives
    elif subset.startswith("p"):
        negatives_count = len(negatives)
        negatives_sel = random.sample(all_negatives, negatives_count)
    elif subset.startswith("s"):
        if neg_frac > 1.0:
            negatives_count = min(len(negatives), int(neg_frac))
        elif neg_frac <= 1.0:
            negatives_count = min(len(negatives), len(negatives)*neg_frac)

        negatives_sel = random.sample(negatives, negatives_count)
    elif subset.startswith("e"):
        negatives_count = min(len(negatives), len(positives))
        negatives_sel = random.sample(negatives, negatives_count)
    else:
        print("Provide correct name of the sampling of negatives")
        sys.exit()

    return positives, negatives_sel

def calcStats(score, input_dir, revord=1, positives=None, negatives=None, thrs=None):

    if thrs == None:
        thrs = set([])
        for (db, val, i) in positives + negatives:
            thrs.add(val)

    n = len(negatives)
    p = len(positives)

    res_list = sorted(negatives + positives, key=lambda db_val_i:(-1*revord*db_val_i[1], db_val_i[2]))
    thrs_sorted = sorted(thrs, key=lambda val:-1*revord*val)

    tpc, fpc, tnc, fnc = 0,0,n,p
    auc = 0.0

    i = 0
    sc_val = res_list[i][1]

    stats_map = {}
    prec_prev = 1.0
    recall_prev = 0.0

    for thr_val in thrs_sorted:
        while i < n+p and ((sc_val >= thr_val and revord == 1) or (sc_val <= thr_val and revord == -1)):
            db, sc_val = res_list[i][:2]
            if db == "Y":
                tpc += 1; fnc -= 1
            else:
                fpc += 1; tnc -= 1  
            i += 1
        if i > 0:
            prec, recall = float(tpc) / i, float(tpc) / p
            auc += 0.5*(recall - recall_prev)*(prec + prec_prev)

            stats_map[thr_val] = (prec, recall, auc)
            prec_prev = prec
            recall_prev = recall

    return stats_map

def computePRs(params):
    score, subset, reps_tmp = params
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    cmp_stats_dir = parameters["RESULTS_CMP_STATS_DIR"]
    cmp_assocs_dir = parameters["RESULTS_CMP_ASSOCS_DIR"]
    
    overwrite = parameters.get("OW", "N")

    if subset.startswith("a"): reps = [0]
    else: reps = reps_tmp

    rev_order = gwamar_scoring.revOrder(score)

    stats_subset_dir = cmp_stats_dir + "/" + subset
    stats_subset_pr_dir = stats_subset_dir + "/pr/"
    stats_score_dir = stats_subset_pr_dir + "/" + score
    gwamar_utils.ensure_dir(stats_score_dir)

    for i in reps:
        output_fn = stats_score_dir + "/" + str(i) + ".txt"
        if os.path.exists(output_fn) and overwrite == "N":
            continue

        pos_sel, neg_sel = selectAssocs(cmp_assocs_dir + score  + ".txt", subset)

        stats_map = calcStats(score, cmp_assocs_dir, revord=-1*rev_order, positives=pos_sel, negatives=neg_sel)
        thrs_sorted = sorted(stats_map, key=lambda thr:rev_order*thr)

        tls = []
        for thr in thrs_sorted:
            (prec, recall, auc) = stats_map[thr]
            tl = "\t".join(map(str, [thr, prec, recall, auc]))
            tls.append(tl + "\n")

        output_fh = open(output_fn, "w")
        for tl in tls:
            output_fh.write(tl)
        output_fh.close()

    return score,subset, str(min(reps)) +"-"+str(max(reps))


if __name__ == '__main__':
    print("cmp1_prec_recall_stats.py: Start.")
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    cmp_dir = parameters["RESULTS_CMP_DIR"]
    cmp_stats_dir = parameters["RESULTS_CMP_STATS_DIR"]
    cmp_assocs_dir = parameters["RESULTS_CMP_ASSOCS_DIR"]
    
    gwamar_utils.ensure_dir(cmp_stats_dir)

    dataset = parameters["D"]

    reps_tmp = int(parameters.get("RAND", "1"))
    overwrite = parameters.get("OW", "N")
    scores_cmp = parameters.get("SCORES", "ws,or,mi,lh,r-tgh").split(",")

    score_fns = os.listdir(cmp_assocs_dir)
    scores_list = []
    for score in scores_cmp:
        score_fn = score + ".txt"
        if score_fn in score_fns:
            scores_list.append(score)

    subsets = parameters.get("SUBSETS", "p14,p15").split(",")
    
    for subset in subsets:
        stats_subset_dir = cmp_stats_dir + "/" + subset
        stats_subset_pr_dir = stats_subset_dir + "/pr/"

        gwamar_utils.ensure_dir(stats_subset_dir)
        gwamar_utils.ensure_dir(stats_subset_pr_dir)

        score = scores_list[0]
        pos_sel, neg_sel = selectAssocs(cmp_assocs_dir + score  + ".txt", subset)

        output_fh = open(stats_subset_dir + "/stats_pn.txt", "w")
        output_fh.write("ps\tns\n")
        output_fh.write(str(len(pos_sel)) + "\t" + str(len(neg_sel)) + "\n")
        #output_fh.write(str(len(pos_sel)) + "\t" + str(len(neg_sel)) + "\n")
        output_fh.close()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    tn = len(scores_list)*len(subsets)

    if reps_tmp > 1:
        k = max(int(WORKERS), 1)
    else:
        k = 1
    seed = max(int(reps_tmp / k), 0)+1
    
    TASKS = []
    for score in scores_list:
        for subset in subsets:
            for i in range(k):
                reps = list(range(i*seed, min(reps_tmp, (i+1)*seed), 1))
                if len(reps) > 0:
                    TASKS.append((score, subset, reps))

    progress = gwamar_progress_utils.GWAMARProgress("Prec-recall stats")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))

    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(computePRs, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = computePRs(T)
            progress.update(str(r))

    print("cmp1_prec_recall_stats.py: Finished.")
