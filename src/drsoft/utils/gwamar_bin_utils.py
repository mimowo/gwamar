from src.drsoft.structs.gene_profile import GeneProfile
from src.drsoft.structs.mutation_profile import MutationProfile
from src.drsoft.utils.gwamar_muts_io_utils import decompressProfile, simpleGeneName
from src.drsoft.utils.gwamar_muts_utils import calcMutStatesMutation

def getMutated(bin_profile):
    ret = set([])
    for index in range(len(bin_profile)):
        if not bin_profile[index] in "0?":
            ret.add(index)
    return ret
    
def getNotMutated(bin_profile):
    ret = set([])
    for index in range(len(bin_profile)):
        if bin_profile[index] == '0':
            ret.add(index)
    return ret

def readBinProfiles(input_fh, n=None, gen_bin_profiles=False, decompress=True):
    bin_profiles = {}
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2: continue
        bin_profile_id = tokens[0]
        if decompress: gen_bin_profile = decompressProfile(tokens[1], n)
        else: gen_bin_profile = tokens[1]
        
        if gen_bin_profiles == False:
            bin_profile = ""
            for i in range(n):
                if gen_bin_profile[i] == "0": bin_profile += "0" 
                elif gen_bin_profile[i] == "?": bin_profile +="?"
                else: bin_profile += "1"
            bin_profiles[bin_profile_id] = bin_profile
        else:
            bin_profiles[bin_profile_id] = gen_bin_profile
    return bin_profiles

def readBinProfileCounts(input_fh):
    bin_profiles_count = {}
    
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        bin_profile_id = tokens[0]
        bin_profile_count = int(tokens[1])
        bin_profiles_count[bin_profile_id] = bin_profile_count
    return bin_profiles_count

def binProfilesDetailsToStr(bin_profile_details_list):
    res = ""
    for entry in bin_profile_details_list:
        res += " ".join(map(str, entry)) + ";"
    res = res.strip(";")

    return res

def readBinProfileDetailsPoints(lines, n, out_format="txt", ref_counts=False, mut_counts=True):
    bin_profile_details = {}
    gene_id = ""
    cluster_id = ""
    gene_name = ""
    for line in lines[1:]:
        line = line.strip()
        if line.startswith(">"):
            line = line[1:]
            tokens = line.split("\t")
            gene_id, cluster_id, gene_name = tokens[0], tokens[1], simpleGeneName(tokens[2])
            continue
        tokens = line.split("\t")
        if len(tokens) < 2: continue
        position = int(tokens[0])
        bin_profile_id = tokens[2]
        ref_state = tokens[3]

        # try:
        ref_state_txt, mut_states_txt = calcMutStatesMutation(tokens[4], ref_state, n, ref_counts=ref_counts, mut_counts=mut_counts)
        # except:
        #     print("error: readBinProfileDetailsPoints", tokens)
        #     print("line0: ", lines[0])
        #     print("line1: ", lines[1])
        #     sys.exit()

        entry = gene_id, cluster_id, gene_name, position, ref_state_txt, mut_states_txt

        if out_format == "txt":
            if not bin_profile_id in bin_profile_details:
                bin_profile_details[bin_profile_id] = ""
            if len(bin_profile_details[bin_profile_id]) > 1: bin_profile_details[bin_profile_id] += ";"

            bin_profile_details[bin_profile_id] += " ".join(map(str, entry))
        elif out_format == "tuple":
            if not bin_profile_id in bin_profile_details:
                bin_profile_details[bin_profile_id] = []
            bin_profile_details[bin_profile_id].append(entry)
            
    return bin_profile_details

def readBinProfileDetailsGenes(lines, n, out_format="txt", ref_counts=False, mut_counts=True):
    bin_profile_details = {}
    
    gene_id = ""
    cluster_id = ""
    gene_name = ""

    for line in lines[1:]:
        line = line.strip()
        if line.startswith(">"):
            line = line[1:]
            tokens = line.split("\t")
            gene_id, cluster_id, gene_name = tokens[0], tokens[1], simpleGeneName(tokens[2])
            continue
        tokens = line.split("\t")
        if len(tokens) < 2: continue
        bin_profile_id = tokens[1]
        
        ref_state = tokens[2]
        ref_state_txt, mut_states_txt = calcMutStatesMutation(tokens[3], ref_state, n, ref_counts=ref_counts, mut_counts=mut_counts)
        if out_format == "txt":
            if not bin_profile_id in bin_profile_details:
                bin_profile_details[bin_profile_id] = ""
            if len(bin_profile_details[bin_profile_id]) > 1: bin_profile_details[bin_profile_id] += ";"

            bin_profile_details[bin_profile_id] += " ".join([gene_id, cluster_id, gene_name, ref_state_txt, mut_states_txt])
        elif out_format == "tuple":
            if not bin_profile_id in bin_profile_details:
                bin_profile_details[bin_profile_id] = []
            bin_profile_details[bin_profile_id].append((gene_id, cluster_id, gene_name, 0, ref_state_txt, mut_states_txt))

    return bin_profile_details

def readBinProfileDetails(input_fh, n, mut_type, out_format="txt", ref_counts=False, mut_counts=True):
    lines = input_fh.readlines()
    if mut_type == "P": return readBinProfileDetailsPoints(lines, n, out_format=out_format, ref_counts=ref_counts, mut_counts=mut_counts)
    else: return readBinProfileDetailsGenes(lines, n, out_format=out_format, ref_counts=ref_counts, mut_counts=mut_counts)

def readBinGeneProfilesPoints(lines, only_ref=False):
    gene_profiles = {}
    gene_id = ""
    cluster_id = ""
    gene_name = ""
    gene_profile = None
    for line in lines:
        if line.startswith(">"):
            line = line[1:]
            tokens = line.split()
            gene_id, cluster_id, gene_name = tokens[0], tokens[1], simpleGeneName(tokens[2])
            if len(tokens) >= 6: ref_start, ref_end, ref_strand = int(tokens[3]), int(tokens[4]), tokens[5]
            else: 
                ref_start, ref_end, ref_strand = -1, -1, "?"
                if only_ref:
                    gene_profile = None
                    continue
            gene_profile = GeneProfile(cluster_id, gene_id, gene_name, ref_start, ref_end, ref_strand)
            gene_profile_id = gene_id + "_" + cluster_id
            gene_profiles[gene_profile_id] = gene_profile
            continue
        tokens = line.split()
        if len(tokens) < 2: continue
        if gene_profile == None: continue
        position = int(tokens[0])
        bin_profile_id = tokens[1]
        res_states_txt = tokens[2]
        mut_states_txt = tokens[3]
        mutation = MutationProfile(position)
        mutation.bin_profiles = {}
        mutation.bin_profiles[bin_profile_id] = (res_states_txt, mut_states_txt)
        gene_profile.mutations[position] = mutation
        
    return gene_profiles

def readBinGeneProfilesGenes(lines, only_ref=False):
    gene_profiles = {}
    for line in lines:
        tokens = line.split()
        if len(tokens) < 2: continue
        bin_profile_id = tokens[0]
            
        if len(tokens) == 6:
            gene_id = tokens[1]
            cluster_id = tokens[2]
            gene_name = simpleGeneName(tokens[3])
            res_states_txt = tokens[4]
            mut_states_txt = tokens[5]
            gene_profile = GeneProfile(cluster_id, gene_id, gene_name)
            gene_profile.bin_profiles = {}
            gene_profile.bin_profiles[bin_profile_id] = (res_states_txt, mut_states_txt)
            gene_profile_id = gene_id + "_" + cluster_id
            gene_profiles[gene_profile_id] = gene_profile
        else: continue
    return gene_profiles

def readBinGeneProfiles(input_fh, only_ref=False):
    lines = input_fh.readlines()
    line0 = lines[0]
    if line0.startswith(">"): return readBinGeneProfilesPoints(lines, only_ref=only_ref)
    else: return readBinGeneProfilesGenes(lines, only_ref=only_ref)

def readBinProfilesSorted(input_fh, rev_order=-1):
    bin_profiles = []
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        bin_profile_id = tokens[0]
        bin_profile_score = float(tokens[1])
        bin_profiles.append((bin_profile_id, bin_profile_score))
        
    bin_profiles_sorted = sorted(bin_profiles, key=lambda id_score:id_score[1]*rev_order)
    
    return bin_profiles_sorted


def selectResRelBinProfiles(bin_profile_details, drug_rel_muts, cluster_ids, cluster_ids_rev):
    sel_bin_profiles = {}

    for bin_profile_id in bin_profile_details:
        for (gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt) in bin_profile_details[bin_profile_id]:
            if not cluster_id in drug_rel_muts and not gene_id in drug_rel_muts:
                continue
            ret_ranges = drug_rel_muts.get(cluster_id, drug_rel_muts.get(gene_id, None))
            if ret_ranges == None:
                if not bin_profile_id in sel_bin_profiles:
                    sel_bin_profiles[bin_profile_id] = set([])
                sel_bin_profiles[bin_profile_id].add((gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt))
            else:
                for start, end in ret_ranges:
                    if position >= start and position <= end:
                        if not bin_profile_id in sel_bin_profiles:
                            sel_bin_profiles[bin_profile_id] = set([])
                        sel_bin_profiles[bin_profile_id].add((gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt))                        
    return sel_bin_profiles

def specBinProfiles(gen_bin_profile):
    n = len(gen_bin_profile)
    states_count = 0
    for i in range(n):
        state = gen_bin_profile[i]
        if state != "0" and state != "?": 
            if int(state) > states_count: states_count = int(state)
    if states_count == 1:
        return [gen_bin_profile]
    else:
        bin_profiles = []
        for k in range(1, states_count+1, 1):
            bin_profile = []
            for i in range(n):
                state = gen_bin_profile[i]
                if state == "0" or state == "?": bin_profile.append(state)
                elif int(state) == k: bin_profile.append("1")
                else: bin_profile.append("0")
            bin_profiles.append(bin_profile)
    return bin_profiles

def countNonZeros(gen_bin_profile):
    n = len(gen_bin_profile)
    states_count = 0
    for i in range(n):
        state = gen_bin_profile[i]
        if state != "0" and state != "?": 
            states_count += 1
    return states_count

def computeRankings(bin_profiles_sorted, bin_profiles_counts={}):
    bin_profiles_rankings = {}
    n = len(bin_profiles_sorted)
    if n == 0: return None

    #mean_ranks_i = [0]*n
    min_ranks_i = [0]*n
    max_ranks_i = [0]*n
    #mean_ranks_pos = [0]*n
    min_ranks_pos = [0]*n
    max_ranks_pos = [0]*n
    
    start_i = 0    
    start_pos = 0
    end_pos = 0
    
    for i in range(n):            
        bin_profile_id, bin_profile_score = bin_profiles_sorted[i]
        
        count = bin_profiles_counts.get(bin_profile_id, 1)
        
        if i == n-1 or bin_profile_score != bin_profiles_sorted[i+1][1]:
            min_rank_i = start_i + 1
            max_rank_i = i + 1

            min_rank_pos = start_pos + 1
            max_rank_pos = end_pos + count

            for j in range(start_i, i+1, 1):
                min_ranks_i[j] = min_rank_i
                max_ranks_i[j] = max_rank_i

                min_ranks_pos[j] = min_rank_pos
                max_ranks_pos[j] = max_rank_pos

            start_i = i+1
            start_pos = end_pos + count
            end_pos = end_pos + count
        else:
            end_pos += count
            
    for i in range(n):            
        bin_profile_id, bin_profile_score = bin_profiles_sorted[i]
        bin_profiles_rankings[bin_profile_id] = (min_ranks_pos[i], max_ranks_pos[i], min_ranks_i[i], max_ranks_i[i])
    return bin_profiles_rankings
