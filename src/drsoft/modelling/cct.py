
from decimal import Decimal
from src.drsoft.utils import gwamar_stat_utils, gwamar_generator_utils,\
  gwamar_bin_utils
from src.drsoft.modelling import model_logic

def colorEdges(tree, alg=0, overwrite=False):
    if alg == 0: # color all edges coming to resistant nodes
        for node in list(tree.leaves) + tree.inner_nodes_bu:
            if node.complete_res_state == "R":
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "b"
            else:
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "w"
    elif alg == 1: # color all edges with S->R
        for node in list(tree.leaves) + tree.inner_nodes_bu:
            if node.complete_res_state == "R" and (node.parent == None or node.parent.complete_res_state == "S"):
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "b"
            else:
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "w"
    return None

def calcFastN(tree, w2=False):
    for node in tree.leaves:
        node.maxp = 0
        node.bc = 0
    for node in tree.inner_nodes_bu:
        node.maxp = 0
        node.bc = 0
        for node_j in node.children:
            if node_j.ecolor == "b":
                node.maxp += 1
                node.bc += 1
            else:
                node.maxp += node_j.maxp
    return tree.root.maxp

def calcFastWnMAT(tree, maxrparam=None, opt=False, w2=True):
    for node in tree.leaves:
        node.count_map = {}                             
        node.count_map[0] = 1
        node.maxn = 0

    for node in tree.inner_nodes_bu:
        node.count_map = {}
        k = len(node.children)

        node.maxn = 0
        for node_j in node.children:
            node.maxn += max(node_j.maxn, 1)

        if maxrparam == None: maxrp = node.maxn
        else: maxrp = min(maxrparam, node.maxn)

        for nt in range(maxrp+1):
          #  print("x", node.node_id, nt,k )
            if w2: maxr = min(nt, k-1, node.maxn)
            else: maxr = min(nt, k, node.maxn)
            
            if opt and (k+1 == node.subnodes_count):
                if w2 and k == nt: wnm = 0
                else: wnm = gwamar_stat_utils.binom(k, nt)
            else:
                wnm = 0
            
                for r in range(maxr+1):
                    if w2 == False and r==k: wnm += 1
                    for gain_profile in gwamar_generator_utils.generateSumProfiles(k, r, 1):
                        kz = 0;
                        gain_bound = []
                        for j in range(k): 
                            if gain_profile[j] == 0: 
                                kz += 1
                                gain_bound.append(node.maxn)
                        for gain_subprofile in gwamar_generator_utils.generateSumProfiles(kz, nt-r, nt-r, gain_bound[::-1]):
                            jz = 0
                            wnm_tmp = 1
                            for j in range(k):
                                if gain_profile[j] == 0:
                                    node_j = node.children[j]
                                    wnm_tmp *= node_j.count_map.get(gain_subprofile[jz], 0)
                                    jz += 1
                                    if wnm_tmp == 0:
                                        break    
                            wnm += wnm_tmp
                            # if nt <=2:
                                # print("   sub", node.node_id, r, nt, gain_profile, gain_subprofile, wnm_tmp, wnm)
            # if nt <=2:
            #      print("www", node.node_id, nt, wnm)
            node.count_map[nt] = wnm
    return tree.root.count_map

def calcFastRP(tree):
    p,r=0,0
    for node in tree.inner_nodes_bu:
        for node_j in node.children:
            if node.complete_mut_state == '0' and node_j.complete_mut_state == "1":
                r += 1
                if node_j.ecolor == "b":
                    p += 1
    return r,p

def calcFastBpnMAT(tree, p, maxrparam, opt=False, w2=True):
    for node in tree.leaves:
        node.count_map = {}
        node.count_map[(0,0)] = 1

    t = 0
    for node in tree.inner_nodes_bu:
        t += 1   
        node.count_map = {}   
        children_list = list(node.children)
        k = len(children_list)

        if maxrparam == None: maxrp = node.maxn
        else: maxrp = min(maxrparam, node.maxn)

        for nt in range(maxrp + 1): # liczba wybranych
            maxp = min(node.maxp, nt)
            for pt in range(maxp + 1):     # liczba pokrytych

                if opt and (k+1 == node.subnodes_count):
                    if w2 and k == nt: wnm = 0
                    else: wnm = gwamar_stat_utils.binom(node.maxp, pt) * gwamar_stat_utils.binom(k-node.maxp, nt-pt)
                else:
                    wnm = 0
                    if w2: maxr = min(k-1, node.maxn, nt)
                    else: maxr = min(k, node.maxn, nt)

                    for r in range(maxr + 1):
                        if r==k and pt==node.bc and w2 == False:  wnm += 1

                        for gain_r_profile in gwamar_generator_utils.generateSumProfiles(k, r, 1):
                            ur = 0 # liczba trafionych
                            kz = 0 # liczba 0
                            gain_r_bound = []
                            gain_u_bound = []
                            for j in range(k):
                                node_j = children_list[j]
                                if gain_r_profile[j] == 1 and node_j.ecolor == "b":
                                    ur += 1
                                elif gain_r_profile[j] == 0:
                                    kz += 1
                                    gain_r_bound.append(node_j.maxn)
                                    gain_u_bound.append(node_j.maxp)

                            for gain_r_subprofile in gwamar_generator_utils.generateSumProfiles(kz, nt-r, nt-r, gain_r_bound[::-1]):
                                gain_p_bound = []
                                for jz in range(kz):
                                    gain_p_bound.append(min(gain_u_bound[jz], gain_r_subprofile[jz]))
                                for gain_u_subprofile in gwamar_generator_utils.generateSumProfiles(kz, pt-ur, pt-ur, gain_p_bound[::-1]):
                                    wnm_tmp = 1
                                    jz = 0
                                    for j in range(k):
                                        if gain_r_profile[j] == 0:
                                            node_j = children_list[j]
                                            wnm_tmp *= node_j.count_map.get((gain_u_subprofile[jz], gain_r_subprofile[jz]),0)
                                            if wnm_tmp == 0:
                                                break
                                            jz += 1

                                    wnm += wnm_tmp
                    # if nt <=2:
                    #     print("   sub", node.node_id, node.bc, pt, nt, wnm_tmp, wnm)

#                                    if nt <=2:
#                                        print("   sub", node.node_id, r, pt, nt, gain_r_profile, gain_r_subprofile, gain_u_subprofile, wnm_tmp, wnm)
                # if nt <=2:
                #     print("zzz", node.node_id, node.bc, pt, nt, node.maxp, wnm)
                node.count_map[(pt,nt)] = wnm
    return tree.root.count_map


    
def calcBinProfilesFastCCT(bin_profiles, drp, trees_list, opt=False, w2=True):
    scores = {}
    
    for bin_profile_id, bin_profile in bin_profiles.items():
        scores[bin_profile_id] = []
    
    for tree in trees_list:
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)
        print(tree)
        
        if len(drp.getResistant()) == 0:
            continue
        
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        newtree.setObservedResProfile(drp.full_profile)
        model_logic.calcSubleavesResCount(newtree)
        model_logic.calcSubnodesCount(newtree)
        model_logic.calcDetParsResModel(newtree)
        colorEdges(newtree, alg=1, overwrite=False)
        print(newtree)

        maxr = 0
        
        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)
                model_logic.calcDetParsMutModel(newtree)
                r,p = calcFastRP(newtree)
                maxr = max(maxr,r)
        
        px = calcFastN(newtree)
        print("px", px)
        wn_mat = calcFastWnMAT(newtree, maxr, True, w2=w2)
        bpn_mat = calcFastBpnMAT(newtree, px, maxr, True, w2=w2)

        print("wn", wn_mat)
        print("bnp", bpn_mat)

        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            score = 0.0
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)
                model_logic.calcDetParsMutModel(newtree)
                
                r,p = calcFastRP(newtree)

                print("rp", r, p)
                bs = 0
                for pt in range(p, min(r, px)+1, 1):
                    bs += bpn_mat[(pt, r)]
            
                cct_score = Decimal(bs) / Decimal(wn_mat[r])
                try:
                    score += float(cct_score)
                except:
                    pass
                 
            scores[bin_profile_id].append(score)

    scores_mean = {}
    
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = gwamar_stat_utils.mean(scores[bin_profile_id])
    
    return scores_mean


