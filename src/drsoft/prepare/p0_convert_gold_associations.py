import sys
import os
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils, gwamar_bin_utils, gwamar_ann_utils, gwamar_muts_io_utils

if __name__ == '__main__':
    print("p0_convert_gold_associations.py. Start.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = parameters["D"]
    input_dir = parameters["DATASET_INPUT_DIR"]
    gold_dir = parameters["DATASET_GOLD_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    ranks_dir = parameters["RESULTS_RANKS_DIR"]
    cmp_dir = parameters["RESULTS_CMP_DIR"]

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    m_type = parameters["MT"]
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_res_profiles = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    input_fh = open(results_dir + 'bin_profiles_' + m_type + '_det.txt')
    bin_profile_details = gwamar_bin_utils.readBinProfileDetails(input_fh, mut_type=m_type, out_format="tuple")
    input_fh.close()

    input_fh = open(results_dir + 'bin_profiles_' + m_type + '.txt')
    bin_profiles = gwamar_bin_utils.readBinProfiles(input_fh, strains.count(), gen_bin_profiles=True)
    input_fh.close()
    
    input_fn = input_dir + "/cluster_gene_ref_ids.txt"
    if os.path.exists(input_fn):
        cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
        cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh, gene_ref_ids=True)
        cluster_fh.close()
    else:
        cluster_ids = {} 
        cluster_ids_rev = {}

    input_fn = gold_dir + "/gold_assocs_A_old.txt"
    if not os.path.exists(input_fn):
        print("File does not exist:" + input_fn)
        drug_res_conf_db = {}
    else:
        input_fh = open(input_fn)
        drug_res_db = gwamar_muts_io_utils.loadDrugResRels(input_fh, cluster_ids_rev)
        input_fh.close()
    
    tls = []

    for drug_name in drug_res_db:
        tls.append("%" + drug_name+"\n")
        for gene_id in drug_res_db[drug_name]:
            tls.append(">" + gene_id+"\n")
            muts = drug_res_db[drug_name][gene_id]
            

            for position in sorted(muts):
                ref_aa_list = set([])
                mut_aa_list = set([])
                for (x,y) in muts[position]:
                    if not x in ["-", "X", "*"]:
                        ref_aa_list.add(x)
                    mut_aa_list.add(y)
                ref_aa_txt = ','.join(list(ref_aa_list))
                mut_aa_txt = ','.join(list(mut_aa_list))
                if len(ref_aa_txt) == 1:
                    tls.append(str(position) + "\t" + ref_aa_txt + "\t" + mut_aa_txt+"\n")
        tls.append("\n")


    output_fn = gold_dir + "/gold_assocs_A.txt"
    output_fh = open(output_fn, "w")
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()

    print("p0_convert_gold_associations.py. Finished.")
