import sys
from src.drsoft.utils import gwamar_params_utils
from src.drsoft.format_input import generate_geo_trees

def loadGeoData(input_fh):
    geo_data_map = {}

    lines = input_fh.readlines()
    for line in lines:
        print(line)
        line = line.strip()
        if line.startswith("#") or len(line) < 2:
            continue
        tokens = line.split("\t")

        if len(tokens) < 1: continue
        elif len(tokens) in [1,2]:
            country = "Unknown"
            continent = "Unknown"
        else:
            strain_id = tokens[1]
            country = tokens[1]
            continuent = tokens[2]
            geo_data_map[strain_id]= (country, continent)
    return geo_data_map
    
if __name__ == '__main__':
    dataset = "mtu"
    tree_ids= ["t1"]

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    gwamar_dir = parameters["GWAMAR_PATH"]
    input_dir = parameters["RES_DATA"] + dataset + "/"

    input_fh = open(input_dir + "/res-data.txt")
    geo_data = loadGeoData(input_fh)
    input_fh.close()

    for tree_id in tree_ids:
        output_fh = open(input_dir + "/geo-tree-"+tree_id+".dot", "w")
        generate_geo_trees.generateGeoTrees(output_fh)
        output_fh.close()
