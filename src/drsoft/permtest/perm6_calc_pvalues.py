import sys
import os
from multiprocessing import Pool
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_scoring, gwamar_utils,\
  gwamar_res_io_utils, gwamar_progress_utils

def findScore(score_tmp, scores_sorted, rev_order):    
    lo, hi = 0, len(scores_sorted) - 1
    while lo <= hi:
        mid = int((lo + hi) / 2)
        if rev_order == -1:
            if scores_sorted[mid] < score_tmp: lo = mid + 1
            elif score_tmp < scores_sorted[mid]: hi = mid - 1
            else: return scores_sorted[mid]
        else:
            if scores_sorted[mid] > score_tmp: lo = mid + 1
            elif score_tmp > scores_sorted[mid]: hi = mid - 1
            else: return scores_sorted[mid]
    return scores_sorted[max(0, lo-1)]

const = 100000.0

def scoreDrug(params):
    score = params["SCORE"]
    drug_name = params["DRUG_NAME"]
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    scores_dir = parameters["RESULTS_SCORES_DIR"]
    perm_combs_dir = parameters["RESULTS_PERM_COMBS_DIR"]
    perm_pvalues_dir = parameters["RESULTS_PERM_PVALUES_DIR"]

    rev_order = gwamar_scoring.revOrder(score)

    score_counts = {}
    
    input_fn = perm_combs_dir + drug_name + "/" + score + ".txt"
    if not os.path.exists(input_fn): 
        return drug_name, score
    
    input_fh = open(perm_combs_dir + drug_name + "/" + score +  ".txt")
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 3: 
        return drug_name, score
    
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 2: continue
        if tokens[0].count(".") > 0: score_float = float(tokens[0])
        else: score_float = float(int(tokens[0]) / const)
            
        score_counts[score_float] = int(tokens[1])
    input_fh.close()

    scores_sorted_rev = sorted(score_counts, key=lambda score_tmp:score_tmp*rev_order)
    scores_sorted = sorted(score_counts, key=lambda score_tmp:-score_tmp*rev_order)

    score_counts_cum = {}
    tot_count = 0
    for score_tmp in scores_sorted_rev:
        count = score_counts[score_tmp]
        tot_count += count
        score_counts_cum[score_tmp] = tot_count
    
    bin_profile_pvalue = {}
    
    input_fn = scores_dir + drug_name + "/" + score + ".txt"
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 3: 
        return drug_name, score
    
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 2: continue
        profile_id = tokens[0]
        score_tmp = float(tokens[1])
        if score_tmp in score_counts_cum:
            score_adj = score_tmp
        else:
            score_adj = findScore(score_tmp, scores_sorted, rev_order)

        bin_profile_pvalue[profile_id] = float(score_counts_cum[score_adj]) / float(tot_count) 
    input_fh.close()
    
    m = len(bin_profile_pvalue)
    
    bin_profile_pvalue_sorted = sorted(bin_profile_pvalue.keys(), key=lambda profile_id:bin_profile_pvalue[profile_id])
    
    output_fh = open(perm_pvalues_dir + drug_name + "/" + score +  ".txt", "w") 
    for profile_id in bin_profile_pvalue_sorted:
        pvalue = bin_profile_pvalue[profile_id]
        output_fh.write(str(profile_id) + "\t" + str(pvalue) +"\t" + str("%.8f" % min(1, (pvalue * m)))+ "\n")
    output_fh.close()

    return drug_name, score
    
if __name__ == '__main__':
    print("perm6_calc_pvalues.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    perm_combs_dir = parameters["RESULTS_PERM_COMBS_DIR"]
    perm_pvalues_dir = parameters["RESULTS_PERM_PVALUES_DIR"]

    gwamar_utils.ensure_dir(perm_pvalues_dir)
    
    score_params = parameters["SCORES"].split(",")
    #score_params += ["mtmow", "tmow", "tmo", "tmw", "tow", "mow"]
    #score_params += ["tm", "to", "tw", "mo", "mw", "ow"]
    #score_params += ["mtw", "mtow", "rand_tmow"]

    TASKS = []
    TASK = {}
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(perm_pvalues_dir + drug_name)
        for score_param in score_params:

            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score_param
            
            TASKS.append(TASK.copy())
        
    progress = gwamar_progress_utils.GWAMARProgress("Computed pvalues")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)   
        for r in pool.imap(scoreDrug, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreDrug(T)
            progress.update(str(r))

    print("perm6_calc_pvalues.py. Finished")
