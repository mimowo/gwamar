import sys
import os
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_utils,\
  gwamar_strains_io_utils, gwamar_res_io_utils, gwamar_res_utils,\
  gwamar_ann_utils, gwamar_muts_io_utils, gwamar_progress_utils,\
  gwamar_muts_utils

if __name__ == '__main__':
  print("p2_convert_profiles_genes.py: converts gene gain/loss profiles. Start.")
  gwamar_params_utils.overwriteParameters(sys.argv)
  parameters = gwamar_params_utils.readParameters()
  
  WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
  
  input_dir = parameters["DATASET_INPUT_DIR"]
  inter_dir = output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"G/"
  gwamar_utils.ensure_dir(inter_dir)

  input_fh = open(input_dir + "/strains_ordered.txt")
  strains = gwamar_strains_io_utils.readStrains(input_fh)
  input_fh.close()
  
  input_fh = open(input_dir + "/res_profiles.txt")
  res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
  input_fh.close()
  
  sc_strains = gwamar_res_utils.findScStrains(res_profiles_list)
  
  res_profiles = {}
  for res_profile in res_profiles_list:
    res_profiles[res_profile.drug_name] = res_profile
  
  if os.path.exists(input_dir + "/cluster_gene_ref_ids.txt"):
    cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
    cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh)
    cluster_fh.close()
  else:
    cluster_ids = {} 
    cluster_ids_rev = {}

  input_fn = input_dir + '/gene_profiles.txt'
  if not os.path.exists(input_fn):
    exit()
  input_fh = open(input_fn)
  gene_profiles = gwamar_muts_io_utils.readGeneGainLossProfiles(input_fh, cluster_ids=cluster_ids, cluster_ids_rev=cluster_ids_rev, gene_subset=None, m_type="a")
  input_fh.close()
  
  bin_profiles = {}
  bin_profiles_det = {}
  bin_profiles_count = {}
  bin_profile_id = 0
  
  bin_mutations = set([])
  
  progress = gwamar_progress_utils.GWAMARProgress("Gene profiles processed")
  progress.setJobsCount(len(gene_profiles))
  
  for gene_profile in gene_profiles.values():
    progress.update()
    
    ref_state = gwamar_muts_utils.calcRefStateMutation(gene_profile.full_profile, sc_strains)
    if ref_state == None: continue
    
    bin_profile = gwamar_muts_utils.calcBinProfileMutation(gene_profile.full_profile, ref_state, gen_bin_profile=True)
    bin_profile_cmp = gwamar_muts_utils.compressProfile(bin_profile)
    full_profile_cmp = gwamar_muts_utils.compressProfile(gene_profile.full_profile)
    
    gene_profile.cmp_profile = bin_profile_cmp
    gene_profile.full_profile_cmp = full_profile_cmp
    gene_profile.ref_state = ref_state
    
    
    if not bin_profile_cmp in bin_profiles:
      bin_profiles[bin_profile_cmp] = bin_profile_id
      bin_profiles_count[bin_profile_id] = 1
      gene_profile.bin_profile_id = bin_profile_id
      
      bin_profile_id += 1
    else:
      bin_profile_id_ex = bin_profiles.get(bin_profile_cmp)
      gene_profile.bin_profile_id = bin_profile_id_ex
      bin_profiles_count[bin_profile_id_ex] += 1

  output_fh = open(inter_dir + '/bin_profiles_G_det.txt', "w")
  gwamar_muts_io_utils.writeMutationsHeader(output_fh, strains.allStrains(), only_strains=True, sep=" ")
  for gene_profile in sorted(gene_profiles.values(), key=lambda gp:(gp.gene_id, gp.cluster_id)):
    if gene_profile.ref_start == -1:
      text_line = ">" + gene_profile.gene_id + "\t" + gene_profile.cluster_id + "\t" + gene_profile.gene_name + "\n"
    else:
      text_line = ">" + gene_profile.gene_id + "\t"
      text_line += gene_profile.cluster_id + "\t"
      text_line += gene_profile.gene_name + "\t"
      text_line += str(gene_profile.ref_start) + "\t"
      text_line += str(gene_profile.ref_end) + "\t"
      text_line += str(gene_profile.ref_strand) + "\n"
    output_fh.write(text_line)
    
    text_line = "g" +"\t" + str(gene_profile.bin_profile_id) + "\t" + gene_profile.ref_state + "\t" + gene_profile.full_profile_cmp + "\n"
    output_fh.write(text_line)
  output_fh.close()

  output_fh = open(inter_dir + '/bin_profiles_G.txt', "w")
  for bin_profile_cmp in sorted(bin_profiles):
    bin_profile_id = bin_profiles[bin_profile_cmp]
    text_line = str(bin_profile_id) +"\t" + bin_profile_cmp
    output_fh.write(text_line + "\n")
  output_fh.close()

  output_fh = open(inter_dir + '/bin_profiles_G_count.txt', "w")
  for bin_profile_id, bin_profile_count in bin_profiles_count.items():
    output_fh.write(str(bin_profile_id) + "\t" + str(bin_profile_count)+ "\n")
  output_fh.close()

  print("p2_convert_profiles_genes.py: Finished.")
  