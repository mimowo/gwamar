import sys
from src.drsoft.utils import gwamar_params_utils



def loadResData(input_fh):
    res_data_map = {}
    citation = ""

    lines = input_fh.readlines()
    for line in lines:
        line = line.strip()
        if line.startswith("#") or len(line) < 2:
            continue
        tokens = line.split("\t")
   #     print(tokens)
        if len(tokens) < 1: continue
        elif len(tokens) <= 2:
            if len(line) < 40:
                print("citation", line)
            citation = tokens[0]
        else:
            drug_name = tokens[0]
            strain_id = tokens[1]
            res_info = tokens[2]

            if not (strain_id, drug_name) in res_data_map:
                res_data_map[(strain_id, drug_name)] = set([])
            res_data_map[(strain_id, drug_name)].add((res_info, citation))
            
    #print("xxx", res_data_map)
    return res_data_map
        
def drugsList(res_data):
    drugs_map = {}
    for (strain_id, drug_name) in res_data:
        if not drug_name in drugs_map:
            drugs_map[drug_name] = 0
        drugs_map[drug_name] += 1
    drugs_list = sorted(drugs_map, key=lambda drug_name:-drugs_map[drug_name])
    return drugs_list

def strainsList(res_data):
    strains_set = set([])
    for (strain_id, drug_name) in res_data:
        strains_set.add(strain_id)
    strains_list = sorted(strains_set)
    return strains_list

def determineResistance(res_data_infos):
    res_count = 0
    susc_count = 0
    int_count = 0
    unk_count = 0
    for (res_info, cite_info) in res_data_infos:
        tokens = res_info.split(":")
        if tokens[0] == "R":
            res_count += 1 
        elif tokens[0] == "S":
            susc_count += 1
        elif tokens[0] == "I":
            int_count += 1
        elif tokens[0] == "U":
            unk_count += 1
    if res_count > 0 and susc_count == 0:
        return "R"
    elif res_count == 0 and susc_count > 0:
        return "S"
    elif res_count > 0 and susc_count > 0:
        return "?"
    elif int_count >0:
        return "I"
    elif unk_count > 0:
        return "?"
    else:
        return "?"
    return "?"


def generateResData(output_fh, res_data):
    tls = []

    drugs_list = drugsList(res_data)
    strains_list = strainsList(res_data)

    k = len(drugs_list)
    n = len(strains_list)

    for drug_name in drugs_list:
        tl = drug_name + "\t"

        strains_res = []
        strains_int = []
        strains_susc = []

        for strain_id in strains_list:
            res_data_infos = res_data.get((strain_id, drug_name), [])
            res_status = determineResistance(res_data_infos)
            if res_status == "R": strains_res.append(strain_id)
            elif res_status == "I": strains_int.append(strain_id)
            elif res_status == "S": strains_susc.append(strain_id)

        for strain_id in sorted(strains_res): tl += strain_id + ";"
        if len(strains_res) > 0: tl = tl[:-1] + "\t"
        else: tl = tl + "\t"

        for strain_id in sorted(strains_susc): tl += strain_id + ";"
        if len(strains_susc) > 0: tl = tl[:-1] + "\t"
        else: tl = tl + "\t"

        for strain_id in sorted(strains_int): tl += strain_id + ";"
        if len(strains_int) > 0: tl = tl[:-1]
        else: tl = tl

        if len(strains_res) > 5 and len(strains_susc) > 5:
            tls.append(tl)

    for tl in tls:
        output_fh.write(tl + "\n")
    return None

if __name__ == '__main__':
    dataset = "spn"

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    gwadrib_dir = parameters["GWADRIB_PATH"]
    input_dir = gwadrib_dir + "/res_data/" + dataset + "/"

    print("xxx")
    input_fh = open(input_dir + "/res-data.txt")
    res_data = loadResData(input_fh)
    input_fh.close()
    print("yyy")

    output_fh = open(input_dir + "/res_profiles_ret.txt", "w")
    generateResData(output_fh, res_data)
    output_fh.close()
    print("zzz")
