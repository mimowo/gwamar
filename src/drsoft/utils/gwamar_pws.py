import time

from src.drsoft.utils import gwamar_stat_utils

def g(n, s, lb=None, ub=None):
    if n == 0:
        pass
    elif n == 1:
        if lb == None and ub == None: yield (s,)
        elif lb == None:
            if s <= ub[0]: yield (s,)
        elif ub == None:
            if s >= lb[0]: yield (s,)
        else:
            if s >= lb[0] and s<=ub[0]: yield (s,)
    else:
        if lb == None and ub == None:
            for i in range(s+1):
                for j in g(n-1, s-i):     
                    yield (i,) + j
        elif lb == None:
            for i in range(min(s, ub[0]) + 1):
                for j in g(n-1, s-i, ub=ub[1:]):     
                    yield (i,) + j
        elif ub == None:
            for i in range(lb[0], s+1, 1):
                for j in g(n-1, s-i, lb=lb[1:]):     
                    yield (i,) + j
        else:
            for i in range(lb[0], min(s,ub[0])+1, 1):
                for j in g(n-1, s-i, lb[1:], ub[1:]):     
                    yield (i,) + j

def generateCombs(k, s, lb=None, ub=None):
    try:
        return list(g(k, s, lb, ub))
    except:
        print("error", k, s, lb, ub, ub[::-1])
        return []

def wsupportTestMap(w_map, debug=False):
    cf = 2
    ret = {}

    w_map_new = {}

    for w in w_map:
        nw = round(w*cf) / cf
        if w > 0 and nw == 0.0: nw = 1.0/cf
        if not nw in w_map_new:
            w_map_new[nw] = 0
        w_map_new[nw] += w_map[w]

    counts = list(w_map_new.values())
    weights = list(w_map_new.keys())

    k = len(w_map_new)
    N = sum(counts)

    for n in range(N+1):
        #print("comp", n)
        #sys.stdout.flush()
        ret[n] = {}
        combs = generateCombs(k, n, ub = counts)
        for comb in combs:
            ws = 0
            count = 1
            for i in range(k):
                ws += comb[i]*weights[i]
                count *= gwamar_stat_utils.binom(counts[i], comb[i])
            if not ws in ret[n]:
                ret[n][ws] = 0
            ret[n][ws] += count
    return ret
