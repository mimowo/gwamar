import sys
import os
from multiprocessing import Pool
from src.drsoft.utils import gwamar_params_utils, gwamar_comp_utils,\
  gwamar_stat_utils, gwamar_utils, gwamar_res_io_utils, gwamar_progress_utils

def readDrugResAssoc(input_fh, drug_name = ""):
    drug_res_assoc = {}
    for line in input_fh.readlines():
        if line.startswith("#"): continue

        tokens = line.strip().split()
        if len(tokens) < 5: continue

        assoc = tokens[0]
        dn = tokens[1]
        if dn != drug_name: continue
        gene = tokens[2]
        position = tokens[4]
        drug_res_assoc[(gene,position)] = assoc
    return drug_res_assoc

def saveScoresCombined(params):
    drug_name = params["DRUG_NAME"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    input_dir = parameters["DATASET_INPUT_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]

    input_fn = input_dir + "/drug_res_muts.txt"
    if not os.path.exists(input_fn):
        exit()
        
    input_fh = open(input_fn)
    drug_res_assoc = readDrugResAssoc(input_fh, drug_name=drug_name)
    input_fh.close()

    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    line0 = lines[0]
    dr_profile = line0.strip().split("\t")[0]
    
    all_set = gwamar_comp_utils.getResSet(dr_profile, inc_q=True, inc_susc=True)
    dr_set = gwamar_comp_utils.getResSet(dr_profile, inc_q=False, inc_susc=False)
    drq_set = gwamar_comp_utils.getResSet(dr_profile, inc_q=True, inc_susc=False)
    
    N = len(all_set)
    mutation_sets_list = [drq_set]
    mutation_desc_list = ["res"]
    
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_desc_tokens = tokens[-1].split(";")[0].split()[2:]
        wscore = float(tokens[-2])
        if wscore < 0: break
        mut_desc = ""
        for tmp in mut_desc_tokens: mut_desc += tmp + " "
        mut_desc = mut_desc[:-1]

        mut_set = gwamar_comp_utils.getBinSet(bin_profile)
        if len(mut_set) <= 0: 
            break
        mutation_sets_list.append(mut_set)
        mutation_desc_list.append(mut_desc)
    input_fh.close()
    
    n = len(mutation_sets_list)
    
    tls = []
    tls.append("digraph G {\n");
    #tls.append("concentrate=true\n")

    for i in range(n):
        label = mutation_desc_list[i]
        label_toks = label.split()
        if len(label_toks) < 2: color = "black"
        else: 
            gene, position = label_toks[:2]
            if not (gene,position) in drug_res_assoc:
                color = "black"
            elif drug_res_assoc[(gene,position)] == "R":
                color = "red"
            elif drug_res_assoc[(gene,position)] == "C":
                color = "blue"
            else:
                color = "black"
        tls.append("\t" +str(i) +' [label="'+  label +"("+str(len(mutation_sets_list[i]))+')",color='+color+'];' + "\n")
    
    for i in range(1, n, 1):
        set1 = mutation_sets_list[i]
        opt_j = 0
        opt_set = dr_set
        opt_pvalue = 1.0

        for j in range(n):
            if i == j: continue
            set2 = mutation_sets_list[j]
            inter = set1 & set2
            jk = len(inter)
            pvalue = gwamar_stat_utils.hyperTestPvalue(N, len(set1), len(set2), len(inter), twoends=False)
            if pvalue < opt_pvalue:
                opt_set = set2
                opt_j = j
                opt_pvalue = pvalue

            pvalue_two = gwamar_stat_utils.hyperTestPvalue(N, len(set1), len(set2), len(inter), twoends=True)
            if pvalue>pvalue_two and pvalue_two<0.01:
                if not set1.issubset(set2) and not set2.issubset(set1):
                    if i<j:
                        label = str("%.6f" % pvalue_two)
                        tls.append('\t' + str(i) + " -> " + str(j) + ' [label="'+str(label)+'",color=red,dir=none];'+'\n')
        if opt_pvalue < 0.01:
            label = str("%.6f" % opt_pvalue)
            if set1.issubset(opt_set):
                tls.append('\t' + str(i) + "->" + str(opt_j) + ' [label="'+label+'",color=blue];'+'\n')
            else:
                tls.append('\t' + str(i) + "->" + str(opt_j) + ' [label="'+label+'",color=black];'+'\n')
    tls.append("}\n")

    output_fh = open(analysis_comp_dir + drug_name + "/graph_pvalues.dot", "w")
    for tl in tls:
        output_fh.write(tl);
    output_fh.close()

    input_fn = os.path.relpath(analysis_comp_dir + drug_name + "/graph_pvalues.dot")
    output_fn = os.path.relpath(analysis_comp_dir + drug_name + "/graph_pvalues.pdf")

    os.system("dot -Tpdf "+input_fn + " -o " + output_fn)

    return drug_name
    
if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    gwamar_utils.ensure_dir(analysis_dir)
    gwamar_utils.ensure_dir(analysis_comb_dir)
    gwamar_utils.ensure_dir(analysis_comp_dir)

    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        TASK["DRUG_NAME"] = drug_name
        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Combined")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveScoresCombined, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveScoresCombined(T)
            progress.update(str(r))

