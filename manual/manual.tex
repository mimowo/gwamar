\documentclass[py@paper=a4paper, ptsize=8pt]{howto}
%\documentclass{article}
%\usepackage[T1]{fontenc}
%\usepackage{polski}
\let\ifpdf\relax
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{fixltx2e}
\usepackage{float}
\usepackage{fancyvrb}
\usepackage[usenames,dvipsnames]{xcolor}
%\usepackage{hyperref}
\usepackage{url}
\usepackage{xspace}

\newcommand{\gwamarhttp}{\small\url{http://bioputer.mimuw.edu.pl/gwamar}\normalsize}
\newcommand{\linkrepo}{\small\url{https://bitbucket.org/mimowo/gwamar}\normalsize}
\newcommand{\mails}{\small\url{m.wozniak@mimuw.edu.pl}\normalsize}
\newcommand{\cmd}[1]{\fcolorbox{white}{GreenYellow}{{\tt #1}}}
\newcommand{\mtuPub}{mtu173\xspace}
\newcommand{\mtuBroad}{mtu\_broad\xspace}
\newcommand{\mtu}{{\it M. tuberculosis}\xspace}
%\def\Verbatim@font{\normalfont\ttfamily}

%\newenvironment{example}{MyVerbatim}}{\end{MyVerbatim}}

\DefineVerbatimEnvironment%
{MyVerbatim}{Verbatim}
{numbersep=1mm,
frame=lines,framerule=0.3mm, rulecolor=\color{GreenYellow},
fontsize=\footnotesize}

\date{27 March 2015}
\release{1.9}

\title{GWAMAR user's manual}
\author{Michal Wozniak}
\begin{document}

\maketitle
\tableofcontents

\section{About the software}

\subsection{Background}

Software to identify drug resistance-associated mutations in bacterial
strains (published: BMC Genomics, 2014). 

\subsection{Availability}

This is the project website:

\gwamarhttp

This is the project repository website:

\linkrepo

Please don't hesitate to contact us with any comments and suggestion or if you 
are interested in co-developing this software.

\subsection{About the authors}

This software was implemented by Michal Wozniak. All authors contributed to 
design of the method and analysis of results. Project idea and guidance came 
from prof. Limsoon Wong (National University of Singapore) and prof. Jerzy 
Tiuryn (University of Warsaw).

Corresponding author: Michal Wozniak (\mails)

\subsection{Installation}
This software is written in Python. Python 2 or 3 is required to run GWAMAR. 
The software does not need any classical installation, you only need to 
download and extract the zip package (it works under Windows, Linux and Mac 
OS) from the project website:

\gwamarhttp

\subsection{Design of GWAMAR}

The software is designed as a set of executable scripts written in Python 
which can be run via the console script {\tt gwamar.py}. 

Figure \ref{fig:structure} presents the hierarchy of folders in GWAMAR open
in the Sublime text editor.

\begin{figure}[h!]
\begin{center}
\includegraphics[height=8cm]{figures/screen.png}
\caption{Hierarchy of folders in GWAMAR open in Sublime.}
\label{fig:structure}
\end{center}
\end{figure}

The hierarchy of folders in GWAMAR (including the dataset folders): 

\begin{itemize}
  \item {\tt gwamar/} --- the main GWAMAR folder with the executable files
  \begin{itemize}
   \item {\tt gwamar.py} --- the main console script to run GWAMAR
   \item {\tt config/} --- configuration files:
   \begin{itemize}
    \item {\tt config\_params.txt} --- configuration of the GWAMAR parameters
   \end{itemize}
   \item {\tt datasets/} --- input datasets
   \begin{itemize}
     \item {\tt dataset1/} --- the folder with {\tt dataset1} input files (for 
     example {\tt dataset1=\mtuPub{}})
  	 \item {\tt dataset2/} --- the folder with {\tt dataset2} input files (for 
     example {\tt dataset2=\mtuBroad{}})
     \item {\tt dataset3/} --- the folder with {\tt dataset3} input files (for 
     example {\tt dataset3=sau461})
   \end{itemize}
   \item {\tt src/}
   \begin{itemize}
    \item {\tt drsoft} --- source code of GWAMAR
    \item {\tt drsoft/analysis} --- scripts to analyze results of 
    post-processing 
    \item {\tt drsoft/comparison} --- scripts to analyze comparison of 
    different association scores based on gold standard
    \item {\tt drsoft/compensatory} --- scripts for visualization of 
    compensatory mutations
    \item {\tt drsoft/formatting} --- scripts for visualization of drug 
    resistance data
    \item {\tt drsoft/permtest} --- scripts for computation of p-value scores
    \item {\tt drsoft/prepare} --- scripts for preparing input for the 
    experiments
    \item {\tt drsoft/scoring} --- scripts running computations of different 
    scores
    \item {\tt drsoft/structs} --- structures used in GWAMAR
    \item {\tt drsoft/utils} --- implementation of common methods used in 
    GWAMAR
    \item {\tt prebroad} --- preprocessing of Broad Institute data
    \item {\tt visR} --- R tools for visualization of results
   \end{itemize}
  \end{itemize}
\end{itemize} 

\subsection{Parameters}

There are two methods to specify parameters in GWAMAR:
\begin{itemize}
  \item -key\textvisiblespace value
  \item key=value
\end{itemize}

The two following commands (to compute association scores) are equivalent:

\begin{itemize}
\item \cmd{python gwamar.py -a s -w 4} 
\item \cmd{python gwamar.py a=s w=4} 
\end{itemize}

Table \ref{tab:parameters} presents the list of GWAMAR available parameters
and their short descriptions.

\begin{table}
\centerline{
\begin{tabular}{l l l p{6cm}}
\toprule
Parameter & Default & Example values & Description \\ 
\midrule
a & -- & p,s,a,cmp & action \\
w & 1 & 1,2,3,\ldots & \# of threads (workers used) \\
d & \mtuBroad{} & \mtuPub,\mtuBroad & dataset \\
s & lh,ws,mi,or,r-tgh & lh,ws,mi,or,r-tgh & scores to compute \\
s & mi+or+lh,mi+or+lh+ws+r-tgh &  & combined scores to compute \\
\bottomrule
\end{tabular}
}
\caption{List of GWAMAR available parameters and their short descriptions.}
\label{tab:parameters}
\end{table}

Table \ref{tab:actions} presents the list of available actions, set by the
{\tt -a} parameter.

\begin{table}[H]
\centerline{
\begin{tabular}{l l}
\toprule
Action & Description \\ 
\midrule
pb & preprocess the Broad Institute dataset \\
p & prepare data for further analysis \\
s & run scoring \\
a & output list of top-scoring mutations \\
cmp & run comparison of different scores \\
cmp1 & save association lists for comparison \\
cmp2 & perform sampling of negatives and AUC calculations \\
cmpF & generate the AUC barplots for different methods \\
\bottomrule
\end{tabular}
}
\caption{List of GWAMAR available options for the {\tt -a} parameter.}
\label{tab:actions}
\end{table}

\subsection{Input data}

Input files for GWAMAR, for a set of bacterial strains, consists of a set of 
mutation profiles and drug resistance profiles to associate. This data is 
specified in the following files:

\begin{itemize} 
\item {\tt dataset/input/strains_ordered.txt} - list of strains,
\item {\tt dataset/input/tree.txt} - phylogenetic tree of strains. Optional 
and necessary only for calculations of tree-aware statistics like: weighted 
support and TGH. It does not need to be provided to calculate odds ratio, 
mutual information or the standard hypergeometric test,
\item {\tt dataset/input/tree_bin.txt} - binary phylogenetic tree of strains. 
Optional, but useful to speed up computations of the TGH scores.
\item {\tt dataset/input/res_profiles.txt} - list of available drug resistance
profiles,
\item {\tt dataset/input/point_mutations.txt} - list of point mutations,
\item {\tt dataset/input/gene_profiles.txt} - list of gene gain/loss profiles,
\item {\tt dataset/gold/gold_assocs_A.txt} - list of mutations associated with 
drug resistance which is used as for gold standard. For \mtu we retrieved this 
list from the TBDReamDB database (taking all the mutations in the database 
into account).
\item {\tt dataset/gold/gold_assocs_H.txt} - list of mutations associated with 
drug resistance which is used as for gold standard. For \mtu we retrieved this 
list from the TBDReamDB database (taking only high confidence mutations in the 
database into account).
\end{itemize}

In the following subsections we describe the input formats and provide small
examples. Larger input datasets are integrated with the sources.

\subsubsection{List of strains}

File {\it dataset/input/strains_ordered.txt} provides the list of 
strains in the same order as the strains appear in the phylogenetic tree.

Example:
\begin{MyVerbatim}
s1
s2
s3
s4
\end{MyVerbatim}

\subsubsection{Phylogenetic tree}

File {\it dataset/input/tree.txt} should provide the phylogenetic tree 
for the set of considered strains in the Nawick format.

Example:
\begin{MyVerbatim}
((s1,s2),(s3.s4));
\end{MyVerbatim}

\subsubsection{Mutation profiles}

First line of the file {\tt dataset/input/point\_mutations.txt} contains an 
order list of strains. Positions of the strains on the list are used as 
the strain identifiers. All mutations are grouped by genes. Each block of 
mutations, for a given gene starts with a line in the following format:
\begin{MyVerbatim}
>gene_id gene_cluster_id gene_name
\end{MyVerbatim}

After that line there is a series of lines which provide information on the set
of mutations within the gene. Each of these lines has the following format:

\begin{MyVerbatim}
position p/n mutation_profile
\end{MyVerbatim}

Here $p$ and $n$ stand for promoter (position $<$ 0) and amino acid, 
respectively.

Example:
\begin{MyVerbatim}
s1 s2 s3 s4
>Rv1484  1  inhA
-40 p TTCC
194 n I?TT
>Rv0667  2  rpoB
450 n SLLS
\end{MyVerbatim}

\subsubsection{Drug resistance profiles}

First line of this file contains an order list of strains. Positions of the 
strains on the list are used as the strain identifiers. In the letter lines 
drug resistance profiles are provided in the following format:

\begin{MyVerbatim}
name_of_drug_resistance_profile drug_resistance_profile
\end{MyVerbatim}

Example:
\begin{MyVerbatim}
s1 s2 s3 s4
drp1   RS?S
drp2   SRS?
\end{MyVerbatim}

\subsubsection{Gold standard associations}

File {\it dataset/gold/gold\_assocs\_H.txt} provides the list of gold 
standard associations which are used for assessment of the accuracy of 
different association methods. These associations are listed in the following 
format:

\begin{MyVerbatim}
drp1 gene_name gene_id position reference_amino_acid mutated_amino_acid 
\end{MyVerbatim}

Example:
\begin{MyVerbatim}
drp1 rpoB Rv0667 450 S L
\end{MyVerbatim}

\subsection{173 strains of M. tuberculosis}

In order to preprocess the data we used eCAMBer. Dataset {\it \mtuPub{}}.

Preparation of intermediate files: \\
\cmd{python gwamar.py -a p -w 4 -d \mtuPub{}}

Computation of association scores: \\
\cmd{python gwamar.py -a s -w 4 -d \mtuPub{} -s lh,ws,mi,or,r-tgh}

Generation of list of scored associations: \\
\cmd{python gwamar.py -a a -w 4 -d \mtuPub{} -s lh,ws,mi,or,r-tgh}

Comparison of different association methods: \\
\cmd{python gwamar.py -a cmp -w 4 -d \mtuPub{} -s lh,ws,mi,or,r-tgh}

\subsection{Broad Institute dataset}

In order to download and preprocess the Broad Institute data 
you can use the following command:
\cmd{python gwamar.py -a pb -w 4 -d \mtuBroad{}}

Here, the dataset {\it \mtuBroad{}} is equivalent to the
{\it \mtuBroad{}} dataset from the paper.

Preparation of intermediate files: \\
\cmd{python gwamar.py -a p -w 4 -d \mtuBroad{}}

Computation of the standard association scores: \\
\cmd{python gwamar.py -a s -w 4 -d \mtuBroad{} -s lh,ws,mi,or,r-tgh}

Computation of the combined association scores: \\
\cmd{python gwamar.py -a s -w 4 -d \mtuBroad{} -s mi+or+lh,mi+or+lh+ws+r-tgh}

Generation of list of scored associations: \\
\cmd{python gwamar.py -a a -w 4 -d \mtuBroad{} -s lh,ws,mi,or,r-tgh}

Comparison of different association methods: \\
\cmd{python gwamar.py -a cmp -w 4 -d \mtuBroad{} -s lh,ws,mi,or,r-tgh}

\end{document}
