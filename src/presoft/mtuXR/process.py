input_fh = open("resdata.txt")
lines = input_fh.readlines()
input_fh.close()

tls = []

drugs_map = {"INH": "Isoniazid",
			"RIF":"Rifampicin",
			"AMI":"Amikacin",
			"STR":"Streptomycin",
			"KAN":"Kanamycin",
			"EMB":"Ethambutol",
			"OFL":"Ofloxacin",
			"CAP":"Capreomycin",
			"ETH":"Ethionamide"}


for line in lines:
	tokens = line.strip().split("\t")
	strain_id = tokens[0]

	drugs_res = set(tokens[1].split(","))
	
	for drug in drugs_res:
		if len(drug) > 1:
			tls.append(drugs_map.get(drug, drug)+"\t"+strain_id+"\tR")
	
	if len(tokens) >= 3:
		drugs_susc = set(tokens[2].split(","))
		for drug in drugs_susc:
			if len(drug) > 1:
				tls.append(drugs_map.get(drug, drug)+"\t"+strain_id+"\tS")
		
for tl in tls:
	print(tl)

