import sys
from src.drsoft.utils import gwamar_params_utils, gwamar_comp_utils,\
  gwamar_utils, gwamar_res_io_utils


def retrieveCompMutations(drug_name):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    comp_dir = parameters["COMP_DATA"]

    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    gene_muts, rdr_muts, dr_profile = gwamar_comp_utils.selectPutativeComp(input_fh, only_comp = False)
    input_fh.close()

#    print(gene_muts.keys())
 #   print("xxx", len(rdr_muts))
    rdr_sets = [gwamar_comp_utils.getBinSet(rdr_mut_profile) for (rdr_mut_desc, rdr_mut_profile) in rdr_muts]

  #  print("xxx", len(rdr_sets))
    tls = []

    tls.append("%" + parameters["D"] +"\n")
    for gene_name in sorted(gene_muts):
        #print(gene_name)
        #print(gene_name, len(gene_muts[gene_name]), len(rdr_muts))
        tls.append(">" + gene_name + "\n")
        # for entry in gene_muts[gene_name]:
        #     print(entry[0].split())
        for mut_dets in sorted(gene_muts[gene_name], key=lambda entry:int(entry[0].split()[3])):
            (mut_desc, bin_profile) = mut_dets

            mut_out = "\t".join(mut_desc.split()[3:])
            #print(mut_out)
            tls.append(mut_out + "\n")

    gwamar_utils.ensure_dir(comp_dir + "/" + drug_name)
    output_fn = comp_dir + "/" + drug_name + "/" + parameters["D"] + ".txt"

#    print(output_fn)
    output_fh = open(output_fn, "w")
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()

#507 - 533
#(533-507+1)*3
#450 - 531
#zatem: 426 - 452 

if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    comp_dir = parameters["COMP_DATA"]

    gwamar_utils.ensure_dir(comp_dir)
    print(comp_dir)
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()

    drug_names = ["Rifampicin"]
    
    for drug_name in drug_names:
        retrieveCompMutations(drug_name)
