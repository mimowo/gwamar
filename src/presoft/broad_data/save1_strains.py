import sys
from src.drsoft.utils import gwamar_params_utils

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    data_dir = parameters["GWADRIB_PATH"]
    strain_list = []
    
    input_fn = data_dir + "broad/drug_res_data1.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines()[1:]:
        tokens = line.split()
        if len(tokens) > 0:
            strain_list.append(tokens[0])
    input_fh.close()

    output_fh = open(data_dir + "broad/strains.txt", "w")
    for strain_id in strain_list:
        output_fh.write(strain_id + "\n")
    output_fh.close()
    