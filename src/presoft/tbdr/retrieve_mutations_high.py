import sys
import os
from src.drsoft.utils import gwamar_params_utils, gwamar_utils


if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    text_lines = []
    dataset = "tbdr"
    dataset_dir = os.path.abspath("../camber-pred/datasets/"+dataset)+"/"
    gwamar_utils.ensure_dir(dataset_dir)
    muts_list = []
    
    mut_details = set([])
    genes_map = {}
    genes_set = set([])
    
    input_fn = dataset_dir + "tbdr_proc_high.txt"
    input_fh = open(input_fn)
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        
        if line.startswith("#"): continue
        elif len(tokens) < 1: continue
        elif len(tokens) == 1: drug_name = tokens[0]
        else:
            gene_name = tokens[0]
            if gene_name == "N/A": continue
            mut_desc = tokens[1]
            if mut_desc == "katG deletion": continue
            elif mut_desc.startswith("nt "):
                mut_desc_tokens = mut_desc.split()
                position = str(((int(mut_desc_tokens[1]) - 1) / 3) + 1)
                ref_states = "X"
                mut_states = "*"
                
            elif mut_desc.startswith("-") or gene_name == "rrs":
                mut_desc = mut_desc.replace("/", " ")
                mut_desc = mut_desc.replace(",", " ")
                mut_desc = mut_desc.replace("(", " ")
                mut_desc = mut_desc.replace(")", " ")
                mut_desc_tokens = mut_desc.strip().split()
                
                position = mut_desc_tokens[0]
                ref_states = mut_desc_tokens[1]
                mut_states = mut_desc_tokens[2]
            else:
                index_s = mut_desc.find(" ")
                mut_desc = mut_desc[:index_s]
                
                ref_states = mut_desc[0]
                if mut_desc.endswith("STOP"): 
                    mut_states = "*"
                    position = mut_desc[1:-4]
                else: 
                    mut_states = mut_desc[-1]
                    position = mut_desc[1:-1]
                    
            if gene_name == "rpoB" and int(position) > 0:
                position_int = int(position) - (526-445)
                position = str(position_int)
                    
            if gene_name == "katG": gene_id = "Rv1908c"
            elif gene_name == "mabA, fabG1": gene_name, gene_id = "fabG1-inhA", "Rv1483"
            elif gene_name == "ahpC": gene_id = "Rv2428"
            elif gene_name == "rpoB": gene_id = "Rv0667"
            elif gene_name == "gyrA": gene_id = "Rv0006"
            elif gene_name == "gyrB": gene_id = "Rv0005"
            elif gene_name == "rrs": gene_id = "Rvnr01"
            elif gene_name == "rpsL": gene_id = "Rv0682"
            elif gene_name == "kasA": gene_id = "Rv2245"
            elif gene_name == "rrl": gene_id = "Rvnr02"
            elif gene_name == "embB": gene_id = "Rv3795"
            elif gene_name == "pncA": gene_id = "Rv2043c"
            else: gene_id = "x"
            
            if drug_name == "Para-Aminosalisylic":
                genes_set.add((drug_name, gene_name, gene_id))
                if not drug_name in genes_map: genes_map[drug_name] = set([])
                genes_map[drug_name].add(gene_id)
                mut_details.add(("Para-Aminosalisylic_acid", gene_name, gene_id, int(position), ref_states, mut_states))
            elif drug_name == "Fluoroquinolones":
                for drug_name_tmp in ["Ciprofloxacin", "Ofloxacin", "Moxifloxacin", "Levofloxacin"]:
                    genes_set.add((drug_name_tmp, gene_name, gene_id))
                    if not drug_name in genes_map: genes_map[drug_name] = set([])
                    genes_map[drug_name].add(gene_id)
                    mut_details.add((drug_name_tmp, gene_name, gene_id, int(position), ref_states, mut_states))
            elif drug_name == "Aminoglycosides":
                for drug_name_tmp in ["Amikacin", "Capreomycin", "Kanamycin"]:
                    genes_set.add((drug_name_tmp, gene_name, gene_id))
                    if not drug_name in genes_map: genes_map[drug_name] = set([])
                    genes_map[drug_name].add(gene_id)
                    mut_details.add((drug_name_tmp, gene_name, gene_id, int(position), ref_states, mut_states))
            else:
                genes_set.add((drug_name, gene_name, gene_id))
                if not drug_name in genes_map: genes_map[drug_name] = set([])
                genes_map[drug_name].add(gene_id)
                mut_details.add((drug_name, gene_name, gene_id, int(position), ref_states, mut_states))
    input_fh.close()
    
    mut_details_sorted = sorted(mut_details)
    
    output_fh = open(dataset_dir + "tbdr_muts_high.txt", "w")
    for (drug_name, gene_name, gene_id, position, ref_states, mut_states) in mut_details_sorted:
        text_line = drug_name + "\t" + gene_name + "\t" + gene_id + "\t" + str(position) + "\t" + ref_states + "\t" + mut_states + "\n"
        output_fh.write(text_line)
    output_fh.close()
    
    genes_details_sorted = sorted(genes_set)
    
    output_fh = open(dataset_dir + "tbdr_genes_high.txt", "w")
    for drug_name in genes_map:
        text_line = drug_name + "\t" 
        for gene_id in genes_map[drug_name]:
            text_line += gene_id + ";"
        if text_line.endswith(";"): text_line = text_line[:-1] + "\n"
        output_fh.write(text_line)
    output_fh.close()
