import sys
from src.drsoft.structs import resistance_profile
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils

def readResProfilesFromTable(input_fh):

    lines = input_fh.readlines()
    line0 = lines[0]
    tokens = line0.split()
    drug_names = tokens[1:]
    
    res_profiles_map = {}
    for drug_name_tmp in drug_names:
        drug_name = drug_names_map.get(drug_name_tmp, drug_name_tmp)
        res_profile = resistance_profile.ResistanceProfile(drug_name, strains.count())
        res_profiles_map[drug_name] = res_profile
        
    for line in lines[1:]:
        line = line.strip()
        tokens_tmp = line.split()
        if not len(tokens_tmp) == len(drug_names) + 1:
            tokens_tmp = line.split("\t")
        strain_id = tokens_tmp[0]
        strain_index = strains.getPosition(strain_id)
        res_tokens = tokens_tmp[1:]
 
        for i in range(len(res_tokens)):
            drug_name_tmp = drug_names[i]
            drug_name = drug_names_map.get(drug_name_tmp, drug_name_tmp)
            res_type_tmp = res_tokens[i].upper()
            res_profile = res_profiles_map[drug_name]
            if res_type_tmp == "": res_type_tmp = "?"
            elif res_type_tmp == "U": res_type_tmp = "?"
            elif res_type_tmp == "?": res_type_tmp = "?"
                
            res_profile.full_profile[strain_index] = res_type_tmp

    return res_profiles_map

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    input_id = "2"
    data_dir = parameters["GWADRIB_PATH"]
    
    input_fh = open(data_dir + "broad/strains.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    drug_names_map = {}
    input_fh = open(data_dir + "broad/drug_name_map.txt")
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2: continue
        drug_names_map[tokens[0]] = tokens[1]
    input_fh.close()
    
    
    input_fh = open(data_dir + "broad/drug_res_data1.txt")
    res_profiles1_map = readResProfilesFromTable(input_fh)
    input_fh.close()

    input_fh = open(data_dir + "broad/drug_res_data2.txt")
    res_profiles2_map = readResProfilesFromTable(input_fh)
    input_fh.close()

    print(res_profiles1_map.keys())
    print(res_profiles2_map.keys())

    for drug_name in res_profiles1_map:
        if not drug_name in res_profiles2_map:
            continue
        for i in range(len(res_profiles1_map[drug_name].full_profile)):
            if res_profiles1_map[drug_name].full_profile[i] != res_profiles2_map[drug_name].full_profile[i]:
                tl = drug_name + " " 
                tl += strains.getStrain(i) + " " 
                tl += res_profiles1_map[drug_name].full_profile[i] + " "
                tl += res_profiles2_map[drug_name].full_profile[i] 
                print(tl)

    res_stats = {}
    for drug_name in res_profiles2_map:
        sc = 0
        rc = 0
        ic = 0
        fp = res_profiles2_map[drug_name].full_profile
        for i in range(len(fp)):
            if fp[i] == "S": sc += 1
            elif fp[i] == "R": rc += 1
            elif fp[i] == "I": ic += 1

        res_stats[drug_name] = (sc+rc+ic, sc, rc, ic)
        
    drug_names_sorted = sorted(res_stats.keys(), key=lambda drug_name: -res_stats[drug_name][0])
    output_fh = open(data_dir + "broad/res_stats.txt", "w")
    for drug_name in drug_names_sorted:
        ac, sc, rc, ic = res_stats[drug_name]
        tl = drug_name + "\t" + str(sc) +"\t" + str(rc) + "\t" + str(ic) + "\n"
        output_fh.write(tl)
    output_fh.close()

