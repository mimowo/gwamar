class RelGraphNode:
    def __init__(self, node_id, label=None, ncolor=None, mprofile=None):
        self.node_id = node_id
        self.edges = []
        self.edges_rev = []
        self.parent = None
        self.ncolor = ncolor
        self.mprofile = mprofile
        if label==None: self.label = node_id
        else: self.label = label
    def setLabel(self, label):
        self.label = label
    def addEdge(self, edge):
        self.edges.append(edge)
    def addEdgeRev(self, edge):
        self.edges_rev.append(edge)
    def is_leaf(self):
        return len(self.edges) == 1            

class RelGraphEdge:
    def __init__(self, tnode, label=None, ecolor=None):
        self.tnode = tnode
        self.label = label
        self.ecolor = ecolor
    def setLabel(self, label):
        self.label = label

class RelGraph:
    def __init__(self):
        self.nodes = {}

    def addNode(self, node):
        self.nodes[node.node_id] = node

    def size(self):
        return len(self.nodes)

    def visible_nodes(self, snodes):
        visible = set([])
        queue = list(snodes)
        for node in self.nodes.values():
            node.visited = False

        while len(queue) > 0:
            node = queue.pop()
            print(node.label)
            node.visited = True
            for edge in node.edges_rev:
                if edge.tnode.visited== False:
                    queue.append(edge.tnode)
            visible.add(node)
        return visible

    def component(self, node):
        cc = []
        queue = [node]
        while len(queue) > 0:
            node = queue.pop()
            node.visited = True
            for edge in node.edges:
                if edge.tnode.visited== False:
                    queue.append(edge.tnode)
            cc.append(node)
        return cc

    def connected_components(self):
        ccs = []
        for node in self.nodes.values():
            node.visited = False

        for node in self.nodes.values():
            if node.visited == False:
                ccs.append(self.component(node))
        return ccs
            
    # def __str__(self):
    #     txt = "TREE ROOT ID: " + str(self.root_id) + "\n"
    #     for i in self.nodes:
    #         node_i = self.nodes[i]
    #         parent = node_i.parent
    #         if parent == None: 
    #             txt += str(i) + "\t" + str("ROOT:")+str(node_i.ecolor)+"|"+str("%.5f" % 0.0)+"\t" #+ str(len(node_i.children)) +":"
    #         else:
    #             txt += str(i) + "\t" + str(parent.node_id) + ":"+str(node_i.ecolor)+"|"+str("%.5f" % node_i.e_len)+"\t" #+ str(len(node_i.children)) +":"
    #         for node_j in node_i.children:
    #             txt += str(node_j.node_id) + ","
    #         txt = txt[:-1] + "\n"
             
    #     return txt
            
