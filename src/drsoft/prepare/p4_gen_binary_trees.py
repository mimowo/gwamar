import os
import sys
import random
import copy
import shutil
from collections import deque

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.structs import res_tree
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_tree_io_utils

def generateBinaryTree(n):
  bin_tree = res_tree.ResTreeNode(1, None)
  
  queue = deque()
  queue.append(bin_tree)
  i = 1
  index = 0
  
  while i < n:
    node = queue.popleft()
    
    left = res_tree.ResTreeNode(index, node)
    right = res_tree.ResTreeNode(index+1, node)
    
    node.children = [left, right]
    queue.append(left)
    queue.append(right)
    i += 1
    index += 2
  return bin_tree

def binarizeTreeRandom(tree):
  queue = deque()
  queue.append(tree.root)
  
  while queue:
    node = queue.popleft()
    if len(node.children) == 0:
      pass
    elif len(node.children) == 2:
      for child_node in node.children:
          queue.append(child_node)
    elif len(node.children) == 1:
      print("something wrong,1 ")
    else:
      n = len(node.children)
      tree_tmp = generateBinaryTree(n)
    
      perm = list(range(n))
      random.shuffle(perm)
    
      node_children = copy.deepcopy(node.children)
      node.children = tree_tmp.children
      
      leaf_nodes = tree_tmp.getLeaves()
    
      for new_child in node.children:
        new_child.parent = node
      
      for i in range(n):
        index = perm[i]
        
        child_node = node_children[i]
        leaf_node = leaf_nodes[index]
                
        leaf_node.children = child_node.children
        leaf_node.node_id = child_node.node_id
                     
        for ch_node in leaf_node.children:
          ch_node.parent = leaf_node
        queue.append(leaf_node)
        
  return tree

def randomBinaryTrees(tree, strains, k):
    random_trees_txt = ""
    flat_tree = tree.flatten(0.00001)
    
    for i in range(k):
        flat_tree_tmp = copy.deepcopy(flat_tree)
        
        random_bin_tree = binarizeTreeRandom(flat_tree_tmp)
        random_tree_txt = random_bin_tree.toString(lengths=False, strains=strains)
        
      #  output_fh.write(random_tree_txt+ ";\n")
        random_trees_txt += random_tree_txt+ ";\n"
    return random_trees_txt

if __name__ == '__main__':
    print("p4_gen_binary_trees: binarizes the given tree (randomly). Start.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    tree_ids=["PHYML", "RAXML", "", "PHYLIP"]
    tree_ids.append(parameters.get("TREE_ID", ""))

    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
        
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
        
    input_fh = open(input_dir + "strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    strains_list = strains.allStrains()

    for tree_id in tree_ids:

        input_fn = results_dir + "tree"+tree_id+".txt"
        if not os.path.exists(input_fn):
            continue
        input_fh = open(input_fn)
        tree = gwamar_tree_io_utils.readPhyloTrees(input_fh, strains_list)[0]
        input_fh.close()
      
        random_trees_txt = randomBinaryTrees(tree, strains, 3)

        results_dir = output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"G/"
        output_fn = results_dir + "tree"+tree_id+"_bin.txt"
        input_bin_fn = input_dir + "tree"+tree_id+"_bin.txt"
        if os.path.exists(input_bin_fn):
            shutil.copy2(input_bin_fn, output_fn)
        else:
            output_fh = open(output_fn, "w")
            output_fh.write(random_trees_txt)
            output_fh.close();
        
        results_dir = output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"P/"
        output_fn = results_dir + "tree"+tree_id+"_bin.txt"
        input_bin_fn = input_dir + "tree"+tree_id+"_bin.txt"
        if os.path.exists(input_bin_fn):
            shutil.copy2(input_bin_fn, output_fn)
        else:
            output_fh = open(output_fn, "w")
            output_fh.write(random_trees_txt)
            output_fh.close();

    print("p4_gen_binary_trees: Finished.")
