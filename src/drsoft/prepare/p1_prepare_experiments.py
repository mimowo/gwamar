import sys
import os
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils, gwamar_utils, gwamar_tree_io_utils

if __name__ == '__main__':
  print("p1_prepare_experiments.py: prepares folders to store results. Start. h")
  gwamar_params_utils.overwriteParameters(sys.argv)
  parameters = gwamar_params_utils.readParameters()
  
  tree_ids=["PHYML", "RAXML", "", "PHYLIP"]
  tree_ids.append(parameters.get("TREE_ID", ""))

  WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
  
  input_dir = parameters["DATASET_INPUT_DIR"]

  input_fh = open(input_dir + "/strains_ordered.txt")
  strains = gwamar_strains_io_utils.readStrains(input_fh)
  input_fh.close()
  
  strains_list = strains.allStrains()
  
  input_fh = open(input_dir + "/res_profiles.txt")
  res_profiles = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True, strains_list=strains_list)
  input_fh.close()
  
  output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"P/"
  gwamar_utils.ensure_dir(output_dir)

  output_fh = open(output_dir + "res_profiles.txt", "w")
  gwamar_res_io_utils.saveResistanceProfiles(output_fh, res_profiles, strains_list, compress=True)
  output_fh.close()

  for tree_id in tree_ids:
    output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"P/"
    
    input_fn = input_dir + "tree"+tree_id+".txt"
    if not os.path.exists(input_fn):
        continue

    input_fh = open(input_fn)
    tree = gwamar_tree_io_utils.readPhyloTrees(input_fh, strains_list)
    input_fh.close()

    output_tree_txt = tree[0].toString(lengths=False, strains=strains)
    output_fh = open(output_dir + "/tree"+tree_id+".txt", "w")
    output_fh.write(output_tree_txt+ ";\n")
    output_fh.close()
    
    output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"G/"
    gwamar_utils.ensure_dir(output_dir)    
    
  print("p1_prepare_experiments.py: Finished.")
