#include <stdio.h>
import itertools as it
import traceback

def generateResistanceProfiles(k):
    return list(it.product("SR", repeat=k))


def generateBinMutationProfiles(k):
    return list(it.product("01", repeat=k))

def generateResBinMutationProfiles(k):
    return list(it.product(["0S","0R","1S","1R"], repeat=k))

def f(n, s, m, ub=None):
    if n == 0:
        pass
    elif n == 1:
        if ub == None:
            if s <= m:
                yield (s,)
        else:
            if s <= m and s<=ub[0]:
                yield (s,)
    else:
        if ub == None:
            for i in range(min(s + 1, m + 1)):
                for j in f(n - 1,s - i, m):     
                    yield (i,) + j
        else:
            for i in range(min(s, m, ub[0]) + 1):
                for j in f(n - 1, s - i, m, ub[1:]):     
                    yield (i,) + j

def g(n, s, lb=None, ub=None):
    if n == 0:
        pass
    elif n == 1:
        if lb == None and ub == None: yield (s,)
        elif lb == None:
            if s <= ub[0]: yield (s,)
        elif ub == None:
            if s >= lb[0]: yield (s,)
        else:
            if s >= lb[0] and s<=ub[0]: yield (s,)
    else:
        if lb == None and ub == None:
            for i in range(s+1):
                for j in g(n-1, s-i):     
                    yield (i,) + j
        elif lb == None:
            for i in range(min(s, ub[0]) + 1):
                for j in g(n-1, s-i, ub=ub[1:]):     
                    yield (i,) + j
        elif ub == None:
            for i in range(lb[0], s+1, 1):
                for j in g(n-1, s-i, lb=lb[1:]):     
                    yield (i,) + j
        else:
            for i in range(lb[0], min(s,ub[0])+1, 1):
                for j in g(n-1, s-i, lb[1:], ub[1:]):     
                    yield (i,) + j



def generateSumProfiles(k, s, m, ub=None):
    try:
        return list(f(k, s, m, ub))
    except:
        print("generateSumProfiles error:", k, s, m, ub, ub[::-1])
        return []


def generateSumProfiles2(k, s, lb=None, ub=None):
  #  print("generateSumProfiles2")
    try:
        return list(g(k, s, lb, ub))
    except: 
        traceback.print_exc()
        print("generateSumProfiles2 error: ", k, s, lb, ub, ub[::-1])
        return []
