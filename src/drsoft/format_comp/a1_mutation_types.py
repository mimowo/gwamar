import sys
import multiprocessing
from multiprocessing import Pool
from src.drsoft.utils import gwamar_params_utils, gwamar_comp_utils,\
  gwamar_utils, gwamar_res_io_utils, gwamar_progress_utils

def saveMutsCategorized(params):
    drug_name = params["DRUG_NAME"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    line0 = lines[0]
    dr_profile = line0.strip().split("\t")[0]
    mutation_sets_list = []
    mutation_desc_list = []
    
    #dr_set = set(range(len(dr_profile)))
    dr_set = gwamar_comp_utils.getResSet(dr_profile, inc_q=True)
  #  print("x", drug_name, len(getResSet(dr_profile, inc_q=False)))
   # print("x", drug_name, len(getResSet(dr_profile, inc_q=True)))
    
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_desc = tokens[-1]
        mut_set = gwamar_comp_utils.getBinSet(bin_profile) & dr_set
        mutation_sets_list.append(mut_set)
        mutation_desc_list.append(mut_desc)
        if len(mut_set) <= 0: 
            break
    input_fh.close()
    
    rem_set = set(dr_set)
    n = len(mutation_sets_list)
    print("sel", n)
    
    res_acq_ids = set([0])
    if n == 0:
        return drug_name
    mutation_category = ["comp"]*n
    mutation_category[0] = "orig"
    
    for i in range(n-1):
        max_overlap_len = 0
        max_overlap_j = -1
        for j in range(0, n, 1):
            rem_curr_set = mutation_sets_list[j] & rem_set
            if len(rem_curr_set) > max_overlap_len:
                max_overlap_len = len(rem_curr_set)
                max_overlap_j = j
        rem_set = set(rem_set - mutation_sets_list[max_overlap_j])
        if max_overlap_j == -1:
            break
        res_acq_ids.add(max_overlap_j)
        mutation_category[max_overlap_j] = "orig"
    res_acq_ids_sorted = sorted(res_acq_ids)
    
    not_covered_by_higher = []
    not_covered_by_other = []
    res_covered = []
    best_id = []
    best_id_covered = []
    for i in range(0, n, 1):
        best_id_tmp = -1
        best_id_covered_tmp = -1
        rem_curr_set = set(dr_set) & mutation_sets_list[i]
        for j in range(n):
            if j == i:
                not_covered_by_higher.append(set(rem_curr_set) & dr_set)
            else:
                rem_curr_set = rem_curr_set - mutation_sets_list[j]
                if len(mutation_sets_list[j] & mutation_sets_list[i]) > best_id_covered_tmp:
                    best_id_covered_tmp = len(mutation_sets_list[j] & mutation_sets_list[i])
                    best_id_tmp = j
        if mutation_desc_list[i].count(" 1224 ")> 0:
            print("xxx", mutation_sets_list[i],dr_set )
        res_covered.append(set(mutation_sets_list[i] & dr_set))
        not_covered_by_other.append(set(rem_curr_set & dr_set))
        best_id.append(best_id_tmp)
        best_id_covered.append(best_id_covered_tmp)
        
    print(n, len(res_acq_ids), len(dr_set), len(rem_set), res_acq_ids_sorted)
    k = len(res_acq_ids_sorted)
    
    comp_set_sorted = sorted(set(range(n)) - set(res_acq_ids_sorted))
    m = len(comp_set_sorted)
    print(k, m)
    print(res_acq_ids_sorted)
    print(comp_set_sorted)
    
    for i in range(m):
        set_comp_id = comp_set_sorted[i]
        ratio_max = 0.0
        ratio_max_j = 0
        for j in range(k):
            set_res_id = res_acq_ids_sorted[j]
            #if set_res_id > set_comp_id:
            #    continue
            inter_set = mutation_sets_list[set_res_id] & mutation_sets_list[set_comp_id]
            sum_set = mutation_sets_list[set_res_id] | mutation_sets_list[set_comp_id]
            if len(mutation_sets_list[set_comp_id]) == 0: ratio = 0.0
            else: ratio = float(len(inter_set)) / float(len(mutation_sets_list[set_comp_id]))
            if len(sum_set) ==0: jaccard = 0.0
            else: jaccard = float(len(inter_set))/float(len(sum_set))
            if ratio > ratio_max:
                ratio_max = ratio
                ratio_max_j = set_res_id
                
        print("y", set_comp_id, ratio_max_j, ratio_max, len(mutation_sets_list[set_comp_id]), mutation_desc_list[set_comp_id], mutation_desc_list[ratio_max_j])
    
    output_fh = open(analysis_comp_dir + drug_name  + "/categories.txt", "w")
    tl = "mutation_category"
    tl += "\t"+ "mutation_ranking"
    tl += "\t" + "res_covered"
    tl += "\t" + "res_not_covered_by_higher"
    tl += "\t" + "res_not_covered_by_other"
    tl += "\t" + "most_covered_by_one"
    tl += "\t" + "best_coverer_details"
    tl += "\t" + "mutation_details" + "\n"
    output_fh.write(tl)
    
    for mut_id in range(n):
        tl = mutation_category[mut_id]
        tl += "\t"+ str(mut_id) 
        tl += "\t" + str(len(res_covered[mut_id]))
        tl += "\t" + str(len(not_covered_by_higher[mut_id]))
        tl += "\t" + str(len(not_covered_by_other[mut_id]))
        tl += "\t" + str(best_id_covered[mut_id])
        tl += "\t" + str(best_id[mut_id])
        tl += "\t" + mutation_desc_list[mut_id] + "\n"
        
        output_fh.write(tl)
        
    return drug_name
    
if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    gwamar_utils.ensure_dir(analysis_dir)
    gwamar_utils.ensure_dir(analysis_comb_dir)
    gwamar_utils.ensure_dir(analysis_comp_dir)

    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(analysis_comp_dir + drug_name)
        TASK["DRUG_NAME"] = drug_name

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Categotized")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveMutsCategorized, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveMutsCategorized(T)
            progress.update(str(r))

