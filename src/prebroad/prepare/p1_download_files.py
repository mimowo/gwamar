import sys
import zipfile
import os.path

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_utils

try:
  from urllib.request import urlopen
except ImportError:
  from urllib2 import urlopen

if __name__ == '__main__':
	print("p1_download_files.py. Start.")
	print("Downloading files...")
	gwamar_params_utils.overwriteParameters(sys.argv)
	parameters = gwamar_params_utils.readParameters()
  
	input_dir = parameters["PREBROAD_DIR"]
	input_download_dir = parameters["PREBROAD_DOWNLOAD_DIR"]
	gwamar_utils.ensure_dir(input_dir)
	gwamar_utils.ensure_dir(input_download_dir)

	www = "http://www.broadinstitute.org/annotation/genome/mtb_drug_resistance.1/assets/GTBDR_120516.snps.zip"
	output_fn = input_download_dir + "GTBDR_120516.snps.zip"
	if not os.path.exists(output_fn):
		f = open(output_fn,'wb')
		f.write(urlopen(www).read())
		f.close()
		print("Downloaded file with mutations.")
	else:
		print("File with mutations is already downloaded.")

	zfile = zipfile.ZipFile(output_fn)
	for name in zfile.namelist():
		(dirname, filename) = os.path.split(name)
		fulldir = input_download_dir#+"/"+dirname
		print("Decompressing " + filename + " on " + fulldir)
		if not os.path.exists(fulldir):
			os.makedirs(fulldir)
		zfile.extract(name, fulldir)

	www = "http://www.broadinstitute.org/annotation/genome/mtb_drug_resistance.1/assets/GTBDR_120516.drugresistance.csv"
	output_fn = input_download_dir + "GTBDR_120516.drugresistance.csv"
	if not os.path.exists(output_fn):
		
		f = open(output_fn,'wb')
		f.write(urlopen(www).read())
		f.close()
		print("File with resistance information.")
	else:
		print("File with resistance information is already downloaded.")
	print("p1_download_files.py. Files downloaded.")
