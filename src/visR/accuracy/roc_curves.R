#install.packages(
require(graphics); require(grDevices);

#data_fn = paste(species_id, "_out/r_subset/", drug_name, ".txt", sep = "")

args <- commandArgs(trailingOnly = TRUE)


if (length(args) >=1) {
	dataset = args[1]
} else {
	dataset = "mtu173"
	#dataset = "broad_mtb"
}

if (length(args) >= 2) {
	mut_type = args[2]
} else {
	mut_type = "P"
}

if (length(args) >= 3) {
	subset = args[3]
} else {
	subset = "eq1"
}

if (length(args) >= 4) {
	prefix = args[4]
} else {
	if (Sys.info()["sysname"] == "Linux") {
		prefix = "/home/misias/Dropbox/gwamar/"
	} else {
		prefix = "D:/Dropbox/Dropbox/gwamar/"
	}
}

if (length(args) >= 5) {
	scores_arg = args[5]
} else {
	scores_arg = "mi,or,r-tgh,ws,lh"
}


## myScript.R

inputdir = paste(prefix,"datasets/",dataset,"/ex",mut_type,"/comparison/stats_", subset, "/", sep="")

score_files = list.files(path = inputdir)

score_areas = c()
score_colors = c()

outdir = paste(prefix,"datasets/",dataset,"/ex",mut_type,"/figures/", sep="")

output_fn = paste(outdir, "/roc_", dataset, "_", subset, ".pdf", sep="")
pdf(output_fn, width=18,height=10)

input_fn = paste(inputdir, "/",score_files[1],"/0.txt", sep="")
print(input_fn)

data = read.table(input_fn, header=FALSE, sep="\t")
if (ncol(data) > 4) {
	positives = data[1,1] + data[1,4]
	negatives = data[1,2] + data[1,3]
} else {
	positives = 0
	negatives = 0
}

main_txt = paste("ROC curves; positives: ", toString(positives), "; negatives: " , toString(negatives), sep="")
plot(c(), xlab="fpr = FP/(FP+TN)", ylab="tpr (recall) = TP/(TP+FN)", main=main_txt, xlim=c(0.0,1.0), ylim=c(0.0, 1.0))

for (score_file in score_files) {
	print(score_file)
	score = score_file
	input_fn = paste(inputdir, "/",score_file,"/0.txt", sep="")
	data = read.table(input_fn, header=FALSE, sep="\t")

	fp = data[,2]
	tn = data[,3]
	fpr = fp/(fp+tn)
	tpr = data[,6]

	fpr = c(0.0, fpr)
	tpr = c(0.0, tpr)
	n = length(fpr)

	area = 0.0

	for (i in 2:n) {
		area = area + 0.5*(tpr[i] + tpr[i-1])*(fpr[i]-fpr[i-1])
	}

	color = rgb(runif(1),runif(1),runif(1)) 

	score_desc = paste(score, " [AUC=" , round(area,2) , "]", sep="")
	score_areas[[score_desc]] = area
	score_colors[[score_desc]] = color

	points(tpr~fpr, col=color, pch=18, lwd=1.5, type="l", xlim=c(0.0,1.0), ylim=c(0.0, 1.0))
}

score_desc_sorted = names(sort(score_areas, decreasing = T))
score_colors_sorted = c()

for (score_desc in score_desc_sorted) {
	score_colors_sorted = c(score_colors_sorted, score_colors[[score_desc]])		
}

legend("topright", fill=score_colors_sorted, legend = score_desc_sorted, ncol=4)

dev.off()


