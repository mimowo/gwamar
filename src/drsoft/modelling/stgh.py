from src.drsoft.utils import gwamar_generator_utils, gwamar_stat_utils

def colorEdges(tree, alg=0, overwrite=False):
    if alg == 0: # color all edges coming to resistant nodes
        for node in list(tree.leaves) + tree.inner_nodes_bu:
            if node.complete_res_state == "R":
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "b"
            else:
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "w"
    elif alg == 1: # color all edges with S->R
        for node in list(tree.leaves) + tree.inner_nodes_bu:
            if node.complete_res_state == "R" and (node.parent == None or node.parent.complete_res_state == "S"):
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "b"
            else:
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "w"
    return None

def calcN(tree):
    for node in tree.nodes.values():
        node.n = node.subleaves_count
    return tree.root.n

def calcVnMAT(tree, n1):
    for node in tree.nodes.values():
        node.vn = {}                             
        node.vn[0] = 1
        node.vn[1] = node.n
        node.vn[node.n] = 1

    for node in tree.inner_nodes_bu:
        children_list = node.children
        r = len(node.children)

        for n in range(2, node.n, 1):
            node.vn[n] = 0
            ubn = []
            for j in range(r):
                ubn.append(children_list[j].n)
            for nv in gwamar_generator_utils.generateSumProfiles2(r, n, ub=ubn):
                c = 1
                for j in range(r):
                    c *= children_list[j].vn[nv[j]]
                node.vn[n] += c
    return tree.root.vn

def calcBnmkMAT(tree, n1, n2):
    mem = {}

    for node in tree.leaves:
        node.bnmk = {}
        mem[node.topos] = {}
        node.bnmk[(0,0,0)] = 1
        mem[node.topos][(0,0,0)] = 1

        for m in range(node.n+1):
            node.bnmk[(node.n, m, m)] = node.vn[m]
            mem[node.topos][(node.n, m, m)] = node.vn[m]
            for k in range(m):
                node.bnmk[(node.n, m, k)] = 0
                mem[node.topos][(node.n, m, k)] = 0

    for node in tree.inner_nodes_bu:
        if node.topos in mem:
            continue
        node.bnmk = {}
        mem[node.topos] = {}
        node.bnmk[(0,0,0)] = 1
        mem[node.topos][(0,0,0)] = 1

        for m in range(node.n+1):
            node.bnmk[(node.n, m, m)] = node.vn[m]
            mem[node.topos][(node.n, m, m)] = node.vn[m]
            for k in range(m):
                node.bnmk[(node.n, m, k)] = 0
                mem[node.topos][(node.n, m, k)] = 0

        children_list = node.children
        r = len(node.children)

        for n in range(0, min(n1+1, node.n), 1):
            print(node.node_id, r, node.n, n)
            for m in range(0, min(n2, n)+1, 1):
                for k in range(max(0, n+m-node.n), min(m, node.n)+1, 1):

                    if r == node.n:
                        bnmk_tmp = gwamar_stat_utils.binom(node.n, m)*gwamar_stat_utils.binom(m, k)*gwamar_stat_utils.binom(node.n-m, n-k)            
                    else:
                        ubn = []
                        for i in range(r):
                            ubn.append(children_list[i].n)
                        bnmk_tmp = 0

                        for nv in gwamar_generator_utils.generateSumProfiles2(r, n, ub=ubn):
                            for mv in gwamar_generator_utils.generateSumProfiles2(r, m, ub=ubn):
                                ubk = []
                                lbk = []
                                for i in range(r):
                                    lbk.append(max(0, nv[i]+mv[i]-children_list[i].n))
                                    ubk.append(min(mv[i], nv[i]))

                                for kv in gwamar_generator_utils.generateSumProfiles2(r, k, lb=lbk, ub=ubk):
                                    c = 1
                                    for i in range(r):
                                        if nv[i] >= mv[i]:
                                            c *= mem[children_list[i].topos][(nv[i], mv[i], kv[i])]
                                        else:
                                            c *= mem[children_list[i].topos][(mv[i], nv[i], kv[i])]
                                    bnmk_tmp += c
                    node.bnmk[(n,m,k)] = bnmk_tmp
                    mem[node.topos][(n,m,k)] = bnmk_tmp
    tree.root.bnmk = mem[tree.root.topos]
    return tree.root.bnmk


