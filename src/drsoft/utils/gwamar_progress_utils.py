from threading import Semaphore
import sys

def shift(text_id, exp_len):
    ret = text_id
    diff = exp_len - len(text_id)
    i = 0
    while i < diff:
        ret += " "
        i+=1
    return ret;

def msg(txt):
    sys.stdout.write(txt)
    sys.stdout.flush()

class GWAMARProgress:
    def __init__(self, text):
        self.number= 0
        self.sem = Semaphore()
        self.done = 0
        self.last_done = 0
        self.dots_put = 0
        self.text = text
        sys.stdout.write(text  + ": 0% [wait...]"+ "\r")
        sys.stdout.flush()
    def setJobsCount(self, number):
        self.number = number
    def update(self, desc = ""):
        self.sem.acquire()
        self.done += 1
        dots_number = int(round(float(self.done*100)/float(self.number)))
        new_dots_number = dots_number - self.dots_put
        #print(self.done, self.number, dots_number, new_dots_number)
        if new_dots_number > 0 or self.done == 1:
            if desc != "":
                msg(shift(self.text + ": "+str(dots_number) + "% " + "["+desc+"]", 79)+"\r")
            else:
                msg(shift(self.text + ": "+str(dots_number) + "% ", 79)+"\r")
            self.last_done = self.done
            self.dots_put = dots_number
        if self.done >= self.number:
            print("")
            print(shift(self.text + ": 100% " + "[DONE]", 79))
        self.sem.release()
