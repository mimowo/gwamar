import sys
import os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils

if __name__ == '__main__':
    print("cmp2_auc_stats.py: Start.")
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = parameters["D"]
    reps_tmp = int(parameters.get("RAND", "1"))

    cmp_dir = parameters["RESULTS_CMP_DIR"]
    cmp_stats_dir = parameters["RESULTS_CMP_STATS_DIR"]
    cmp_assocs_dir = parameters["RESULTS_CMP_ASSOCS_DIR"]
   
    scores_cmp = parameters.get("SCORES", "ws,or,mi,lh,r-tgh").split(",")
    stats = parameters.get("STATS", "auc,auc10,auc20").split(",")

    score_fns = os.listdir(cmp_assocs_dir)
    scores_list = []
    for score in scores_cmp:
        score_fn = score + ".txt"
        if score_fn in score_fns:
            scores_list.append(score)

    subsets = parameters.get("SUBSETS", "p14,p15").split(",")
    
    for subset in subsets:
        if subset.startswith("a"): reps = 1
        elif subset.startswith("s"): reps = reps_tmp
        else: reps = reps_tmp

        stats_pr_dir = cmp_stats_dir + "/" + subset + "/pr/"

        results_map = []
        for i in range(reps):
            results_map.append({})

        for score in scores_list:
            stats_pr_score_dir = stats_pr_dir + "/" + score + "/"
            auc_v = []

            tls = []

            for i in range(reps):
                input_fn = stats_pr_score_dir + "/" + str(i) + ".txt"
                if not os.path.exists(input_fn):
                    continue

                input_fh = open(input_fn)
                lines = input_fh.readlines()
                input_fh.close()

                results_map[i][score] = {}
                for stat_name in stats:
                    results_map[i][score][stat_name] = "NA"

                for j in range(len(lines)):
                    line = lines[j]
                    score_val, prec, recall, auc_val = map(float, line.split())
                    if j+1 in [10,20]:
                        stat_name = "auc" + str(j+1)
                        results_map[i][score][stat_name] = auc_val
                    if j+1 == len(lines):
                        stat_name = "auc"
                        results_map[i][score][stat_name] = auc_val


        for stat in stats:
            output_fh = open(cmp_stats_dir + "/" + subset + "/"+str(stat)+".txt", "w")
            output_fh.write("\t".join(scores_list) + "\n")
            
            for i in range(len(results_map)):
                tl = ""
                for score in scores_list:
                    if score in results_map[i]:
                        tl += str(results_map[i][score][stat]) + "\t"
                    else:
                        tl += "NA" + "\t"
                tl = tl.strip() + "\n"
                output_fh.write(tl)
            output_fh.close()

    print("cmp2_auc_stats.py: Finished.")
