
%brandis_fitness-compensatory_2012
>rpoA	Rv3457c
182|191	R	C,S
185|194	Q	P

>rpoB	Rv0667
435|516	D	G
479|560	P	L
483|564	P	S
484|565	E	A
556|637	R	C
593|673	H	Y

>rpoC	Rv0668
#64	P	L
847|770	L	P
1026|1136	G	A
1030|1140	R	H
1088|1198	V	E

%brandis_genetic_2013
>rpoA	Rv3457c
180|189	A	E
187|196	T	S

>rpoB	Rv0667
#339	N	H
#340	D	Y
465|546	E	K
722|809	G	A
1056|1264	Q	R

>rpoC	Rv0668
494|419	H	P
521|446	A	V
714|622	D	E
726|634	R	P
766|674	T	I
#937	I	T
#942	S	L
#943	R	H
#1075	R	L,C
1026|1136	G	A

#Rv3457c rpoA 3878507 3877464 -
GGGGGGCGCCGCCCCCCGAGTACCCCCACCCTCGGGGGCGCCGCCCCCGAGTGCCCCCACAGACGTCATATGGCGGACGTCGAAAGGAAGAAGAAACACC
MLISQRPTLSEDVLTDNRSQFVIEPLEPGFGYTLGNSLRRTLLSSIPGAAVTSIRIDGVLHEFTTVPGVKEDVTEIILNLKSLVVSSEEDEPVTMYLRKQGPGEVTAGDIVPPAGVTVHNPGMHIATLNDKGKLEVELVVERGRGYVPAVQNRASGAEIGRIPVDSIYSPVLKVTYKVDATRVEQRTDFDKLILDVETKNSISPRDALASAGKTLVELFGLARELNVEAEGIEIGPSPAEADHIASFALPIDDLDLTVRSYNCLKREGVHTVGELVARTESDLLDIRNFGQKSIDEVKIKLHQLGLSLKDSPPSFDPSEVAGYDVATGTWSTEGAYDEQDYAETEQL*

#Rv0667 rpoB 759807 763325 +
CGCCGGCCGAAACCGACAAAATTATCGCGGCGAACGGGCCCGTGGGCACCGCTCCTCTAAGGGCTCTCGTTGGTCGCATGAAGTGCTGGAAGGATGCATC
LADSRQSKTAASPSPSRPQSSSNNSVPGAPNRVSFAKLREPLEVPGLLDVQTDSFEWLIGSPRWRESAAERGDVNPVGGLEEVLYELSPIEDFSGSMSLSFSDPRFDDVKAPVDECKDKDMTYAAPLFVTAEFINNNTGEIKSQTVFMGDFPMMTEKGTFIINGTERVVVSQLVRSPGVYFDETIDKSTDKTLHSVKVIPSRGAWLEFDVDKRDTVGVRIDRKRRQPVTVLLKALGWTSEQIVERFGFSEIMRSTLEKDNTVGTDEALLDIYRKLRPGEPPTKESAQTLLENLFFKEKRYDLARVGRYKVNKKLGLHVGEPITSSTLTEEDVVATIEYLVRLHEGQTTMTVPGGVEVPVETDDIDHFGNRRLRTVGELIQNQIRVGMSRMERVVRERMTTQDVEAITPQTLINIRPVVAAIKEFFGTSQLSQFMDQNNPLSGLTHKRRLSALGPGGLSRERAGLEVRDVHPSHYGRMCPIETPEGPNIGLIGSLSVYARVNPFGFIETPYRKVVDGVVSDEIVYLTADEEDRHVVAQANSPIDADGRFVEPRVLVRRKAGEVEYVPSSEVDYMDVSPRQMVSVATAMIPFLEHDDANRALMGANMQRQAVPLVRSEAPLVGTGMELRAAIDAGDVVVAEESGVIEEVSADYITVMHDNGTRRTYRMRKFARSNHGTCANQCPIVDAGDRVEAGQVIADGPCTDDGEMALGKNLLVAIMPWEGHNYEDAIILSNRLVEEDVLTSIHIEEHEIDARDTKLGAEEITRDIPNISDEVLADLDERGIVRIGAEVRDGDILVGKVTPKGETELTPEERLLRAIFGEKAREVRDTSLKVPHGESGKVIGIRVFSREDEDELPAGVNELVRVYVAQKRKISDGDKLAGRHGNKGVIGKILPVEDMPFLADGTPVDIILNTHGVPRRMNIGQILETHLGWCAHSGWKVDAAKGVPDWAARLPDELLEAQPNAIVSTPVFDGAQEAELQGLLSCTLPNRDGDVLVDADGKAMLFDGRSGEPFPYPVTVGYMYIMKLHHLVDDKIHARSTGPYSMITQQPLGGKAQFGGQRFGEMECWAMQAYGAAYTLQELLTIKSDDTVGRVKVYEAIVKGENIPEPGIPESFKVLLKELQSLCLNVEVLSSDGAAIELREGEDEDLERAAANLGINLSRNESASVEDLA*

#Rv0668 rpoC 763370 767320 +
ACCTGGGAATCAATCTGTCCCGCAACGAATCCGCAAGTGTCGAGGATCTTGCGTAAAGCTGTCGCAAAATTACTAAACCCGTTAGGGGAAAGGGAGTTAC
VLDVNFFDELRIGLATAEDIRQWSYGEVKKPETINYRTLKPEKDGLFCEKIFGPTRDWECYCGKYKRVRFKGIICERCGVEVTRAKVRRERMGHIELAAPVTHIWYFKGVPSRLGYLLDLAPKDLEKIIYFAAYVITSVDEEMRHNELSTLEAEMAVERKAVEDQRDGELEARAQKLEADLAELEAEGAKADARRKVRDGGEREMRQIRDRAQRELDRLEDIWSTFTKLAPKQLIVDENLYRELVDRYGEYFTGAMGAESIQKLIENFDIDAEAESLRDVIRNGKGQKKLRALKRLKVVAAFQQSGNSPMGMVLDAVPVIPPELRPMVQLDGGRFATSDLNDLYRRVINRNNRLKRLIDLGAPEIIVNNEKRMLQESVDALFDNGRRGRPVTGPGNRPLKSLSDLLKGKQGRFRQNLLGKRVDYSGRSVIVVGPQLKLHQCGLPKLMALELFKPFVMKRLVDLNHAQNIKSAKRMVERQRPQVWDVLEEVIAEHPVLLNRAPTLHRLGIQAFEPMLVEGKAIQLHPLVCEAFNADFDGDQMAVHLPLSAEAQAEARILMLSSNNILSPASGRPLAMPRLDMVTGLYYLTTEVPGDTGEYQPASGDHPETGVYSSPAEAIMAADRGVLSVRAKIKVRLTQLRPPVEIEAELFGHSGWQPGDAWMAETTLGRVMFNELLPLGYPFVNKQMHKKVQAAIINDLAERYPMIVVAQTVDKLKDAGFYWATRSGVTVSMADVLVPPRKKEILDHYEERADKVEKQFQRGALNHDERNEALVEIWKEATDEVGQALREHYPDDNPIITIVDSGATGNFTQTRTLAGMKGLVTNPKGEFIPRPVKSSFREGLTVLEYFINTHGARKGLADTALRTADSGYLTRRLVDVSQDVIVREHDCQTERGIVVELAERAPDGTLIRDPYIETSAYARTLGTDAVDEAGNVIVERGQDLGDPEIDALLAAGITQVKVRSVLTCATSTGVCATCYGRSMATGKLVDIGEAVGIVAAQSIGEPGTQLTMRTFHQGGVGEDITGGLPRVQELFEARVPRGKAPIADVTGRVRLEDGERFYKITIVPDDGGEEVVYDKISKRQRLRVFKHEDGSERVLSDGDHVEVGQQLMEGSADPHEVLRVQGPREVQIHLVREVQEVYRAQGVSIHDKHIEVIVRQMLRRVTIIDSGSTEFLPGSLIDRAEFEAENRRVVAEGGEPAAGRPVLMGITKASLATDSWLSAASFQETTRVLTDAAINCDKLNGLKENVIIGKLIPAGTGINRYRNIAVQPTEEARAAAYTIPSYEDQYYSPDFGAATGAAVPLDDYGYSDYR*

#salmonella RpoC
MKDLLKFLKAQTKTEEFDAIKIALASPDMIRSWSFGEVKKPETINYRTFKPERDGLFCARIFGPVKDYECLCGKYKRLKHRGVICEKCGVEVTQTKVRRERMGHIELASPTAHIWFLKSLPSRIGLLLDMPLRDIERVLYFESYVVIEGGMTNLERQQILTEEQYLDALEEFGDEFDAKMGAEAIQALLKSMDLEQECETLREELNETNSETKRKKLTKRIKLLEAFVQSGNKPEWMILTVLPVLPPDLRPLVPLDGGRFATSDLNDLYRRVINRNNRLKRLLDLAAPDIIVRNEKRMLQEAVDALLDNGRRGRAITGSNKRPLKSLADMIKGKQGRFRQNLLGKRVDYSGRSVITVGPYLRLHQCGLPKKMALELFKPFIYGKLELRGLATTIKAAKKMVEREEAVVWDILDEVIREHPVLLNRAPTLHRLGIQAFEPVLIEGKAIQLHPLVCAAYNADFDGDQMAVHVPLTLEAQLEARALMMSTNNILSPANGEPIIVPSQDVVLGLYYMTRDCVNAKGEGMVLTGPKEAERIYRAGLASLHARVKVRITEYEKDENGEFVAHTSLKDTTVGRAILWMIVPKGLPFSIVNQALGKKAISKMLNTCYRILGLKPTVIFADQTMYTGFAYAARSGASVGIDDMVIPEKKHEIISEAEAEVAEIQEQFQSGLVTAGERYNKVIDIWAAANDRVSKAMMDNLQTETVINRDGQEEQQVSFNSIYMMADSGARGSAAQIRQLAGMRGLMAKPDGSIIETPITANFREGLNVLQYFISTHGARKGLADTALKTANSGYLTRRLVDVAQDLVVTEDDCGTHEGILMTPVIEGGDVKEPLRDRVLGRVTAEDVLKPGTADILVPRNTLLHEQWCDLLEANSVDAVKVRSVVSCDTDFGVCAHCYGRDLARGHIINKGEAIGVIAAQSIGEPGTQLTMRTFHIGGAASRAAAESSIQVKNKGSIKLSNVKSVVNSSGKLVITSRNTELKLIDEFGRTKESYKVPYGAVMAKGDGEQVAGGETVANWDPHTMPVITEVSGFIRFTDMIDGQTITRQTDELTGLSSLVVLDSAERTTGGKDLRPALKIVDAQGNDVLIPGTDMPAQYFLPGKAIVQLEDGVQISSGDTLARIPQESGGTKDITGGLPRVADLFEARRPKEPAILAEIAGIVSFGKETKGKRRLVITPVDGSDPYEEMIPKWRQLNVFEGERVERGDVISDGPEAPHDILRLRGVHAVTRYIVNEVQDVYRLQGVKINDKHIEVIVRQMLRKATIESAGSSDFLEGEQVEYSRVKIANRELEANGKVGATFSRDLLGITKASLATESFISAASFQETTRVLTEAAVAGKRDELRGLKENVIVGRLIPAGTGYAYHQDRMRRRAAGEQPATPQVTAEDASASLAELLNAGLGGSDNE
