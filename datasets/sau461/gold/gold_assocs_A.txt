Meticillin	mecA	SAR0039	0	L	G
Meticillin	mecA	SAR0041	0	L	G
Meticillin	mecA	SACOL0034	0	L	G
Meticillin	mecA	SARLGA251_00260	0	L	G
Meticillin	mecA	SARLGA251_00280	0	L	G
Meticillin	mecA	SARLGA251_00270	0	L	G

Penicillin	mecA	SAR0039	0	L	G
Penicillin	mecA	SAR0041	0	L	G
Penicillin	mecA	SACOL0034	0	L	G
Penicillin	mecA	SARLGA251_00260	0	L	G
Penicillin	mecA	SARLGA251_00280	0	L	G
Penicillin	mecA	SARLGA251_00270	0	L	G
Penicillin	mecA	SAR1830	0	L	G
Penicillin	mecA	SAR1831	0	L	G
Penicillin	mecA	SARLGA251_00250	0	L	G
Penicillin	mecA	SARLGA251_00260	0	L	G
Penicillin	mecA	SARLGA251_00280	0	L	G
Penicillin	mecA	SARLGA251_00270	0	L	G

Oxacillin	mecA	SAR0039	0	L	G
Oxacillin	mecA	SAR0041	0	L	G
Oxacillin	mecA	SACOL0034	0	L	G
Oxacillin	mecA	SARLGA251_00260	0	L	G
Oxacillin	mecA	SARLGA251_00280	0	L	G
Oxacillin	mecA	SARLGA251_00270	0	L	G

Erythromycin	ermC	SAR0050	0	L	G
Erythromycin	ermC	SAR1735	0	L	G
Erythromycin	ermC	SAUSA300_pUSA030007	0	L	G
Erythromycin	ermC	HMPREF0773_2657	0	L	G
Clindamycin	ermC	SAR0050	0	L	G
Clindamycin	ermC	SAR0050	0	L	G
Clindamycin	ermC	SAR0050	0	L	G
Clindamycin	ermC	SAR0050	0	L	G
Clindamycin	ermC	SAR0050	0	L	G

Gentamicin	aaCa-aphd	SAA6008_01993	0	L	G

Tetracycline	tet	SAHV_0395	0	L	G
Tetracycline	tet	SATW20_00660	0	L	G
Tetracycline	tet	SAR2464	0	L	G
Tetracycline	tet	SA21252_1148	0	L	G
Tetracycline	tet	SAR0139	0	L	G
Tetracycline	tet	SAR2464	0	L	G
Tetracycline	tet	pKKS627_p3	0	L	G
Tetracycline	tet	pKKS825_p5	0	L	G

