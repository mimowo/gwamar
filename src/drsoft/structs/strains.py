class Strains:
    def __init__(self):
        self.ref_strain = ""
        self.strain_ids = []
    def setReferenceStrain(self, strain_id):
        self.ref_strain = strain_id;
    def addStrain(self, strain_id, position = 0):
        if position == 0:
            self.strain_ids.append(strain_id)
        else:
            self.strain_ids.insert(position, strain_id);
    def getStrain(self, position):
        return self.strain_ids[position]
    def getPosition(self, strain_id):
        return self.strain_ids.index(strain_id)
    def count(self):
        return len(self.strain_ids)
    def changeOrder(self, strain_positions_map):
        for strain_id in strain_positions_map:
            pos = strain_positions_map[strain_id]
            self.strain_ids[pos] = strain_id
    def allStrains(self):
        return self.strain_ids
