import os, math

from decimal import Decimal
from src.drsoft.utils import gwamar_stat_utils, gwamar_generator_utils,\
  gwamar_bin_utils
from src.drsoft.modelling import model_logic

def colorEdges(tree, alg=0, overwrite=False):
    if alg == 0: # color all edges coming to resistant nodes
        for node in list(tree.leaves) + tree.inner_nodes_bu:
            if node.complete_res_state == "R":
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "b"
            else:
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "w"
    elif alg == 1: # color all edges with S->R
        for node in list(tree.leaves) + tree.inner_nodes_bu:
            if node.complete_res_state == "R" and (node.parent == None or node.parent.complete_res_state == "S"):
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "b"
            else:
                if overwrite == True or node.ecolor == None:
                    node.ecolor = "w"
    return None

def calcFastBC(tree):
    for node in tree.leaves: node.bc = 0
    for node in tree.inner_nodes_bu:
        node.bc = 0
        for node_j in node.children:
            if node_j.ecolor == "b":
                node.bc += 1
    return tree.root.bc

def calcFastN(tree, w2=False):
    for node in tree.leaves: node.maxn = 1

    if w2:
        for node in tree.inner_nodes_bu:
            sum_tmp = 0
            all_leaves = True
            for ch_node in node.children:
                sum_tmp += max(1, ch_node.maxn)
                if not ch_node.is_leaf():
                    all_leaves = False 
            if all_leaves:
                node.maxn = len(node.children)-1
            else:
                node.maxn = sum_tmp
    else:
        for node in tree.inner_nodes_bu:
            node.maxn = 0
            for ch_node in node.children:
                node.maxn += max(ch_node.maxn, 1)
    
    # for node in tree.nodes.values():
    #   print(node.topos, node.maxn, tree.root.maxn)
    
    return tree.root.maxn

def calcFastP(tree, w2=False):
    for node in tree.leaves: 
        node.maxp0 = 0
        if node.ecolor == "b": node.maxp = 1
        else: node.maxp = 0

    if w2:
        for node in tree.inner_nodes_bu:
            sum_tmp = 0
            all_black = True
            for ch_node in node.children:
                sum_tmp += ch_node.maxp
                if ch_node.ecolor != "b": all_black = False
                
            if all_black:
                sum_tmp_max = 0
                for ch_node in node.children:
                    sum_tmp_max = max(sum_tmp_max, sum_tmp - ch_node.maxp + ch_node.maxp0)
                node.maxp0 = sum_tmp_max
                if node.ecolor == "b": node.maxp = max(1, sum_tmp_max)
                else: node.maxp = sum_tmp_max
            else:
                node.maxp0 = sum_tmp
                if node.ecolor == "b": node.maxp = max(1, sum_tmp)
                else: node.maxp = sum_tmp
    else:
        # print("there, maxp")
        for node in tree.inner_nodes_bu:
            sum_tmp = 0
            for ch_node in node.children:
                sum_tmp += ch_node.maxp
            if node.ecolor == "b": node.maxp = max(1, sum_tmp)
            else: node.maxp = sum_tmp
    # for node in tree.nodes.values():
    #     print("maxp", node.node_id, node.maxp)

    return tree.root.maxp

def calcFastWnMAT(tree, maxn_t=None, opt=False, w2=True):
    for node in tree.leaves:
        node.count_map = {}                             
        node.count_map[0] = 1

    for node in tree.inner_nodes_bu:
        node.count_map = {}
        k = len(node.children)

        maxn = min(maxn_t, node.maxn)

        for nt in range(maxn+1):
            if w2: maxr = min(nt, k-1, node.maxn)
            else: maxr = min(nt, k, node.maxn)
            
            if opt and (k+1 == node.subnodes_count):
                if w2 and k == nt: wnm = 0
                else: wnm = gwamar_stat_utils.binom(k, nt)
            else:
                wnm = 0
            
                for r in range( maxr+1):
                    for gain_profile in gwamar_generator_utils.generateSumProfiles2(k, r, ub=[1]*k,lb=[0]*k):
                        gain_bound = []
                        for j in range(k): 
                            if gain_profile[j] == 1: 
                                gain_bound.append(0)
                            else:
                                gain_bound.append(node.maxn)
                        for gain_subprofile in gwamar_generator_utils.generateSumProfiles2(k, nt-r, ub=gain_bound):
                            wnm_tmp = 1
                            for j in range(k):
                                if gain_profile[j] == 0:
                                    node_j = node.children[j]
                                    wnm_tmp *= node_j.count_map.get(gain_subprofile[j], 0)
                                    if wnm_tmp == 0:
                                        break    
                            wnm += wnm_tmp
            node.count_map[nt] = wnm
    return tree.root.count_map

def calcFastRP(tree):
    # r - total number of 0->1 changes 
    # p - total number of 0->1 changes on black edges
    r,p = 0,0
    for node in tree.inner_nodes_bu:
        for node_j in node.children:
            if node.complete_mut_state == '0' and node_j.complete_mut_state == "1":
                r += 1
                if node_j.ecolor == "b":
                    p += 1
    return r,p

def calcFastBpnMAT(tree, maxp_t, maxn_t, opt=False, w2=True, mem={}):
    mem = {}

    for node in tree.leaves:
        mem[node.topos] = {}
        mem[node.topos][(0,0)] = 1
        
    for node in tree.inner_nodes_bu:
        maxn = min(maxn_t, node.maxn)
        maxp = min(maxp_t, node.maxp)

        if node.topos in mem and (maxp, maxn) in mem[node.topos]:
            continue
        if not node.topos in mem: mem[node.topos] = {}
        
        children_list = list(node.children)
        k = len(children_list)

        for nt in range(maxn + 1): # liczba wybranych
            maxp = min(node.maxp, nt, maxp_t)
            if (maxp, nt) in mem[node.topos]:
                continue
            for pt in range(maxp + 1):     # liczba pokrytych
                if (pt, nt) in mem[node.topos]:
                    continue

                if opt and (k+1 == node.subnodes_count):
                    if w2 and k == nt: wnm = 0
                    else: wnm = gwamar_stat_utils.binom(node.bc, pt) * gwamar_stat_utils.binom(k-node.bc, nt-pt)
                else:
                    wnm = 0
                    if w2: maxr = min(k-1, nt)
                    else: maxr = min(k, nt)
                    for r in range(maxr + 1):
                        for gain_r_profile in gwamar_generator_utils.generateSumProfiles2(k, r, ub=[1]*k,lb=[0]*k):
                            ur = 0 # liczba trafionych
                            gain_r_bound = []
                            for j in range(k):
                                node_j = children_list[j]
                                if gain_r_profile[j] == 1:
                                    gain_r_bound.append(0)
                                    if  node_j.ecolor == "b":
                                        ur += 1
                                elif gain_r_profile[j] == 0:
                                    gain_r_bound.append(min(node_j.maxn, nt-r))

                            for gain_r_subprofile in gwamar_generator_utils.generateSumProfiles2(k, nt-r, ub=gain_r_bound):
                                gain_p_bound = []
                                for j in range(k):
                                    node_j = children_list[j]
                                    gain_p_bound.append(min(node_j.maxp, gain_r_subprofile[j]))

                                for gain_u_subprofile in gwamar_generator_utils.generateSumProfiles2(k, pt-ur, ub=gain_p_bound):
                                    wnm_tmp = 1

                                    for j in range(k):
                                        if gain_r_profile[j] == 0:
                                            node_j = children_list[j]
                                            ptmp, ntmp = gain_u_subprofile[j], gain_r_subprofile[j]
                                            wnm_tmp *= mem[node_j.topos].get((ptmp, ntmp), 0)
                                            if wnm_tmp == 0:
                                                break
                                    wnm += wnm_tmp

                mem[node.topos][(pt,nt)] = wnm
    return mem


    
def calcBinProfilesTGH(bin_profiles, drp, trees_list, norm=False, opt=True, w2=True, mem={}):
    scores = {}
    
    for bin_profile_id, bin_profile in bin_profiles.items():
        scores[bin_profile_id] = []
    
    for tree in trees_list:
        tree.setObservedResProfile(drp.full_profile)
        tree.calcInnerNodesBU()
        model_logic.calcSubleavesResCount(tree)
        model_logic.calcSubnodesCount(tree)

        if len(drp.getResistant()) == 0:
            continue
        
        newtree = model_logic.removeUnResLeaves(tree)
        newtree.calcInnerNodesBU()
        newtree.setObservedResProfile(drp.full_profile)
        model_logic.calcSubleavesResCount(newtree)
        model_logic.calcSubnodesCount(newtree)
        model_logic.calcDetParsResModel(newtree)
        colorEdges(newtree, alg=0, overwrite=False)
        model_logic.computeTopos(newtree, r=True)

        maxn = 0
        maxp = 0

        n_dist = {}
        
        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)
                model_logic.calcDetParsMutModel(newtree)
                r,p = calcFastRP(newtree)
                maxn = max(maxn,r)
                maxp = max(maxp,p)
                if not r in n_dist:
                    n_dist[r] = 0.0
                n_dist[r] += 1.0
        
        calcFastBC(newtree)
        calcFastN(newtree, w2=w2)
        calcFastP(newtree, w2=w2)

        wn_mat = calcFastWnMAT(newtree, maxn, opt=opt, w2=w2)

        mem = calcFastBpnMAT(newtree, maxp, maxn, opt=opt, w2=w2, mem=mem)
        bpn_mat = mem[newtree.root.topos]

        for bin_profile_id, gen_bin_profile in bin_profiles.items():
            score = 0.0
            for bin_profile in gwamar_bin_utils.specBinProfiles(gen_bin_profile):
                newtree.setObservedMutProfile(bin_profile)
                model_logic.calcSubleavesMutCount(newtree)
                model_logic.calcDetParsMutModel(newtree)
                
                r,p = calcFastRP(newtree)

                bs = wn_mat[r]
                for pt in range(p):
                    bs -= bpn_mat[(pt, r)]

                cct_score = Decimal(bs) / Decimal(wn_mat[r])
                try:
                    if norm:
                        score += -math.log(float(cct_score)*n_dist[r])
                    else:
                        score += -math.log(float(cct_score))
                except:
                    pass
                 
            scores[bin_profile_id].append(score)

    scores_mean = {}
    
    for bin_profile_id in scores:
        scores_mean[bin_profile_id] = gwamar_stat_utils.mean(scores[bin_profile_id])

    return scores_mean, mem


def readTGHMem(mem_fn):
    if not os.path.exists(mem_fn):
        return {}
    mem = {}

    input_fh = open(mem_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    topo = ""
    for line in lines:
        tokens = line.split()
        if len(tokens) < 1: 
            continue
        elif len(tokens) == 1:
            topo = tokens[0]
            if not topo in mem:
                mem[topo] = {}
        else:
            k,n,b = map(int, tokens)
            mem[topo][(k,n)] = b

    return mem

def saveTGHMem(mem, mem_fn):
    output_fh = open(mem_fn, "w")
    topo_sorted = sorted(mem.keys(), key=lambda topo:(len(topo), topo))
    for topo in topo_sorted:
        output_fh.write(topo + "\n")
        for (k, n) in sorted(mem[topo], key=lambda kn:(kn[1],kn[0])):
            b = mem[topo][(k,n)]
            output_fh.write("\t".join(map(str,[k, n, b])) + "\n")
    output_fh.close()

