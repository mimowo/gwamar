def maxGroup(l):
	ret = 0.0
	w = 0
	for a in l:
		if w > 0: w += a
		else: w = a
		if (w > ret): ret = w
	return ret
