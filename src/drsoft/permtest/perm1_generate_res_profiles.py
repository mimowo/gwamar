import sys
import os
import random
from multiprocessing import Pool
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils, gwamar_utils, gwamar_progress_utils
from src.drsoft.structs import resistance_profile

def generateResProfiles(params):
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    drug_name = params["DRUG_NAME"]
    perm_rep = int(parameters["REPS"])

    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    perm_profiles_dir = parameters["RESULTS_PERM_PROFILES_DIR"]
    
    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    strains_list = strains.allStrains()

    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    res_profiles = {}
    for res_profile in res_profiles_list:
        res_profiles[res_profile.drug_name] = res_profile
    drp = res_profiles[drug_name]
    
    n = strains.count()

    used_p = set([])
    
    input_fn = perm_profiles_dir + "/" + drug_name + ".txt"
    if os.path.exists(input_fn):
        input_fh = open(input_fn) 
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 1: continue
            tokens2 = tokens[0].split("_")
            if len(tokens2) < 2: continue
            used_p.add(int(tokens2[-1]))
        input_fh.close()

    perm_res_profiles = []
        
    for p in range(perm_rep):
        if p in used_p: continue
        
        perm = list(drp.full_profile)
        random.shuffle(perm)
        drug_name_id = drug_name + "_" + str(p)
        perm_res_profile = resistance_profile.ResistanceProfile(drug_name_id, n)
        perm_res_profile.full_profile = perm
        perm_res_profiles.append(perm_res_profile)
        
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 2: header = True
    else: header = False
    
    output_fh = open(input_fn, "a") 
    gwamar_res_io_utils.saveResistanceProfiles(output_fh, perm_res_profiles, strains_list=strains_list, compress=True, header=header)
    output_fh.close()
    
    return drug_name
    
if __name__ == '__main__':
    print("perm1_generate_res_profiles.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    perm_dir = parameters["RESULTS_PERM_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    perm_profiles_dir = parameters["RESULTS_PERM_PROFILES_DIR"]
    
    gwamar_utils.ensure_dir(perm_dir)
    gwamar_utils.ensure_dir(perm_profiles_dir)
    
    TASKS = []
    TASK = {}
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        TASK["DRUG_NAME"] = drug_name

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Profiles generated")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))
    
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(generateResProfiles, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = generateResProfiles(T)
            progress.update(str(r))
    print("perm1_generate_res_profiles.py. Finished.")
